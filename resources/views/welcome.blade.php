@extends('layouts.main')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="card-box">
                    <div class="row">
                        <div class="col-sm-6">
                            <h1><b>{{ $settings['app_title'] }}</b></h1>


                            <div class="row top-margin-20">
                                <p>{{ $settings['app_subtitle'] }}</p>
                            </div>



                            <div class="row top-margin-30 left-padding-50" style="text-align: center">

                                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-6" >
                                    <div class="single-pack">
                                        <div class="icon-cover">
                                            <i class="fa fa-users"></i>
                                        </div>
                                        <p> {{ $data->total_student }} Students </p>
                                    </div>
                                </div>

                                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-6">
                                    <div class="single-pack">
                                        <div class="icon-cover"><i class="fa fa-video-camera"></i></div>
                                        <p>{{ $data->total_courses }} Courses </p>
                                    </div>
                                </div>

                                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-6">
                                    <div class="single-pack">
                                        <div class="icon-cover"><i class="fa fa-graduation-cap"></i></div>
                                        <p>{{ $data->total_tutor }} eTeachers </p>
                                    </div>
                                </div>

                                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-6">
                                    <div class="single-pack">
                                        <div class="icon-cover"><i class="fa fa-desktop"></i></div>
                                        <p>Best Experience</p>
                                    </div>
                                </div>
                            </div>

                            <div class="button-cover top-margin-20">
                                <div class="btn-group" role="group" aria-label="...">
                                    <a title="All Courses" href="/courses/category/all"><button type="button" class="btn btn-default"> ALL COURSES</button></a>
                                    <a title="About 3scholars" href="/about">
                                        <button type="button" class="btn btn-default"><i class="fa fa-play-circle"> </i>&nbsp; ABOUT SCHOLARS</button>
                                    </a>

                                </div>
                            </div>

                        </div>

                        <div class="col-sm-6">
                            <div class="course_video_wrapper left-padding-50">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="{{ $settings['youtube_video_link'] }}" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="">
                                    </iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tab-hide">
            <div class="card-box">
                <h4 class="text-dark header-title m-t-0 m-b-30">Select Category</h4>

                <div id="portlet2" class="panel-collapse collapse in">
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-hover mails m-0 table table-actions-bar">


                                <tbody>
                                <tr class="success"><td><a href="/courses/category/all"> <i class="fa fa-th-large"></i>  &nbsp; All courses</a></td></tr>

                                @foreach($catagories as $category)
                                    <tr><td><a href="/courses/category/{{ $category->category_id }}"> <i class="fa fa-th-large"></i>  &nbsp; {{ $category->category_name }}</a></td></tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-9 col-lg-9 col-sm-8 col-xs-12">
            <div class="card-box">
                <h4 class="text-dark header-title m-t-0">Select Course</h4>
                <div class="row" style="margin-top: 20px">

                    @foreach($courses as $course)

                        <div class="col-md-4">
                            <div class="thumbnail" href="#">
                                <a href="course/{{ $course->course_id }}">
                                    <img src="/images/course/{{ $course->course_image }}" class="img-responsive" style="height: 130px">

                                    <table class="table">
                                        <tbody>

                                        <tr>
                                            <td width="50%">
                                                <div class="profile">
                                                    <img class="img-rounded img-responsive" alt="Instructor Profile Picture" src="/images/users/{{ $course->user_img }}">
                                                </div>
                                            </td>
                                            <td>{{ $course->name }}</td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <i class="md md-star yellow"></i>
                                                <i class="md md-star yellow"></i>
                                                <i class="md md-star yellow"></i>
                                                <i class="md md-star yellow"></i>
                                                <i class="md md-star-half yellow"></i>
                                            </td>
                                            <td><i class="glyphicon glyphicon-user"></i> {{ $course->student_count }}</td>
                                        </tr>

                                        <tr>
                                            <td colspan="2" style="height: 67px">
                                                <div class="course_caption">
                                                    <strong>{{ $course->course_name }}</strong>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="2">
                                                <div class="course_description">
                                                    {{ get_words($course->course_title) }}
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <strong class="course_price_tag">
                                                    @if($course->free_course == 1)
                                                        Free
                                                    @elseif($course->discount == 0)
                                                        ৳ {{ $course->price }}
                                                    @elseif($course->discount > 0 && $course->discount<$course->price)
                                                        <del>৳{{ $course->price }}</del>&nbsp; ৳{{ $course->discount }}
                                                    @else
                                                        ৳ {{ $course->price }}
                                                    @endif
                                                </strong>
                                            </td>
                                            <td>{{ $course->category_name }}</td>
                                        </tr>

                                        </tbody>
                                    </table>

                                </a>


                            </div>
                        </div>

                    @endforeach
                        {{ $courses->links() }}

                </div>
            </div>
        </div>
    </div>
    <!-- end row -->




@endsection
