@extends('layouts.main')

@section('title', 'Frequently Asked Questions')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/view/courses">Courses</a></li>
    <li><a href="/course/{{ $course->course_id }}">{{ $course->course_name }}</a></li>
    <li><a href="">Frequently Asked Questions</a></li>
  </ol>
@endsection


@section('content')

  <script src="/js/faq.js"></script>

  <input type="hidden" id="course_id" value="{{ $course->course_id }}">


  <div class="row">
    <div class="col-sm-12">
      <div class="card-box">

        <div class="row">
          <div class="col-sm-12">
            <div class="text-center">
              <h3 class="font-600"><a href="/course/{{ $course->course_id }}">{{ $course->course_name }}</a></h3>
              <p class="text-muted">{{ $course->course_title }}</p>

              @auth
                @if(Auth::user()->id == $course->instructor)
                  <button type="button" class="btn btn-success waves-effect waves-light m-t-10" data-toggle='modal' data-target='#add_faq_modal'>Add FAQ</button>
                @else
                  <button type="button" class="btn btn-primary waves-effect waves-light m-t-10">Send us your question</button>
                @endif

              @else
                <button type="button" class="btn btn-primary waves-effect waves-light m-t-10">Send us your question</button>
              @endauth




            </div>
          </div>
        </div>

        <div class="row m-t-40">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="p-20 p-t-0" id="faq_span">
              <!-- Question/Answer -->

              @foreach($faq as $item)

                <div class="faq-box" id="faq_{{ $item->faq_id }}">
                  <h4 class="question" data-wow-delay=".1s">{{ $item->faq_question }}</h4>
                  <p class="answer">{{ $item->faq_answer }}</p>


                  @auth
                    @if(Auth::user()->id == $course->instructor)
                      <div class="m-t-30">
                        <button type="button" class="btn btn-instagram waves-effect waves-light"
                                onclick="edit_faq('{{ $item->faq_id }}', '{{ $item->faq_question }}', '{{ $item->faq_answer }}')">
                          <i class="ion-edit"></i>
                        </button>

                        <button type="button" class="btn btn-youtube waves-effect waves-light"
                                onclick="delete_faq('{{ $item->faq_id }}')">
                          <i class="ion-close"></i>
                        </button>
                      </div>
                    @endif

                  @endauth




                </div>

              @endforeach


            </div>
          </div>

        </div>
      </div>
    </div>
  </div>




  <div id="add_faq_modal" class="modal bounceInDown animated" role="dialog" style="padding-left: 17px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Add FAQ</h4>
        </div>
        <div class="modal-body">

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="faq_question" class="control-label">Question</label>
                <input type="text" class="form-control" id="faq_question">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group no-margin">
                <label for="faq_answer" class="control-label">Answer</label>
                <textarea class="form-control autogrow" id="faq_answer" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
          <button id='add_faq_button' type="button" class="btn btn-info waves-effect waves-light" data-dismiss="modal">Ok</button>
        </div>
      </div>
    </div>
  </div>


  <div id="edit_faq_modal" class="modal bounceInDown animated" role="dialog" style="padding-left: 17px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Edit FAQ</h4>
        </div>
        <div class="modal-body">

          <input type="hidden" id="edit_faq_id">

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="faq_question" class="control-label">Question</label>
                <input type="text" class="form-control" id="edit_faq_question">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group no-margin">
                <label for="faq_answer" class="control-label">Answer</label>
                <textarea class="form-control autogrow" id="edit_faq_answer" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
          <button id='edit_faq_button' type="button" class="btn btn-info waves-effect waves-light" data-dismiss="modal">Ok</button>
        </div>
      </div>
    </div>
  </div>







@endsection
