@extends('layouts.main')

@section('title', "$data->course_name")
@section('title-link', "/course/$data->course_id")

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/view/courses">Courses</a></li>
    <li><a href="/course/{{ $data->course_id }}">{{ $data->course_name }}</a></li>
    <li><a href="/lecture/{{ $data->lecture_id }}">{{ $data->lecture_title }}</a></li>
  </ol>
@endsection


@section('content')

  <script src="/js/lecture.js"></script>

  <input type="hidden" id="lecture_id" value="{{ $data->lecture_id }}">
  <input type="hidden" id="section_id" value="{{ $data->section_id }}">
  <input type="hidden" id="course_id" value="{{ $data->course_id }}">
  <input type="hidden" id="free_course" value="{{ $data->free_course }}">


  @if(check_course_enrollment($data->course_id) == 'accepted' || Auth::user()->id == $data->tutor_id)

    <div class="row">
      <div class="col-md-3 col-lg-3">

        @if(Auth::user()->id == $data->tutor_id)
          <div class="card-box">

            <div class="btn-group" style="display: block">
              <button type="button" class="btn btn-block btn-lg btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                <span class="fa fa-plus"></span>&nbsp;Add Content
              </button>
              <ul class="dropdown-menu pointer" role="menu">
                <li><a data-toggle="modal" data-target="#text_content_modal">Text</a></li>
                <li><a data-toggle="modal" data-target="#media_content_modal">Media</a></li>
                <li><a data-toggle="modal" data-target="#onlineVideo_content_modal">Online Video</a></li>
                <li><a data-toggle="modal" data-target="#add_flashcard_modal">Flash Card</a></li>
              </ul>
            </div>

            <button class="btn btn-block btn-lg btn-default waves-effect waves-light m-t-10" data-toggle="modal" data-target="#edit_lecture_modal">
              <i class="fa fa-edit"></i><span>&nbsp; Edit Lecture</span>
            </button>

            <a href="/add/quiz/{{ $data->lecture_id }}" class="btn btn-block btn-lg btn-inverse waves-effect waves-light m-t-10">
              <i class="fa fa-plus"></i><span>&nbsp; Add Quiz</span>
            </a>

            <button class="btn btn-block btn-lg btn-danger waves-effect waves-light m-t-10" id="delete_lecture">
              <i class="fa fa-warning"></i><span>&nbsp; Delete</span>
            </button>

          </div>
        @endif

        <div class="profile-detail card-box">
          <div>

            <div class="panel-group" id="section_div">

              @php $section_count=1; @endphp
              @php $lecture_count=1; @endphp

              @if(count($sections) == 0)
                <div class="panel panel-default" id="no_section_msg_div">
                  <div class="panel-heading">
                    <h1 class="text-center">Please Add Section</h1>
                  </div>
                </div>

              @else
                @foreach($sections as $section)
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#section_div" href="#collapseOne-{{ $section_count }}">
                          Section #{{ $section_count }} {{$section->section_name!=''?': '.$section->section_name:''}}
                        </a>
                      </h4>
                    </div>

                    <div id="collapseOne-{{ $section_count++ }}" class="panel-collapse collapse in">
                      <div class="panel-body">

                        <ul class="list-unstyled" id="section_{{ $section->section_id }}">

                          @foreach($lectures as $lecture)

                            @if($lecture->section_id == $section->section_id)
                              <a class="list-group-item {{ $data->lecture_id==$lecture->lecture_id? 'active':'' }}" href="/lecture/{{ $lecture->lecture_id }}">
                                <li>
                                  <i class="fa fa-play-circle-o"></i>
                                  Lecture-{{ $lecture_count++ }}: {{ $lecture->lecture_title }}
                                </li>
                              </a>
                            @endif

                          @endforeach

                        </ul>

                      </div>
                    </div>
                  </div>
                @endforeach

              @endif

            </div>

            <hr>
            <h4 class="text-uppercase font-600">About Me</h4>
            <p class="text-muted font-13 m-b-30">
              {{ $data->user_description }}
            </p>

            <div class="text-left">
              <p class="text-muted font-13"><strong>Instructor Name :</strong> <span class="m-l-15">{{ $data->instructor_name }}</span></p>

              <p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15">{{ $data->instructor_phone }}</span></p>

              <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">{{ $data->instructor_email }}</span></p>

            </div>

          </div>

        </div>


        <div class="card-box">
          <h4 class="header-title m-b-30"><b>Your Progress</b></h4>

          @php $lecture_completed=getLectureProgress(Auth::user()->id, $data->lecture_id); @endphp

          <div class="progress progress-lg m-b-15">
            <div class="progress-bar progress-bar-pink wow animated progress-animated" role="progressbar" aria-valuenow="{{ $lecture_completed }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $lecture_completed }}%;">
              {{ $lecture_completed }}%
            </div>
          </div>

        </div>

      </div>


      <div class="col-lg-9 col-md-9">

        <div class="card-box">
          <h2><b>{{ $data->lecture_title }}</b></h2>
          @if($data->media != Null)
            @php $media = get_video_link($data->media); @endphp

            @if($media[0] == 'vimeo')
              <div class="embed-responsive embed-responsive-16by9 m-t-30">
                <iframe class="embed-responsive-item" src="http://player.vimeo.com/video/{{ $media[1] }}?title=0&amp;byline=0&amp;"></iframe>
              </div>
            @endif

          @else
            {{--<h4 class="text-dark text-center">No Video Content</h4>--}}
          @endif
        </div>



        @if($data->lecture_description != Null)
          <div class="card-box">
            <div class="row">
              <div class="col-sm-12">
                <h2><b>Description</b></h2>
                <h4 class="m-t-30">{{ $data->lecture_description }}</h4>
              </div>
            </div>
          </div>
        @endif



        <div id="lecture_content_div">

          @foreach($contents as $content)


            <div class="card-box" id="lecture_content_{{ $content->content_id }}">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">

                  @if ($content->content_title != Null)
                    <h2><b>{{ $content->content_title }}</b></h2>
                  @endif


                  @if(Auth::user()->id == $data->tutor_id)

                    <button class="btn btn-instagram waves-effect waves-light"
                            onclick="edit_content('{{ $content->content_id }}', '{{ $content->mime_type }}', '{{ $content->content_title }}', '{{ preg_replace("/\s+/", " ", $content->content) }}');">
                      <span class="btn-label"><i class="ion-edit"></i></span>
                      Edit
                    </button>

                    <button class="btn btn-youtube waves-effect waves-light" onclick="delete_content({{ $content->content_id }})">
                      <span class="btn-label"><i class="ion-close"></i></span>
                      Delete
                    </button>

                  @endif


                  @if($content->mime_type == 'text/plain')
                    <h4 class="m-t-30">{{ $content->content }}</h4>

                  @elseif(strpos($content->mime_type, 'image') === 0)
                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 m-t-30">
                        <img src="/media/{{ $content->content }}" alt="{{ $content->content_title }}" class="img-responsive img-div">
                      </div>
                    </div>

                  @elseif(strpos($content->mime_type, 'video') === 0)
                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 m-t-30">
                        <video width="100%" controls>
                          <source src="/media/{{ $content->content }}" type="{{ $content->mime_type }}">
                        </video>
                      </div>
                    </div>

                  @elseif(strpos($content->mime_type, 'vimeo.com') === 0)
                    <div class="embed-responsive embed-responsive-16by9 m-t-30">
                      <iframe class="embed-responsive-item" src="http://player.vimeo.com/video/{{ get_video_link($content->content, $content->mime_type) }}?title=0&amp;byline=0&amp;"></iframe>
                    </div>


                  @elseif(strpos($content->mime_type, 'youtube.com') === 0)
                    <div class="embed-responsive embed-responsive-16by9 m-t-30">
                      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ get_video_link($content->content, $content->mime_type) }}" frameborder="0" allow="autoplay; encrypted-media"></iframe>
                    </div>

                  @endif
                </div>
              </div>
            </div>


          @endforeach




          @if(count($content_flashcard)>0)

            <div class="card-box">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 text-center div-to-center">

                  @foreach($content_flashcard as $flashcard)
                    <div class="ObjectContainer pointer"
                         onclick="flashcard_view('{{ $flashcard->flashcard_title }}', '{{ $flashcard->flashcard_description }}','{{ $flashcard->media }}')">
                      <div class="Object div_1">{{ $flashcard->flashcard_title }}</div>
                    </div>
                  @endforeach

                </div>
              </div>
            </div>

          @endif



        </div>


        @foreach($quizzes as $quiz)
          <div class="card-box" id="quiz_{{ $quiz->quiz_id }}">
            <div class="row">
              <div class="col-sm-6">
                <div class="product-right-info">
                  <h2><b>{{ $quiz->quiz_title }}</b></h2>
                  <h5 class="font-600 m-t-30">{{ $quiz->quiz_description }}</h5>
                  <div class="m-t-30">
                    <button class="btn btn-success waves-effect waves-light btn-lg" onclick="start_quiz({{ $quiz->quiz_id }})">Start Quiz</button>
                    @if(Auth::user()->id == $data->tutor_id)
                      <button class="btn btn-primary waves-effect waves-light btn-lg" onclick="edit_quiz({{ $quiz->quiz_id }})">Edit Quiz</button>
                      <button class="btn btn-danger waves-effect waves-light btn-lg" onclick="delete_quiz({{ $quiz->quiz_id }})">Delete Quiz</button>
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="sp-wrap sp-non-touch" style="display: inline-block;">
                  <div class="sp-large">
                    <img class="img-rounded pull-right" src="/images/assets/quiz.jpg" alt="" width="80%">
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endforeach

        <form id="start_quiz_form" action="/quiz" method="POST" style="display: none;">
          {{ csrf_field() }}
          <input type="hidden" name="quiz_id" id="quiz_id" value="0">
        </form>


        <form id="edit_quiz_form" action="/edit/quiz" method="POST" style="display: none;">
          {{ csrf_field() }}
          <input type="hidden" name="quiz_id" id="edit_quiz_id" value="0">
        </form>





      </div>

    </div>


    <div id="edit_lecture_modal" class="modal fade" role="dialog" style="padding-right: 17px;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Edit Lecture</h4>
          </div>
          <div class="modal-body">

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="field-3" class="control-label">Select Section</label>

                  @php $section_count=1; @endphp

                  <select class="form-control" id="select_section_div">

                    @foreach($sections as $section)
                      <option value="{{$section->section_id}}" {{ $data->section_id==$section->section_id?'selected':'' }} >Section #{{ $section_count++ }}: {{$section->section_name}}</option>
                    @endforeach

                  </select>

                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="field-3" class="control-label">Lecture Title</label>
                  <input type="text" class="form-control" id="lecture_title" value="{{ $data->lecture_title }}" autofocus>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="field-3" class="control-label">Lecture Description</label>
                  <input type="text" class="form-control" id="lecture_description" value="{{ $data->lecture_description }}">
                </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info waves-effect waves-light" id="edit_lecture_button" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>


    <div id="text_content_modal" class="modal fade" role="dialog" style="padding-right: 17px;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Add Text Content</h4>
          </div>
          <div class="modal-body">

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="field-3" class="control-label">Title</label>
                  <input type="text" class="form-control" id="content_title">
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="field-3" class="control-label">Text Content</label>
                  <textarea class="form-control" id="content" rows="10" autofocus></textarea>
                </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info waves-effect waves-light" id="text_content_button" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>


    <div id="edit_text_content_modal" class="modal fade" role="dialog" style="padding-right: 17px;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Edit Text Content</h4>
          </div>
          <div class="modal-body">

            <input type="hidden" id="edit_content_id">

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="field-3" class="control-label">Title</label>
                  <input type="text" class="form-control" id="edit_content_title">
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="field-3" class="control-label">Text Content</label>
                  <textarea class="form-control" id="edit_content" rows="10" autofocus></textarea>
                </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info waves-effect waves-light" id="edit_text_content_button" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>


    <div id="media_content_modal" class="modal fade" role="dialog" style="padding-right: 17px;">
      <div class="modal-dialog">

        <form role="form" action="/add/media/content" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}

          <input type="hidden" name="lecture_id" value="{{ $data->lecture_id }}">
          <input type="hidden" name="course_id" value="{{ $data->course_id }}">

          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title">Add Media Content</h4>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="field-3" class="control-label">Title</label>
                    <input type="text" class="form-control" name="content_title">
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="field-3" class="control-label">Content</label>
                    <input type="file" class="form-control" name="content" required>
                  </div>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-info waves-effect waves-light">OK</button>
            </div>
          </div>
        </form>


      </div>
    </div>


    <div id="edit_media_content_modal" class="modal fade" role="dialog" style="padding-right: 17px;">
      <div class="modal-dialog">

        <form role="form" action="/edit/media/content" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}

          <input type="hidden" name="lecture_id" value="{{ $data->lecture_id }}">
          <input type="hidden" name="content_id" id="media_content_id">

          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title">Edit Media Content</h4>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="field-3" class="control-label">Title</label>
                    <input type="text" class="form-control" name="content_title" id="edit_media_content_title">
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="field-3" class="control-label">Content</label>
                    <input type="file" class="form-control" name="content">
                  </div>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-info waves-effect waves-light">OK</button>
            </div>
          </div>
        </form>


      </div>
    </div>


    <div id="onlineVideo_content_modal" class="modal fade" role="dialog" style="padding-right: 17px;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Add Online Video Content</h4>
          </div>
          <div class="modal-body">

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="field-3" class="control-label">Title</label>
                  <input type="text" class="form-control" id="onlineVideo_content_title">
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="field-3" class="control-label">Text Content</label>
                  <input type="text" class="form-control" id="onlineVideo_content" autofocus>
                </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info waves-effect waves-light" id="onlineVideo_content_button" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>


    <div id="edit_onlineVideo_content_modal" class="modal fade" role="dialog" style="padding-right: 17px;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Edit Online Video Content</h4>
          </div>
          <div class="modal-body">

            <input type="hidden" id="onlineVideo_content_id">

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="field-3" class="control-label">Title</label>
                  <input type="text" class="form-control" id="edit_onlineVideo_content_title">
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="field-3" class="control-label">Text Content</label>
                  <input type="text" class="form-control" id="edit_onlineVideo_content" autofocus>
                </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info waves-effect waves-light" id="edit_onlineVideo_content_button" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>


    <div id="add_flashcard_modal" class="modal fade" role="dialog" style="padding-right: 17px;">
      <div class="modal-dialog">

        <form role="form" action="/add/flashcard/content" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}

          <input type="hidden" name="lecture_id" value="{{ $data->lecture_id }}">

          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title">Add Flashcard</h4>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="field-3" class="control-label">Title</label>
                    <input type="text" class="form-control" name="flashcard_title">
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="field-3" class="control-label">Description</label>
                    <input type="text" class="form-control" name="flashcard_description">
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="field-3" class="control-label">Content</label>
                    <input type="file" class="form-control" name="media" required>
                  </div>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-info waves-effect waves-light">OK</button>
            </div>
          </div>
        </form>


      </div>
    </div>


    <div id="flash_card_modal" class="modal tada animated" role="dialog" style="padding-right: 17px;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="fc_title">Modal Heading</h4>
          </div>

          <div class="modal-body">
            <h4 id="fc_description"></h4>
            <hr>
            <div class="form-group">
              <img id="fc_image" src="" width="100%">
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>


  @else

    <div class="row">

      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card-box">
          <h2 class="text-center"><b>Join this course to view lecture contents</b></h2>

          <div class="div-to-center m-t-40" id="course_enrollment_div">

            @if(check_course_enrollment($data->course_id) == 'requested')
              <button type="button" class="btn btn-warning waves-effect waves-light m-l-10">
                        <span class="btn-label">
                        <i class="glyphicon glyphicon-ok"></i>
                        </span>
                You have requested for this course
              </button>


            @elseif(check_course_enrollment($data->course_id) == 'null')
              <button type="button" class="btn btn-danger waves-effect waves-light m-l-10" id="join_course_button">
                        <span class="btn-label">
                        <i class="fa fa-shopping-cart"></i>
                        </span>
                Join Course
              </button>

            @endif

          </div>



        </div>
      </div>
    </div>

  @endif




@endsection
