@extends('layouts.main')

@section('title', 'Enrolled Courses')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/">3Scholars</a></li>
        <li><a href="/dashboard">Dashboard</a></li>
        <li><a href="/enrolled-courses">Enrolled Courses</a></li>
    </ol>
@endsection

@section('content')

    <script src="/js/student-dashboard.js"></script>

    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="portlet">
                <div class="portlet-heading">
                    <h3 class="text-dark header-title m-t-30 m-b-20">Enrolled Courses</h3>
                    <div class="clearfix"></div>
                </div>
                <div id="portlet2" class="panel-collapse collapse in" aria-expanded="true" style="">
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Course Name</th>
                                    <th>Start Date</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @php $course_count=1; @endphp

                                @foreach($courses as $course)

                                    <tr id="course_student_{{ $course->course_student_id }}">
                                        <td>{{ $course_count++ }}</td>
                                        <td><a href="/course/{{ $course->course_id }}">{{ $course->course_name }}</a></td>
                                        <td>{{ date('F j, Y', strtotime( $course->started_date )) }}</td>
                                        <td>{{ $course->free_course==0?$course->price:'Free' }}</td>
                                        <td>
                                            <button type="button" class="btn btn-youtube waves-effect waves-light" onclick="delete_enrollment({{ $course->course_student_id }})"><i class="ion-close"></i></button>
                                        </td>
                                    </tr>

                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->

    </div>




@endsection
