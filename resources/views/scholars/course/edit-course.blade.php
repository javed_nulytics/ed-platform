@extends('layouts.main')
@section('title', 'Edit Course')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/view/courses">Courses</a></li>
    <li><a href="/course/{{ $course->course_id }}">{{ $course->course_name }}</a></li>
    <li><a href="">Edit</a></li>
  </ol>
@endsection

@section('content')

  <div class="row">
    <div class="col-sm-12">
      <div class="card-box">

        <div class="row">
          <div class="col-md-12">

            <form class="form-horizontal" action="/edited/course" method="post" enctype="multipart/form-data">
              <h1 class="text-center">{{ $course->course_name }}</h1>
              {{--<h4 class="text-center">Instructor: {{ $course->name }}</h4>--}}
              <h4 class="text-center">Created At: {{ date('F j, Y', strtotime( $course->created_at )) }}</h4>
              <h4 class="text-center">Status: {{ $course->status==1?'Active':'Inactive' }}</h4>

              {{ csrf_field() }}
              <input type="hidden" name="course_id" id="course_id" value="{{ $course->course_id }}">
              <input type="hidden" name="redirect_url" id="redirect_url" value="{{ $redirect_url }}">



              <div class="form-group top-margin-30">
                <label class="col-sm-2 control-label">Instructor</label>
                <div class="col-sm-8">
                  <select class="form-control" name="instructor">
                    @foreach($users as $user)
                      <option value="{{ $user->id }}" {{ $course->instructor==$user->id?'selected':'' }}>{{ $user->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Course Name *</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="course_name" value="{{ $course->course_name }}">
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Course Title</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="course_title" value="{{ $course->course_title }}">
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Course Overview</label>
                <div class="col-md-8">
                  <textarea class="form-control" rows="5" name="course_overview">{{ $course->course_overview }}</textarea>
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">What Student Will Learn</label>
                <div class="col-md-8">
                  <textarea class="form-control" rows="5" name="what_will_i_learn">{{ $course->what_will_i_learn }}</textarea>
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Course Requirement</label>
                <div class="col-md-8">
                  <textarea class="form-control" rows="5" name="course_requirement">{{ $course->course_requirement }}</textarea>
                </div>
              </div>



              <div class="form-group">
                <label class="col-sm-2 control-label">Category</label>
                <div class="col-sm-8">
                  <select class="form-control" name="category">
                    @foreach($catagories as $category)
                      <option value="{{ $category->category_id }}" {{ $course->category_id==$category->category_id?'selected':'' }}>{{ $category->category_name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-2 control-label">Course Price</label>
                <div class="col-md-8">
                  <input type="number" class="form-control" min="0" name="price" value="{{ $course->price }}">
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Course Price Discount</label>
                <div class="col-md-8">
                  <input type="number" class="form-control" min="0" name="discount" value="{{ $course->discount }}">
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Free Course</label>
                <div class="col-md-8 m-t-5">
                  <input type="checkbox" id="free_course" name="free_course" {{ $course->free_course==1?'checked':''}}>
                  <label class="text-danger m-l-5" for="free_course">Tick If You Want Make This Course Free</label>
                </div>
              </div>


              <div class="form-group">
                <label class="col-sm-2 control-label">Course Level</label>
                <div class="col-sm-8">
                  <select class="form-control" name="course_level">
                    @foreach($course_level as $level)
                      <option value="{{ $level->course_level_id }}" {{ $course->course_level==$level->course_level_id?'selected':'' }}>{{ $level->course_level_name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Course Image</label>
                <div class="col-md-4">
                  <input type="file" class="form-control" name="image" accept="image/*">
                </div>
                <div class="col-md-4">
                  <img src="/images/course/{{ $course->course_image }}" alt="image" class="img-responsive img-rounded" width="100%">
                </div>
              </div>

              <div class="top-margin-50">
                <button type="submit" class="btn btn-block btn-lg btn-success waves-effect waves-light">Edit</button>
              </div>



            </form>
          </div>


        </div>
      </div>
    </div>
  </div>

@endsection
