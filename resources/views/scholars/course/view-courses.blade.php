@extends('layouts.main')

@section('title', 'View All Courses')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/view/courses">Courses</a></li>
    <li><a href="/view/courses">All Courses</a></li>
  </ol>
@endsection


@section('content')


<script src="/js/settings.js"></script>

<form id="edit_course_form" action="/edit/course" method="POST" style="display: none;">
  {{ csrf_field() }}
  <input type="hidden" id="form_course_id" name="form_course_id" value="0">
  <input type="hidden" name="redirect_url" id="redirect_url" value="view/courses">
</form>


  <div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="card-box">
        <div class="portlet-heading">
          <h3 class="text-dark header-title m-t-15 m-b-20">Courses</h3>
          <div class="row">
            <div class="col-sm-6 text-xs-center m-t-15">
              <div class="form-group">
                <a class="btn btn-inverse m-b-20" href="/create-course">
                  <i class="fa fa-plus m-r-5"></i> Add New Course
                </a>

                {{--<a class="btn btn-warning m-b-20" href="/generate/course">--}}
                  {{--<i class="ion-alert-circled m-r-5"></i> Generate Random Users--}}
                {{--</a>--}}

              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div id="portlet2" class="panel-collapse collapse in" aria-expanded="true" style="">
          <div class="portlet-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Course Name</th>
                  <th>Start Date</th>
                  <th>Instructor</th>
                  <th>Category</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th>No of Student</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @php $course_count=1; @endphp

                @foreach($courses as $course)

                  <tr>
                    <td>{{ $course_count++ }}</td>
                    <td><a href="/course/{{ $course->course_id }}">{{ $course->course_name }}</a></td>
                    <td>{{ date('F j, Y', strtotime( $course->created_at )) }}</td>
                    <td><a href="/profile/{{ $course->id }}">{{ $course->name }}</a></td>
                    <td>{{ $course->category_name }}</td>
                    <td>{{ $course->price }}</td>
                    <td>{{ ucfirst($course->status_name) }}</td>
                    <td>{{ $course->no_student }}</td>
                    <td style="width: 10%">

                      <!-- <button type="button" class="btn btn-twitter waves-effect waves-light"><i class="ion-ios7-eye"></i></button> -->
                      <button type="button" onclick="editCourse({{$course->course_id}})" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button>
                      <button type="button" class="btn btn-youtube waves-effect waves-light" onclick="delete_course({{ $course->course_id }})"><i class="ion-close"></i></button>

                    </td>
                  </tr>

                @endforeach


                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- end col -->

  </div>



@endsection
