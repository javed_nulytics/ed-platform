@extends('layouts.main')

@section('title', 'Course')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/">3Scholars</a></li>
        <li><a href="/view/courses">Courses</a></li>
        <li><a href="/course/{{ $course->course_id }}">{{ $course->course_name }}</a></li>
    </ol>
@endsection


@section('content')

    <script src="/js/course.js"></script>

    <input type="hidden" id="course_id" value="{{ $course->course_id }}">
    <input type="hidden" id="free_course" value="{{ $course->free_course }}">

    <div class="row">
        <div class="col-xs-12">
            <div class="card-box product-detail-box">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="sp-loading" style="display: none;"><img src="/images/sp-loading.gif" alt=""><br>LOADING
                            IMAGES
                        </div>
                        <div class="sp-wrap sp-non-touch" style="display: inline-block;">

                            <div class="sp-large"><a href="/images/course/default.jpg" class=".sp-current-big">
                                    <img src="/images/course/{{ $course->course_image }}" alt="" width="100%">
                                </a></div></div>
                    </div>

                    <div class="col-sm-4">
                        <div class="product-right-info">
                            <h3><b>{{ $course->course_name }}</b></h3>
                            <div class="rating">
                                <ul class="list-inline">
                                    <li><a class="fa fa-star" href=""></a></li>
                                    <li><a class="fa fa-star" href=""></a></li>
                                    <li><a class="fa fa-star" href=""></a></li>
                                    <li><a class="fa fa-star" href=""></a></li>
                                    <li><a class="fa fa-star-o" href=""></a></li>
                                </ul>
                            </div>


                            <h2>
                                @if($course->free_course == 1)
                                    <b>Free</b>
                                @elseif($course->discount == 0)
                                    <b>৳ {{ $course->price }}</b>
                                @elseif($course->discount > 0 && $course->discount<$course->price)
                                    <b>৳{{ $course->discount }}</b><small class="text-muted m-l-10"><del>৳{{ $course->price }}</del> </small>
                                @else
                                    <b>৳{{ $course->price }}</b>
                                @endif
                            </h2>


                            <hr>

                            {{--<h5 class="font-600">Course Title</h5>--}}

                            <p class="text-muted">{{ $course->course_title }}</p>



                            <div class="m-t-30">



                                @guest
                                <a href="/login" type="button" class="btn btn-danger waves-effect waves-light m-l-10">
                  <span class="btn-label">
                  <i class="fa fa-shopping-cart"></i>
                  </span>
                                    Log In to Join Course
                                </a>

                                @else
                                    <span id="course_enrollment_div" class="button-list">
                    @if(Auth::user()->id == $course->instructor)

                                            <button class="btn btn-default waves-effect waves-light" id="edit_course_button">
                      <span class="btn-label">
                      <i class="glyphicon glyphicon-pencil"></i>
                      </span>
                        Edit Course
                      </button>

                                            <button class="btn btn-purple waves-effect waves-light" data-toggle="modal" data-target="#add_section_modal">
                      <span class="btn-label">
                      <i class="glyphicon glyphicon-plus"></i>
                      </span>
                        Add Section
                      </button>



                                            <button class="btn btn-inverse waves-effect waves-light" data-toggle="modal" data-target="#add_lecture_modal">
                      <span class="btn-label">
                      <i class="glyphicon glyphicon-plus"></i>
                      </span>
                        Add Lecture
                      </button>




                                            <a href="/view/course-request/{{ $course->course_id }}" class="btn btn-primary waves-effect waves-light">
                      <span class="btn-label">
                      <i class="glyphicon glyphicon-th-list"></i>
                      </span>
                        Student List
                      </a>



                                            <button class="btn btn-danger waves-effect waves-light" onclick="delete_course({{ $course->course_id }})">
                      <span class="btn-label">
                      <i class="glyphicon glyphicon-remove"></i>
                      </span>
                        Delete Course
                      </button>



                                            <form id="edit_course_form" action="/edit/course" method="POST" style="display: none;">
                        {{ csrf_field() }}
                                                <input type="hidden" name="form_course_id" name="form_course_id" value="{{ $course->course_id }}">
                        <input type="hidden" name="redirect_url" id="redirect_url" value="/course/{{ $course->course_id }}">
                      </form>

                                        @elseif(check_course_enrollment($course->course_id) == 'null')
                                            <button type="button" class="btn btn-danger waves-effect waves-light m-l-10" id="join_course_button">
                      <span class="btn-label">
                      <i class="fa fa-shopping-cart"></i>
                      </span>
                        Join Course
                      </button>

                                        @elseif(check_course_enrollment($course->course_id) == 'requested')
                                            <button type="button" class="btn btn-warning waves-effect waves-light m-l-10">
                      <span class="btn-label">
                      <i class="glyphicon glyphicon-ok"></i>
                      </span>
                        You have requested for this course
                      </button>


                                        @elseif(check_course_enrollment($course->course_id) == 'accepted')
                                            <button type="button" class="btn btn-success waves-effect waves-light m-l-10">
                      <span class="btn-label">
                      <i class="glyphicon glyphicon-ok"></i>
                      </span>
                        You are enrolled in this course
                      </button>

                                        @endif
                  </span>
                                    @endguest

                            </div>
                        </div>
                    </div>






                    <div class="col-lg-4 col-md-4 col-sm-4">

                        <div class="card-box">

                            <h4><b>Specifications:</b></h4>
                            <div class="table-responsive m-t-20">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td width="50%">Instructor</td>
                                        <td>{{ $course->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Category</td>
                                        <td>{{ $course->category_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Students</td>
                                        <td>{{ $course->student_count }}</td>
                                    </tr>
                                    <tr>
                                        <td>Timeline</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>Skill Level</td>
                                        <td>{{ $course->course_level_name }}</td>
                                    </tr>

                                    <tr>
                                        <td>FAQ</td>
                                        <td><a href="/course/faq/{{ $course->course_id }}">Click Here To See FAQ</a></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>

                        @auth

                        @if(Auth::user()->id == $course->instructor)
                            <div class="card-box">
                                <h4 class="m-t-0 m-b-20 header-title"><b>Students <span class="text-muted">({{ count($course_student) }})</span></b></h4>

                                <div class="friend-list text-center">

                                    @foreach($course_student as $student)

                                        <a href="#">
                                            <img src="/images/users/{{ $student->user_img }}" class="img-circle thumb-md" alt="{{ $student->name }}">
                                        </a>

                                    @endforeach

                                    <a href="/view/course-request/{{ $course->course_id }}">
                                        <span class="extra-number">+{{ count($course_student) }}</span>
                                    </a>
                                </div>
                            </div>
                        @endif

                        @endauth
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <ul class="nav nav-tabs navtab-bg nav-justified">
                <li class="active">
                    <a href="#course_overview_tab" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><i class="fa fa-home"></i></span>
                        <span class="hidden-xs">Course Overview</span>
                    </a>
                </li>
                <li class="">
                    <a href="#what_will_i_learn_tab" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">What I Will Learn</span>
                    </a>
                </li>
                <li class="">
                    <a href="#course_requirement_tab" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                        <span class="hidden-xs">Course Requirement</span>
                    </a>
                </li>

            </ul>
            <div class="tab-content">

                <div class="tab-pane active" id="course_overview_tab">
                    <p>{{ $course->course_overview }}</p>
                </div>

                <div class="tab-pane" id="what_will_i_learn_tab">
                    @php $what_will_i_learn = explode("\n", $course->what_will_i_learn); @endphp
                    @foreach($what_will_i_learn as $item)
                        <p><i class="ion-arrow-right-a"></i>&nbsp; &nbsp;{{ $item }}</p>

                    @endforeach
                </div>

                <div class="tab-pane" id="course_requirement_tab">

                    @php $course_requirement = explode("\n", $course->course_requirement); @endphp
                    @foreach($course_requirement as $item)
                        <p><i class="ion-arrow-right-a"></i>&nbsp; &nbsp;{{ $item }}</p>

                    @endforeach

                </div>

            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12">

            @php $section_count=1; @endphp
            @php $lecture_count=1; @endphp

            <div class="panel-group" id="section_div">

                @if(count($sections) == 0)
                    <div class="panel panel-default" id="no_section_msg_div">
                        <div class="panel-heading">
                            @guest
                            <h1 class="text-center">No Section</h1>
                            @else
                                <h1 class="text-center">Please Add Section</h1>
                                @endguest
                        </div>
                    </div>

                @else
                    @foreach($sections as $section)
                        <div class="panel panel-default" id="section_{{ $section->section_id }}">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#section_div" href="#collapseOne-{{ $section_count }}" aria-expanded="false" class="collapsed">
                                        Section #{{ $section_count }} {{$section->section_name!=''?': '.$section->section_name:''}}
                                        @auth
                                        @if(Auth::user()->id == $course->instructor)
                                            <i class="ion-edit" onclick="edit_section('{{ $section->section_id }}', '{{ $section->section_name }}')"></i>
                                            <i class="ion-close" onclick="delete_section('{{ $section->section_id }}')"></i>
                                        @endif
                                        @endauth
                                    </a>
                                </h4>
                            </div>

                            <div id="collapseOne-{{ $section_count++ }}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">

                                    <ul class="list-unstyled" style="margin-left: 20px" id="span_section_{{ $section->section_id }}">

                                        @foreach($lectures as $lecture)

                                            @if($lecture->section_id == $section->section_id)
                                                <a href="/lecture/{{ $lecture->lecture_id }}">
                                                    <li>
                                                        <i class="fa fa-play-circle-o"></i>
                                                        Lecture-{{ $lecture_count++ }}: {{ $lecture->lecture_title }}
                                                    </li>
                                                </a>
                                            @endif

                                        @endforeach

                                    </ul>

                                </div>
                            </div>
                        </div>
                    @endforeach

                @endif

            </div>

            <script type="application/javascript">
                <?php echo 'set_section_count("'.$section_count.'");'; ?>
                <?php echo 'set_lecture_count("'.$lecture_count.'");'; ?>
            </script>

        </div>
    </div>



    <div id="add_section_modal" class="modal fade" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">New Section</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Section Name</label>
                                <input type="text" class="form-control" id="modal_section_name" placeholder="Introduction" autofocus>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info waves-effect waves-light" id="add_section_button" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>


    <div id="edit_section_modal" class="modal fade" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Edit Section</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="edit_section_id">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Section Name</label>
                                <input type="text" class="form-control" id="edit_section_name" autofocus>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info waves-effect waves-light" id="edit_section_button" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>


    <div id="add_lecture_modal" class="modal fade" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">New Lecture</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Select Section</label>

                                @php $section_count=1; @endphp

                                <select class="form-control" id="select_section_div">

                                    @foreach($sections as $section)
                                        <option value="{{$section->section_id}}">Section #{{ $section_count++ }}: {{$section->section_name}}</option>
                                    @endforeach

                                </select>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Lecture Title</label>
                                <input type="text" class="form-control" id="lecture_title" placeholder="Introduction" autofocus>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info waves-effect waves-light" id="add_lecture_button" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>



@endsection
