@extends('layouts.main')

@section('title', 'View Course Requests')

@section('breadcrumb')

    @if ($course_id == 0)
        <ol class="breadcrumb">
            <li><a href="/">3Scholars</a></li>
            <li><a href="/dashboard">Dashboard</a></li>
            <li><a href="/view/course-request">Course Requests</a></li>
        </ol>
    @else
        <ol class="breadcrumb">
            <li><a href="/">3Scholars</a></li>
            <li><a href="/view/courses">Courses</a></li>
            <li><a href="/course/{{ $course->course_id }}">{{ $course->course_name }}</a></li>
            <li><a href="/view/course-request">Course Requests</a></li>
        </ol>
    @endif
@endsection


@section('content')


    <script src="/js/settings.js"></script>


    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card-box">
                <div class="portlet-heading">
                    <h3 class="text-dark header-title m-t-15 m-b-20">Course Requests</h3>
                    <div class="row">
                        <div class="col-sm-6 text-xs-center m-t-15">
                            <div class="form-group">

                                <button class="btn btn-warning waves-effect waves-light w-lg m-b-20" id="clear_request_button">
                                    <i class="ion-alert-circled m-r-5"></i> Clear All Request
                                </button>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="portlet2" class="panel-collapse collapse in" aria-expanded="true" style="">
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Student Name</th>
                                    <th>Course Name</th>
                                    <th>Requested At</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="course_request_table">

                                @php $request_count=1; @endphp

                                @foreach($course_requests as $request)

                                    @if($course_id == 0 || $course_id == $request->course_id)

                                        <tr id="course_student_{{ $request->course_student_id }}">
                                            <td>{{ $request_count++ }}</td>
                                            <td><a href="/profile/{{ $request->student_id }}">{{ $request->name }}</a></td>
                                            <td><a href="/course/{{ $request->course_id }}">{{ $request->course_name }}</a></td>
                                            <td>{{ date('F j, Y', strtotime( $request->created_at )) }}</td>
                                            <td>
                                                @if($request->status_name == 'accepted')
                                                    <button class="btn btn-success waves-effect waves-light">{{ ucfirst($request->status_name) }}</button>
                                                @elseif($request->status_name == 'requested')
                                                    <button id="cs_button_{{ $request->course_student_id }}" onclick="course_request('{{ $request->course_student_id }}', '{{ $request->status_name }}')" class="btn btn-warning waves-effect waves-light">{{ ucfirst($request->status_name) }}</button>
                                                @endif
                                            </td>

                                            <td>
                                                <button type="button" class="btn btn-youtube waves-effect waves-light" onclick="delete_enrollment({{ $request->course_student_id }})"><i class="ion-close"></i></button>
                                            </td>
                                        </tr>

                                    @endif

                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



@endsection
