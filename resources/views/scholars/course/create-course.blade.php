@extends('layouts.main')

@section('title', 'Create Course')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/view/courses">Courses</a></li>
    <li><a href="/create-course">Create Course</a></li>
  </ol>
@endsection


@section('content')

  <div class="row">
    <div class="col-sm-12">
      <div class="card-box">

        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="create-course" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}

              <div class="form-group">
                <label class="col-md-2 control-label">Course Name *</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" placeholder="WordPress for Beginners" name="course_name" required>
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Course Title</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" placeholder="Easy and step by step guide to create and develop a website using WordPress." name="course_title">
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Course Overview</label>
                <div class="col-md-8">
                  <textarea class="form-control" rows="5" name="course_overview"></textarea>
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">What Student Will Learn</label>
                <div class="col-md-8">
                  <textarea class="form-control" rows="5" name="what_will_i_learn"></textarea>
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Course Requirement</label>
                <div class="col-md-8">
                  <textarea class="form-control" rows="5" name="course_requirement"></textarea>
                </div>
              </div>


              <div class="form-group">
                <label class="col-sm-2 control-label">Category *</label>
                <div class="col-sm-8">
                  <select class="form-control" name="category" required>
                    @foreach($catagories as $category)
                      <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-2 control-label">Course Price *</label>
                <div class="col-md-8">
                  <input type="number" class="form-control" placeholder="5000" min="0" name="price" value="0">
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Course Price Discount</label>
                <div class="col-md-8">
                  <input type="number" class="form-control" placeholder="1000" min="0" name="discount" value="0">
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Free Course</label>
                <div class="col-md-8 m-t-5">
                  <input type="checkbox" id="free_course" name="free_course">
                  <label class="text-danger m-l-5" for="free_course">Tick If You Want Make This Course Free</label>
                </div>
              </div>


              <div class="form-group">
                <label class="col-sm-2 control-label">Course Level *</label>
                <div class="col-sm-8">
                  <select class="form-control" name="course_level" required>
                    @foreach($course_level as $level)
                      <option value="{{ $level->course_level_id }}">{{ $level->course_level_name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Course Image *</label>
                <div class="col-md-8">
                  <input type="file" class="form-control" name="image" accept="image/*">
                </div>
              </div>

              <div class="top-margin-50">
                <button type="submit" class="btn btn-block btn-lg btn-success waves-effect waves-light">Create Course</button>
              </div>



            </form>
          </div>


        </div>
      </div>
    </div>
  </div>

@endsection
