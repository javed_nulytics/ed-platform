@extends('layouts.main')

@section('title', 'View All Users')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="">Settings</a></li>
    <li><a href="">Users</a></li>
  </ol>
@endsection


@section('content')

  <script src="/js/settings.js"></script>

  @foreach($role_lookup as $role)
    <script type="application/javascript">
        @php echo 'pushToRoleArray("'.$role->role_name.'");'; @endphp
    </script>
  @endforeach


  <div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="card-box">

        <div id="portlet2" class="panel-collapse collapse in" aria-expanded="true" style="">

          <div class="portlet-body">
            <h3 class="text-dark header-title m-t-15 m-b-20">Users</h3>
            <div class="row">
              <div class="col-sm-6 text-xs-center m-t-15">
                <div class="form-group">
                  <button class="btn btn-inverse m-b-20" href="#add_user_modal" data-animation="fadein" data-plugin="custommodal" data-overlayspeed="200" data-overlaycolor="#36404a">
                    <i class="fa fa-plus m-r-5"></i> Add New User
                  </button>

                  <a class="btn btn-warning m-b-20" href="/generate/user">
                    <i class="ion-alert-circled m-r-5"></i> Generate Random Users
                  </a>

                </div>
              </div>
            </div>

            <div class="table-responsive">
              <table class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th></th>
                  <th>User Name</th>
                  <th>Email</th>
                  <th></th>
                  <th>Role</th>
                  <th>Status</th>
                  <th>Joined</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="user_table">

                @php $user_count=1; @endphp

                @foreach($users as $user)

                  <tr id="user_{{ $user->id }}">
                    <td>{{ $user_count++ }}</td>
                    <td><img src="/images/users/{{ $user->user_img }}" alt="image" class="img-responsive thumb-md img-circle"></td>
                    <td><a href="/profile/{{ $user->id }}">{{ $user->name }}</a></td>
                    <td>{{ $user->email }}</td>
                    <td>
                      <button type="button" href="#edit_password_modal" onclick="change_password('{{ $user->id }}')" data-animation="fadein" data-plugin="custommodal" data-overlayspeed="200" data-overlaycolor="#36404a" class="btn btn-warning btn-custom waves-effect waves-light" {{ $user->email=='superuser@scholars.com'?'disabled':'' }}>Change Password</button>
                    </td>
                    <td>

                      <select class="form-control user-role" {{ $user->email=='superuser@scholars.com'?'disabled':'' }}>
                        @foreach($role_lookup as $role)
                          <option value="{{$role->role_name.'-'.$user->id}}" {{ $role->role_name==$user->role?'selected':'' }}>{{ ucfirst($role->role_name) }}</option>
                        @endforeach
                      </select>

                    </td>

                    <td>{{ ucfirst($user->status_name) }}</td>
                    <td>{{ date('F j, Y', strtotime( $user->created_at )) }}</td>
                    <td style="width: 14%">

                      <a href="/profile/{{ $user->id }}" class="btn btn-twitter waves-effect waves-light"><i class="ion-ios7-eye"></i></a>
                      <button type="button" class="btn btn-instagram waves-effect waves-light"
                              onclick="edit_user('{{$user->id}}', '{{$user->name}}', '{{$user->email}}', '{{$user->role}}', '{{ trim($user->user_description)}}')" {{ $user->email=='superuser@scholars.com'?'disabled':'' }}>
                        <i class="ion-edit"></i>
                      </button>
                      <button type="button" onclick="delete_user({{ $user->id }})" class="btn btn-youtube waves-effect waves-light" {{ $user->email=='superuser@scholars.com'?'disabled':'' }}>
                        <i class="ion-close"></i>
                      </button>

                    </td>
                  </tr>

                @endforeach


                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>



  <div id="add_user_modal" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
      <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Add New User</h4>
    <div class="custom-modal-text text-left">

      <div role="form">


        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" id="name" required autofocus>
        </div>

        <div class="form-group">
          <label for="email" class="control-label">Email</label>
          <input type="email" class="form-control" id="email" required>
        </div>

        <div class="form-group">
          <label for="password" class="control-label">Password</label>
          <input type="text" class="form-control" id="password">
        </div>

        <div class="form-group">
          <label for="password" class="control-label">Role</label>
          <select class="form-control" id="role_selected">
            @foreach($role_lookup as $role)
              <option value="{{ $role->role_name }}">{{ ucfirst($role->role_name) }}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group no-margin">
          <label for="field-7" class="control-label">Personal Info</label>
          <textarea class="form-control autogrow" id="user_description" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
        </div>


        <button type="button" class="btn btn-default waves-effect waves-light" id="add_user_button" onclick="Custombox.close();">Save</button>
        <button type="button" class="btn btn-danger waves-effect waves-light m-l-10" onclick="Custombox.close();">Cancel</button>
      </div>

    </div>
  </div>


  <div id="edit_user_modal" class="modal fadein" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Edit User</h4>
        </div>
        <div class="modal-body">

          <input type="hidden" id="edit_user_id">

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="edit_name">Name</label>
                <input type="text" class="form-control" id="edit_name" autofocus>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="edit_email" class="control-label">Email</label>
                <input type="email" class="form-control" id="edit_email" disabled>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="password" class="control-label">Role</label>
                <select class="form-control" id="edit_role_selected">
                  @foreach($role_lookup as $role)
                    <option value="{{ $role->role_name }}">{{ ucfirst($role->role_name) }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-md-12">
              <div class="form-group no-margin">
                <label for="field-7" class="control-label">Personal Info</label>
                <textarea class="form-control autogrow" id="edit_user_description" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
              </div>
            </div>
          </div>




        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-info waves-effect waves-light" id="edit_user_button" data-dismiss="modal">Save</button>
        </div>
      </div>
    </div>
  </div>


  <div id="edit_password_modal" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
      <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Change Password</h4>
    <div class="custom-modal-text text-left">

      <div role="form">

        <input type="hidden" id="new_password_user_id">

        <div class="form-group">
          <label>New Password</label>
          <input type="password" class="form-control" id="new_password">
        </div>


        <div class="form-group">
          <label>Retype Password</label>
          <input type="password" class="form-control" id="retype_password">
        </div>




        <button type="button" id="edit_password_button" class="btn btn-default waves-effect waves-light" onclick="Custombox.close();">Save</button>
        <button type="button" class="btn btn-danger waves-effect waves-light m-l-10" onclick="Custombox.close();">Cancel</button>
      </div>

    </div>
  </div>






@endsection
