@extends('layouts.main')

@section('title', 'View Quizzes')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/view/courses">Courses</a></li>
    <li><a href="/view/quizzes">Quizzes</a></li>
  </ol>
@endsection



@section('content')

  <script src="/js/quiz.js"></script>



  <div class="row">
    <div class="col-sm-12">

      <div class="form-horizontal">

        <div class="card-box">
          <div class="row">
            <div class="col-md-12">

              <div class="form-group">
                <label class="col-md-2 control-label">Course</label>
                <div class="col-md-8">
                  <select class="form-control select2" name="course_id">
                    <option value="0">Select A Course</option>
                    @foreach($courses as $course)
                      <option value="{{ $course->course_id }}">{{ $course->course_name }}</option>
                    @endforeach

                  </select>
                </div>
              </div>


            </div>
          </div>





          <button type="button" class="btn btn-block btn-lg btn-success waves-effect waves-light">Search Quiz</button>

        </div>

      </div>

    </div>
  </div>


  <div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="portlet">
        <div class="portlet-heading">

          <h3 class="text-dark header-title m-t-30 m-b-20">Quizzes</h3>
          <div class="clearfix"></div>
        </div>
        <div id="portlet2" class="panel-collapse collapse in" aria-expanded="true" style="">
          <div class="portlet-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Instructor</th>
                  <th>Course</th>
                  <th>Lecture</th>
                  <th>Status</th>
                  <th>Avg. Scores</th>
                  <th>Created At</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @php $quiz_count=1; @endphp

                @foreach($quizzes as $quiz)

                  <tr>
                    <td>{{ $quiz_count++ }}</td>
                    <td>{{ $quiz->quiz_title }}</td>
                    <td>{{ $quiz->quiz_description }}</td>
                    <td><a href="/profile/{{ $quiz->tutor_id }}">{{ $quiz->name }}</a></td>
                    <td>{{ $quiz->course_name }}</td>
                    <td>{{ $quiz->lecture_title }}</td>
                    <td>{{ $quiz->status }}</td>
                    <td></td>
                    <td>{{ date('F j, Y', strtotime( $quiz->created_at )) }}</td>
                    <td style="width: 10%">
                      <button type="button" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button>
                      <button type="button" class="btn btn-youtube waves-effect waves-light" onclick="delete_quiz({{ $quiz->quiz_id }})"><i class="ion-close"></i></button>
                    </td>
                  </tr>

                @endforeach


                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

@endsection
