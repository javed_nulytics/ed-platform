@extends('layouts.main')

@section('title', 'Quiz Result')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/view/exams">Exam</a></li>
    <li><a href="">Result</a></li>
  </ol>
@endsection


@section('content')

  <div class="row">
    <div class="col-sm-12">
      <div class="card-box widget-inline">
        <div class="row">
          <div class="col-lg-3 col-sm-6">
            <div class="widget-inline-box text-center">
              <h3><i class="text-primary glyphicon glyphicon-stats"></i> <b data-plugin="counterup">{{ $exam_data->score }}</b></h3>
              <h4 class="text-muted">Score</h4>
            </div>
          </div>

          <div class="col-lg-3 col-sm-6">
            <div class="widget-inline-box text-center">
              <h3>Answered <b data-plugin="counterup">{{ $exam_data->answered_question }}</b> out of <b data-plugin="counterup">{{ $exam_data->total_question }}</b></h3>
              <h4 class="text-muted">Questions</h4>
            </div>
          </div>

          <div class="col-lg-3 col-sm-6">
            <div class="widget-inline-box text-center">
              <h3><i class="text-primary glyphicon glyphicon-thumbs-up"></i> <b data-plugin="counterup">{{ $exam_data->percentile }}</b></h3>
              <h4 class="text-muted">Percentile</h4>
            </div>
          </div>

          <div class="col-lg-3 col-sm-6">
            <div class="widget-inline-box text-center b-0">
              <h3><i class="text-primary glyphicon glyphicon-calendar"></i> {{ date('F j, Y G:ia', strtotime( $exam_data->created_at )) }}</h3>
              <h4 class="text-muted">Exam Given Date</h4>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-lg-12">

      @php $question_count=1; @endphp
      @php $q_count=0; @endphp

      @foreach($questions as $question)


        <div class="card-box m-b-10">
          <div class="table-box opport-box">

            <div class="col-md-8">

              <h4 class="m-t-0 header-title m-b-30">
                <b>{{ $question_count++ }}. {{ $question->question }}</b>
              </h4>

              @if($question->question_type == 'text')
                <div class="form-group m-t-30">
                  <div class="col-md-8 btn-success p-t-10" style="height: 50px; border-radius: 10px">
                    <b>Correct Answer: {{ $question->answer }}</b>
                  </div>
                </div>


              @elseif($question->question_type == 'mcq')

                <ul class="list-unstyled">

                  @if($question->option_1 != Null)
                    <li class="{{ $question->answer==$question->option_1?'btn-success':'' }} b-r-5"><i class="glyphicon glyphicon-record"></i> {{ $question->option_1 }}</li>
                  @endif

                  @if($question->option_2 != Null)
                    <li class="{{ $question->answer==$question->option_2?'btn-success':'' }} b-r-5"><i class="glyphicon glyphicon-record"></i> {{ $question->option_2 }}</li>
                  @endif

                  @if($question->option_3 != Null)
                    <li class="{{ $question->answer==$question->option_3?'btn-success':'' }} b-r-5"><i class="glyphicon glyphicon-record"></i> {{ $question->option_3 }}</li>
                  @endif

                  @if($question->option_4 != Null)
                    <li class="{{ $question->answer==$question->option_4?'btn-success':'' }} b-r-5"><i class="glyphicon glyphicon-record"></i> {{ $question->option_4 }}</li>
                  @endif

                </ul>

              @endif


              <br>

              <h4 class="m-t-15 col-md-8 p-t-10 {{ $question->answer==$question->submitted_answer?'btn-success':'btn-danger' }}" style="height: 50px; border-radius: 10px">
                <b>Your Answer: {{ $question->submitted_answer }}</b>
              </h4>




            </div>





            <div class="col-md-4">

              @if($question->question_media != Null)
                <div class="table-detail">
                  <img src="/images/course/default.jpg" alt="img" class="" width="100%">
                </div>
              @endif



            </div>

          </div>
        </div>

        @php $q_count++; @endphp
      @endforeach


    </div>
  </div>



@endsection
