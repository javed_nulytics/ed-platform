@extends('layouts.main')

@section('title', 'Create Quiz')


@section('content')

  <script src="/js/quiz.js"></script>



  <div class="row">
    <div class="col-sm-12">

      <form class="form-horizontal" action="/add/quiz/" method="post" enctype="multipart/form-data" id="quiz_form">
        {{ csrf_field() }}

        <input type="hidden" id="lecture_id" name="lecture_id" value="{{ $lecture->lecture_id }}">
        <input type="hidden" id="course_id" name="course_id" value="{{ $lecture->course_id }}">

        <div class="card-box">
          <div class="row">
            <div class="col-md-12">

              <div class="form-group">
                <label class="col-md-2 control-label">Quiz Name</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" placeholder="Quiz 3" name="quiz_title" value="Quiz {{ ++$lecture->quiz_count }}">
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-2 control-label">Quiz Description</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" placeholder="Identify your areas for growth in these lessons:" name="quiz_description" value="Identify your areas for growth in these lessons:">
                </div>
              </div>


              <div class="form-group">
                <div class="col-md-12 text-center m-t-15">

                  <div class="btn-group">
                    <button type="button" class="btn btn-success btn-rounded waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">
                      <span class="btn-label"><i class="fa fa-plus"></i></span>
                      Add Question &nbsp;<span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                      <li onclick="create_question('mcq')"><a href="#">MCQ</a></li>
                      <li onclick="create_question('text')"><a href="#">Text</a></li>
                    </ul>
                  </div>

                  <button type="button" class="btn btn-danger btn-rounded waves-effect waves-light" onclick="clear_question()">
                    <span class="btn-label"><i class="fa fa-times"></i>
                    </span>Clear All Question
                  </button>


                </div>

              </div>

            </div>
          </div>
        </div>




        <div id="quiz_div">


        </div>





        <div class="card-box">
          <div class="">
            <button type="button" class="btn btn-block btn-lg btn-success waves-effect waves-light" onclick="form_submit()">Create Quiz</button>
          </div>
        </div>

      </form>



    </div>
  </div>

@endsection
