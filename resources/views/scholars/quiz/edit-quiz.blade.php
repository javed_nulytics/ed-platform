@extends('layouts.main')

@section('title', 'Edit Quiz')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/view/courses">Courses</a></li>
    <li><a href="/course/{{ $quiz->course_id }}">{{ $quiz->course_name }}</a></li>
    <li><a href="/lecture/{{ $quiz->lecture_id }}">{{ $quiz->lecture_title }}</a></li>
    <li><a href="">{{ $quiz->quiz_title }}</a></li>
  </ol>
@endsection


@section('content')

  <script src="/js/quiz.js"></script>


  <div class="row">
    <div class="col-sm-12">

      <form class="form-horizontal" action="/update/quiz/" method="post" enctype="multipart/form-data" id="quiz_form">
        {{ csrf_field() }}

        <input type="hidden" name="quiz_id" value="{{ $quiz->quiz_id }}" >
        <input type="hidden" name="lecture_id" value="{{ $quiz->lecture_id }}" >


        <div class="card-box">
          <div class="row">
            <div class="col-md-12">

              <div class="form-group">
                <label class="col-md-2 control-label">Quiz Name</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" value="{{ $quiz->quiz_title }}" name="quiz_title">
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-2 control-label">Quiz Description</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" value="{{ $quiz->quiz_description }}" name="quiz_description">
                </div>
              </div>


              <div class="form-group">
                <div class="col-md-12 text-center m-t-15">

                  <div class="btn-group">
                    <button type="button" class="btn btn-success btn-rounded waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">
                      <span class="btn-label"><i class="fa fa-plus"></i></span>
                      Add Question &nbsp;<span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                      <li onclick="create_question('mcq')"><a href="#">MCQ</a></li>
                      <li onclick="create_question('text')"><a href="#">Text</a></li>
                    </ul>
                  </div>

                  <button type="button" class="btn btn-danger btn-rounded waves-effect waves-light" onclick="clear_question()">
                    <span class="btn-label"><i class="fa fa-times"></i>
                    </span>Clear All Question
                  </button>


                </div>

              </div>

            </div>
          </div>
        </div>




        <div id="quiz_div">

          @php $question_number=0; @endphp

          @foreach($questions as $question)

            @php $question_number++; @endphp

            @if($question->question_type == 'mcq')
              <div class="card-box" id="quiz_question_{{ $question->question_id }}">
                <input type="hidden" name="question_id[]" value="{{ $question->question_id }}">
                <input type="hidden" name="question_type[]" value="mcq">
                <div class="form-group">
                  <label class="col-md-2 control-label">Question {{ $question_number }}</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" name="question[]" value="{{ $question->question }}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Option 1</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" value="{{ $question->option_1 }}" id="q_{{ $question_number }}_o_1" name="option[]" onkeyup="lookup(this);">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Option 2</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" value="{{ $question->option_2 }}" id="q_{{ $question_number }}_o_2" name="option[]" onkeyup="lookup(this);">
                  </div>
                </div>
                <div class="form-group"><label class="col-md-2 control-label">Option 3</label><div class="col-md-8">
                    <input type="text" class="form-control" value="{{ $question->option_3 }}" id="q_{{ $question_number }}_o_3" name="option[]" onkeyup="lookup(this);">
                  </div>
                </div>
                <div class="form-group"><label class="col-md-2 control-label">Option 4</label><div class="col-md-8">
                    <input type="text" class="form-control" value="{{ $question->option_4 }}" id="q_{{ $question_number }}_o_4" name="option[]" onkeyup="lookup(this);">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Select Correct Answer</label>
                  <div class="col-md-8">
                    <select class="form-control" name="answer[]" id="select_answer_{{ $question_number }}">
                      <option value="{{ $question->option_1 }}" id="select_{{ $question_number }}_option_1" {{ $question->option_1==$question->answer?'selected':'' }}>
                        {{ $question->option_1 }}
                      </option>

                      <option value="{{ $question->option_2 }}" id="select_{{ $question_number }}_option_2" {{ $question->option_2==$question->answer?'selected':'' }}>
                        {{ $question->option_2 }}
                      </option>

                      <option value="{{ $question->option_3 }}" id="select_{{ $question_number }}_option_3" {{ $question->option_3==$question->answer?'selected':'' }}>
                        {{ $question->option_3 }}
                      </option>

                      <option value="{{ $question->option_4 }}" id="select_{{ $question_number }}_option_4" {{ $question->option_4==$question->answer?'selected':'' }}>
                        {{ $question->option_4 }}
                      </option>
                    </select>
                  </div>
                </div>
                <div class="form-group"><label class="col-md-2 control-label">Media</label><div class="col-md-8">
                    <input type="file" class="form-control" name="media_{{ $question_number }}" accept="image/*">
                  </div>
                </div>
                <div class="form-group"><label class="col-md-2 control-label">Explanation</label><div class="col-md-8">
                    <input type="text" class="form-control" name="explanation[]" value="{{ $question->explanation }}">
                  </div>
                </div>
                <div class="form-group"><label class="col-md-2 control-label">Explanation Media</label><div class="col-md-8">
                    <input type="file" class="form-control" name="explanation_media_{{ $question_number }}" accept="image/*">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12 text-center m-t-15">
                    <button type="button" onclick="delete_quiz_question('{{ $question->question_id }}')" class="btn btn-danger btn-rounded waves-effect waves-light m-l-10">
                      <span class="btn-label"><i class="fa fa-times"></i></span>Delete Question {{ $question_number }}
                    </button>
                  </div>
                </div>
              </div>

            @elseif($question->question_type == 'text')
              <div class="card-box" id="quiz_question_{{ $question->question_id }}">
                <input type="hidden" name="question_id[]" value="{{ $question->question_id }}">
                <input type="hidden" name="question_type[]" value="text">
                <div class="form-group">
                  <label class="col-md-2 control-label">Question {{ $question_number }}</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" value="{{ $question->question }}" name="question[]">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Answer</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" value="{{ $question->answer }}" name="answer[]">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Media</label><div class="col-md-8">
                    <input type="file" class="form-control" name="media_{{ $question_number }}" accept="image/*">
                  </div>
                </div>
                <div class="form-group"><label class="col-md-2 control-label">Explanation</label><div class="col-md-8">
                    <input type="text" class="form-control" name="explanation[]" value="{{ $question->explanation }}">
                  </div>
                </div>
                <div class="form-group"><label class="col-md-2 control-label">Explanation Media</label><div class="col-md-8">
                    <input type="file" class="form-control" name="explanation_media_{{ $question_number }}" accept="image/*">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12 text-center m-t-15">

                    <button type="button" onclick="delete_quiz_question('{{ $question->question_id }}')" class="btn btn-danger btn-rounded waves-effect waves-light m-l-10">
                      <span class="btn-label"><i class="fa fa-times"></i></span>
                      Delete Question {{ $question_number }}</button>
                  </div>
                </div>
              </div>
            @endif
          @endforeach


          <script type="application/javascript">
              <?php echo 'set_question_no("'.$question_number.'");'; ?>
          </script>


        </div>



        <div class="card-box">
          <div class="">
            <button type="button" class="btn btn-block btn-lg btn-success waves-effect waves-light" onclick="form_submit()">Edit Quiz</button>
          </div>
        </div>

      </form>



    </div>
  </div>



@endsection
