@extends('layouts.main')

@section('title', 'Quiz')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/view/courses">Courses</a></li>
    <li><a href="/course/{{ $quiz->course_id }}">{{ $quiz->course_name }}</a></li>
    <li><a href="/lecture/{{ $quiz->lecture_id }}">{{ $quiz->lecture_title }}</a></li>
    <li><a href="">{{ $quiz->quiz_title }}</a></li>
  </ol>
@endsection


@section('content')

  <script src="/js/quiz.js"></script>


  <input type="hidden" id="quiz_id" value="{{ $quiz_id }}">


  <div class="row">
    <div class="col-lg-12">

      @php $question_count=1; @endphp
      @php $q_count=0; @endphp

      @foreach($questions as $question)

        <script type="application/javascript">
            <?php echo 'pushToAnswerArray("'.$question->answer.'", "'.$question->question_type.'", "'.$question->question_id.'");'; ?>
        </script>

        <div class="card-box m-b-10">
          <div class="table-box opport-box">

            <div class="col-md-8">

              <h4 class="m-t-0 header-title m-b-30">
                <b>{{ $question_count++ }}. {{ $question->question }}</b>
              </h4>

              @if($question->question_type == 'text')
                <div class="form-group m-t-30">
                  <div class="col-md-8">
                    <input type="text" class="form-control" placeholder="Answer" name="question_{{ $q_count }}">
                  </div>
                </div>


              @elseif($question->question_type == 'mcq')

                @if($question->option_1 != Null)
                  <div class="radio radio-success">
                    <input type="radio" name="question_{{ $q_count }}" id="question_{{ $question->question_id }}" value="{{ $question->option_1 }}">
                    <label for="question_{{ $question->question_id }}">
                      {{ $question->option_1 }}
                    </label>
                  </div>
                @endif

                @if($question->option_2 != Null)
                  <div class="radio radio-success">
                    <input type="radio" name="question_{{ $q_count }}" id="question_{{ $question->question_id }}" value="{{ $question->option_2 }}">
                    <label for="question_{{ $question->question_id }}">
                      {{ $question->option_2 }}
                    </label>
                  </div>
                @endif

                @if($question->option_3 != Null)
                  <div class="radio radio-success">
                    <input type="radio" name="question_{{ $q_count }}" id="question_{{ $question->question_id }}" value="{{ $question->option_3 }}">
                    <label for="question_{{ $question->question_id }}">
                      {{ $question->option_3 }}
                    </label>
                  </div>
                @endif

                @if($question->option_4 != Null)
                  <div class="radio radio-success">
                    <input type="radio" name="question_{{ $q_count }}" id="question_{{ $question->question_id }}" value="{{ $question->option_4 }}">
                    <label for="question_{{ $question->question_id }}">
                      {{ $question->option_4 }}
                    </label>
                  </div>
                @endif

              @endif




            </div>
            <div class="col-md-4">

              @if($question->question_media != Null)
                <div class="table-detail">
                  <img src="/images/course/default.jpg" alt="img" class="" width="100%">
                </div>
              @endif



            </div>

          </div>
        </div>

        @php $q_count++; @endphp
      @endforeach


    </div>
  </div>


  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">

      <div class="m-t-20">
        <div class="">
          <button id="submit_quiz_button" type="button" class="btn btn-block btn-lg btn-success btn-rounded waves-effect waves-light">
            <i class="fa fa-rocket m-r-5"></i> Submit Answers
          </button>
        </div>
      </div>
      <!-- end card-box -->


    </div> <!-- end col-6 -->


  </div>


  <form id="view_result_form" action="view/quiz/result/" method="POST" style="display: none;">
    {{ csrf_field() }}
    <input type="hidden" id="quiz_result_id" name="quiz_result_id" value="0">
  </form>



@endsection
