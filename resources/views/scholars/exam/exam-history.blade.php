@extends('layouts.main')

@section('title', 'Exam Result')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/view/exams">Exam</a></li>
    <li><a href="">Result</a></li>
  </ol>
@endsection


@section('content')


  <div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="portlet">
        <div class="portlet-heading">
          <h3 class="text-dark header-title m-t-30 m-b-20">Exam Results</h3>
          <div class="clearfix"></div>
        </div>
        <div id="portlet2" class="panel-collapse collapse in" aria-expanded="true" style="">
          <div class="portlet-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Exam Title</th>
                  <th>Course</th>
                  <th>Answered</th>
                  <th>Score</th>
                  <th>Percentile</th>
                  <th>Exam Taken</th>
                  <th>View Result</th>
                </tr>
                </thead>
                <tbody>

                @php $exam_count=1; @endphp

                @foreach($exam_results as $result)

                  <tr>
                    <td>{{ $exam_count++ }}</td>
                    <td><span class="pointer">{{ $result->exam_title }}</span></td>
                    <td><a href="/course/{{ $result->course_id }}">{{ $result->course_name }}</a></td>
                    <td>{{ $result->answered_question }} out of {{ $result->total_question }}</td>
                    <td>{{ $result->score }}%</td>
                    <td>{{ $result->percentile }}%</td>
                    <td>{{ date('F j, Y', strtotime( $result->created_at )) }}</td>

                    <td class="div-to-center">
                      <a href="/exam/result/{{ $result->result_id }}" type="button" class="btn btn-instagram waves-effect waves-light"><i class="ion-ios7-eye"></i></a>
                    </td>
                  </tr>

                @endforeach


                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>



@endsection
