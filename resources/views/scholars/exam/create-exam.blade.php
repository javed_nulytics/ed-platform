@extends('layouts.main')

@section('title', 'Create Exam')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/view/exams">Exams</a></li>
    <li><a href="/create/exam">Create Exam</a></li>
  </ol>
@endsection


@section('content')

  <script src="/js/exam.js"></script>



  <div class="row">
    <div class="col-sm-12">

      <form class="form-horizontal" action="/create/exam" method="post" enctype="multipart/form-data" id="exam_form">
        {{ csrf_field() }}

        <div class="card-box">
          <div class="row">
            <div class="col-md-12">


              <div class="form-group">
                <label class="col-md-2 control-label">Course</label>
                <div class="col-md-8">
                  <select class="form-control select2" name="course_id">
                    <option value="0">Select A Course</option>
                    @foreach($courses as $course)
                      <option value="{{ $course->course_id }}">{{ $course->course_name }}</option>
                    @endforeach

                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-2 control-label">Exam Title</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" placeholder="Exam 3" name="exam_title" value="Exam">
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-2 control-label">Exam Description</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="exam_description" value="Identify your areas for growth in these lessons:">
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label">Publish Exam</label>
                <div class="col-md-8 checkbox checkbox-primary m-l-10">
                  <input type="checkbox" name="published" checked>
                  <label for="checkbox2">Check if you want to publish the exam</label>
                </div>
              </div>



              <div class="form-group">
                <label class="col-md-2 control-label">Scheduled Exam</label>
                <div class="col-md-8 checkbox checkbox-primary m-l-10">
                  <input type="checkbox" name="scheduled" id="scheduled">
                  <label for="checkbox2">Check if Scheduled Exam</label>
                </div>
              </div>



              <div id="schedule_div" style="display: none">
                <div class="form-group">
                  <label class="col-md-2 control-label">Exam Date</label>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <input type="text" class="form-control" name="schedule_start_date" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                      <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                    </div>
                  </div>
                </div>



                <div class="form-group">
                  <label class="col-md-2 control-label">Exam Time</label>
                  <div class="col-sm-4">
                    <div class="input-group bootstrap-timepicker">
                      <input id="timepicker" type="text" class="form-control" name="schedule_start_time">
                      <span class="input-group-addon bg-custom b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-2 control-label">Duration</label>
                <div class="col-md-4">
                  <div class="input-group bootstrap-touchspin">
                  <span class="input-group-btn">
                    <button class="btn btn-default bootstrap-touchspin-down" type="button">-</button>
                  </span>
                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                    <input type="text" value="60" name="duration" id="duration" class="form-control">
                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                    <span class="input-group-btn">
                    <button class="btn btn-default bootstrap-touchspin-up" type="button">+</button>
                  </span>
                  </div>
                </div>





                <div class="form-group">
                  <div class="col-md-12 text-center m-t-30">
                    <div class="btn-group">
                      <button type="button" class="btn btn-success btn-rounded waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">
                        <span class="btn-label"><i class="fa fa-plus"></i></span>
                        Add Question &nbsp;<span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu">
                        <li onclick="create_question('mcq')"><a href="#">MCQ</a></li>
                        <li onclick="create_question('text')"><a href="#">Text</a></li>
                      </ul>
                    </div>
                    <button type="button" class="btn btn-danger btn-rounded waves-effect waves-light" onclick="clear_question()">
                    <span class="btn-label"><i class="fa fa-times"></i>
                    </span>Clear All Question
                    </button>
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div id="exam_div">

          </div>

          <div class="card-box">
            <div class="">
              <button type="button" class="btn btn-block btn-lg btn-success waves-effect waves-light" onclick="form_submit()">Create Exam</button>
            </div>
          </div>
        </div>

      </form>

    </div>
  </div>

@endsection
