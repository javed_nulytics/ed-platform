@extends('layouts.main')

@section('title', 'View Exams')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/view/exams">Exams</a></li>
  </ol>
@endsection

@section('content')

  <div class="row">

    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tab-hide">
      <div class="card-box">
        <h4 class="text-dark header-title m-t-0 m-b-30">Select Category</h4>

        <div id="portlet2" class="panel-collapse collapse in">
          <div class="portlet-body">
            <div class="table-responsive">
              <table class="table table-hover mails m-0 table table-actions-bar">


                <tbody>
                <tr><td><a href="/courses/category/all"> <i class="fa fa-th-large"></i>  &nbsp; All courses</a></td></tr>

                @foreach($catagories as $category)
                  <tr><td><a href="/courses/category/{{ $category->category_id }}"> <i class="fa fa-th-large"></i>  &nbsp; {{ $category->category_name }}</a></td></tr>
                @endforeach


                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>

    </div>

    <div class="col-md-9 col-lg-9 col-sm-8 col-xs-12">
      <div class="card-box">
        <h4 class="text-dark header-title m-t-0">Select Course</h4>
        <div class="row" style="margin-top: 20px">

          @if(count($courses) == 0)
            <h1 class="text-warning text-center">No Course In This Category</h1>
          @else

            @foreach($courses as $course)

              <div class="col-md-4">
                <div class="thumbnail" href="#">
                  <a href="/exam/course/{{ $course->course_id }}">
                    <img src="/images/course/{{ $course->course_image }}" class="img-responsive" style="height: 130px">

                    <table class="table">
                      <tbody>

                      <tr>
                        <td width="50%">
                          <div class="profile">
                            <img class="img-rounded img-responsive" alt="Instructor Profile Picture" src="/images/users/{{ $course->user_img }}">
                          </div>
                        </td>
                        <td>{{ $course->name }}</td>
                      </tr>



                      <tr>
                        <td colspan="2" style="height: 67px">
                          <div class="course_caption">
                            <strong>{{ $course->course_name }}</strong>
                          </div>
                        </td>
                      </tr>

                      <tr>
                        <td colspan="2">
                          <div class="course_caption">

                            <strong>
                              <p class="text-custom">
                              {{ $course->exam_count }} {{ $course->exam_count>1? 'Exams':'Exam' }}
                              </p>
                            </strong>
                          </div>
                        </td>
                      </tr>



                      </tbody>
                    </table>

                  </a>


                </div>
              </div>

            @endforeach
          @endif

        </div>
      </div>
    </div>

  </div>





@endsection


