@extends('layouts.main')

@section('title', '')


@section('content')

  <script src="/js/exam.js"></script>

  <input type="hidden" id="exam_id" value="{{ $exam_id }}">


  <script type="application/javascript">
      <?php echo 'pushToDurationArray("'.$exam_data->duration.'");'; ?>
  </script>


  <div class="row preExamSpan">

    <div class="col-lg-12">
      <div class="portlet">
        <div class="portlet-heading bg-danger">
          <h3 class="portlet-title">
            Instructions To The Candidates Before The Examination Starts
          </h3>

          <div class="clearfix"></div>
        </div>
        <div id="bg-danger" class="panel-collapse collapse in">
          <div class="portlet-body">
            <ol>
              <li>
                Arrive at the examination venue at least 15 minutes before the start of the examination / 35 minutes before a digital examination.
              </li>
              <li>
                Consectetur adipiscing elit
              </li>
              <li>
                Law: The examination starts at the moment the book control begins, and you must therefore be present by 8.20 a.m. for regular written examinations and 8.10 a.m. at digital examinations.
              </li>
              <li>
                When using a laptop at digital examinations, the laptop has to be set up as soon as possible. If you are taking a law exam, the laptop must be set up before the book control.
              </li>
              <li>
                Coats, backpacks, bags, etc. must be placed as directed. Mobile phones, mp3 players, smartwatches and other electronic devices must be turned off and put away, and cannot be stored in coats or pockets.
              </li>
              <li>
                f support material, other than that which is specifically permitted, is found at or by the desk, it may be treated as an attempt to cheat and relevant procedures for cheating will be followed.
              </li>
              <li>
                The head invigilator will provide information about examination support materials that you are permitted to use during the examination. All dictionaries must be approved by the faculty before the exam and will be handed out in the exam venue by the invigilators.
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>




  </div>


  <div class="row preExamSpan">
    <div class="col-md-12">
      <div class="card-box">


        <div class="">
          <button type="button" class="btn btn-block btn-lg btn-success waves-effect waves-light" onclick="startExam()">
            <i class="fa fa-rocket m-r-5"></i>
            <span>Start Exam</span>
          </button>

        </div>
      </div>
    </div>



  </div>



  <div class="row postExamSpan div-hide" id="time_span_div">

    <div class="col-lg-12 col-sm-12">
      <div class="widget-inline-box card-box text-center">
        <h3><i class="text-pink md glyphicon glyphicon-time"></i>
          <span id="clockdiv"></span></h3>

        <h4 class="text-muted">Remaining</h4>
      </div>
    </div>

  </div>


  <div class="row postExamSpan div-hide">
    <div class="col-lg-12">

      @php $question_count=1; @endphp
      @php $q_count=0; @endphp

      @foreach($questions as $question)


        <script type="application/javascript">
            @php echo 'pushToAnswerArray("'.addslashes($question->answer).'", "'.$question->question_type.'", "'.$question->question_id.'");' @endphp
        </script>



        <div class="card-box m-b-10">
          <div class="table-box opport-box">

            <div class="col-md-8">

              <h4 class="m-t-0 header-title m-b-30">
                <b>{{ $question_count++ }}. {{ $question->question }}</b>
              </h4>

              @if($question->question_type == 'text')
                <div class="form-group m-t-30">
                  <div class="col-md-8">
                    <input type="text" class="form-control" placeholder="Answer" name="question_{{ $q_count }}">
                  </div>
                </div>


              @elseif($question->question_type == 'mcq')

                @if($question->option_1 != Null)
                  <div class="radio radio-success">
                    <input type="radio" name="question_{{ $q_count }}" id="question_{{ $question->question_id }}" value="{{ $question->option_1 }}">
                    <label for="question_{{ $question->question_id }}">
                      {{ $question->option_1 }}
                    </label>
                  </div>
                @endif

                @if($question->option_2 != Null)
                  <div class="radio radio-success">
                    <input type="radio" name="question_{{ $q_count }}" id="question_{{ $question->question_id }}" value="{{ $question->option_2 }}">
                    <label for="question_{{ $question->question_id }}">
                      {{ $question->option_2 }}
                    </label>
                  </div>
                @endif

                @if($question->option_3 != Null)
                  <div class="radio radio-success">
                    <input type="radio" name="question_{{ $q_count }}" id="question_{{ $question->question_id }}" value="{{ $question->option_3 }}">
                    <label for="question_{{ $question->question_id }}">
                      {{ $question->option_3 }}
                    </label>
                  </div>
                @endif

                @if($question->option_4 != Null)
                  <div class="radio radio-success">
                    <input type="radio" name="question_{{ $q_count }}" id="question_{{ $question->question_id }}" value="{{ $question->option_4 }}">
                    <label for="question_{{ $question->question_id }}">
                      {{ $question->option_4 }}
                    </label>
                  </div>
                @endif

              @endif




            </div>
            <div class="col-md-4">

              @if($question->question_media != Null)
                <div class="table-detail">
                  <img src="/images/course/default.jpg" alt="img" class="" width="100%">
                </div>
              @endif



            </div>

          </div>
        </div>

        @php $q_count++; @endphp
      @endforeach


    </div>
  </div>


  <div class="row postExamSpan div-hide">
    <div class="col-lg-12 col-md-12 col-sm-12">

      <div class="m-t-20">
        <div class="">
          <button id="submit_exam_button" type="button" class="btn btn-block btn-lg btn-success btn-rounded waves-effect waves-light" onclick="submit_exam_button();">
            <i class="fa fa-rocket m-r-5"></i> Submit Answers
          </button>
        </div>
      </div>
      <!-- end card-box -->


    </div> <!-- end col-6 -->


  </div>



@endsection
