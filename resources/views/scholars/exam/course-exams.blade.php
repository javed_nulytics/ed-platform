@extends('layouts.main')

@section('title', 'Exams')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/view/exams">Exams</a></li>
    <li><a href="">{{ $course_data->course_name }}</a></li>
  </ol>
@endsection


@section('content')

  <script src="/js/exam.js"></script>

  <div class="row">
    <div class="m-b-15">

      @foreach($exams as $exam)

        <div class="col-sm-6 col-lg-3 col-md-4" id="exam_{{ $exam->exam_id }}">
          <div class="product-list-box thumb">
            <div class="image-popup">
              <img src="/images/assets/exam-cover.jpg" class="thumb-img">

              @if (Auth::user()->id == $exam->tutor_id || Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin')
                <div class="product-action">
                  <button onclick="edit_exam({{ $exam->exam_id }})" class="btn btn-success btn-sm"><i class="md md-mode-edit"></i></button>
                  <button onclick="delete_exam({{ $exam->exam_id }})" class="btn btn-danger btn-sm"><i class="md md-close"></i></button>
                </div>
              @endif


              <div class="detail pointer" onclick="start_exam({{ $exam->exam_id }})">
                <h4 class="m-t-0"><span class="text-dark">{{ $exam->exam_title }}</span></h4>
                <h5 class="m-0"> <span class="text-muted">Duration: {{ $exam->duration }}min</span></h5>
              </div>
            </div>
          </div>
        </div>

      @endforeach

    </div>
  </div>


  <form id="start_exam_form" action="/exam" method="POST" style="display: none;">
    {{ csrf_field() }}
    <input type="hidden" name="exam_id" id="exam_id" value="0">
  </form>

@endsection
