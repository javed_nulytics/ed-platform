@extends('layouts.main')

@section('title', 'Profile')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/">3Scholars</a></li>
        <li><a href="/profile">Profile</a></li>
    </ol>
@endsection

@section('content')

    <script src="/js/profile.js"></script>


    <div class="row">

        <div class="col-md-4 col-lg-3">
            <div class="profile-detail card-box">
                <div>
                    <img src="/images/users/{{ $user->user_img }}" class="img-circle" alt="profile-image">

                    <ul class="list-inline status-list m-t-20">
                        <li>
                            <h3 class="text-primary m-b-5">{{ count($courses) }}</h3>
                            <p class="text-muted">Courses</p>
                        </li>

                        <li>
                            <h3 class="text-success m-b-5">5864</h3>
                            <p class="text-muted">Students</p>
                        </li>
                    </ul>

                    <a type="button" href="#edit_profile_modal"  class="btn btn-pink btn-custom btn-rounded waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" data-overlayspeed="200" data-overlaycolor="#36404a">
                        Edit Profile
                    </a>
                    <br>

                    <a type="button" href="#edit_password_modal"  class="btn btn-danger btn-custom btn-rounded waves-effect waves-light m-t-10" data-animation="fadein" data-plugin="custommodal" data-overlayspeed="200" data-overlaycolor="#36404a">
                        Change Password
                    </a>

                    <hr>
                    <h4 class="text-uppercase font-600">{{ $user->name }}</h4>
                    <p class="text-muted font-13 m-b-30">{{ $user->user_description }}</p>

                    <div class="text-left">
                        <p class="text-muted font-13"><strong>Role :</strong> <span class="m-l-15">{{ ucfirst($user->role) }}</span></p>

                        <p class="text-muted font-13"><strong>Phone :</strong><span class="m-l-15">{{ $user->phone }}</span></p>

                        <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">{{ $user->email }}</span></p>

                        <p class="text-muted font-13"><strong>Status :</strong> <span class="m-l-15">{{ $user->status }}</span></p>

                    </div>

                </div>

            </div>
        </div>


        <div class="col-lg-9 col-md-8">

            {{--<form method="post" class="well">--}}
            {{--<span class="input-icon icon-right">--}}
            {{--<textarea rows="2" class="form-control" placeholder="Send a new message"></textarea>--}}
            {{--</span>--}}
            {{--<div class="p-t-10 pull-right">--}}
            {{--<a class="btn btn-sm btn-primary waves-effect waves-light">Send</a>--}}
            {{--</div>--}}
            {{--<ul class="nav nav-pills profile-pills m-t-10">--}}
            {{--<li>--}}
            {{--<a href="#"><i class="fa fa-user"></i></a>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="#"><i class="fa fa-location-arrow"></i></a>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="#"><i class=" fa fa-camera"></i></a>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="#"><i class="fa fa-smile-o"></i></a>--}}
            {{--</li>--}}
            {{--</ul>--}}

            {{--</form>--}}

            @if($user->name=='tutor')
                <div class="card-box">
                    <h4 class="text-dark header-title m-t-0">Courses By {{ $user->name }}</h4>
                    <div class="row" style="margin-top: 20px">


                        @if(count($courses) == 0)
                            <h1 class="text-warning text-center">No Courses By {{ $user->name }}</h1>
                        @else

                            @foreach($courses as $course)

                                <div class="col-md-4">
                                    <div class="thumbnail" href="#">
                                        <a href="/course/{{ $course->course_id }}">
                                            <img src="/images/course/{{ $course->course_image }}" class="img-responsive" style="height: 130px">

                                            <table class="table">
                                                <tbody>

                                                <tr>
                                                    <td width="50%">
                                                        <div class="profile">
                                                            <img class="img-rounded img-responsive" alt="Instructor Profile Picture" src="/images/users/{{ $course->user_img }}">
                                                        </div>
                                                    </td>
                                                    <td>{{ $course->name }}</td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <i class="md md-star yellow"></i>
                                                        <i class="md md-star yellow"></i>
                                                        <i class="md md-star yellow"></i>
                                                        <i class="md md-star yellow"></i>
                                                        <i class="md md-star-half yellow"></i>
                                                    </td>
                                                    <td><i class="glyphicon glyphicon-user"></i> {{ $course->student_count }}</td>
                                                </tr>

                                                <tr>
                                                    <td colspan="2" style="height: 67px">
                                                        <div class="course_caption">
                                                            <strong>{{ $course->course_name }}</strong>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="2">
                                                        <div class="course_description">
                                                            {{ get_words($course->course_title) }}
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <strong class="course_price_tag">
                                                            @if($course->free_course == 1)
                                                                Free
                                                            @elseif($course->discount == 0)
                                                                ৳ {{ $course->price }}
                                                            @elseif($course->discount > 0 && $course->discount<$course->price)
                                                                <del>৳{{ $course->price }}</del>&nbsp; ৳{{ $course->discount }}
                                                            @else
                                                                ৳ {{ $course->price }}
                                                            @endif
                                                        </strong>
                                                    </td>
                                                    <td>{{ $course->category_name }}</td>
                                                </tr>

                                                </tbody>
                                            </table>

                                        </a>


                                    </div>
                                </div>

                            @endforeach
                        @endif



                    </div>
                </div>
            @else
                <div class="card-box">
                    <a href="/student-analytics/{{ $user->id }}" class="btn btn-purple btn-block btn-lg waves-effect waves-light">
                        <span>Student Analytics Report</span>
                        <i class="glyphicon glyphicon-stats m-l-5"></i>
                    </a>
                </div>
            @endif

        </div>

    </div>


    <div id="edit_profile_modal" class="modal-demo">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only">Close</span>
        </button>
        <h4 class="custom-modal-title">Edit Profile</h4>
        <div class="custom-modal-text text-left">

            <form role="form" action="/edit/profile" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
                </div>


                <div class="form-group">
                    <label for="user_description">Description</label>
                    <textarea type="text" class="form-control" id="user_description" name="user_description">{{ trim($user->user_description) }}</textarea>
                </div>


                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" disabled>
                </div>


                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone }}">
                </div>


                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" class="form-control" id="image" name="image" accept="image/*">
                </div>


                <button type="submit" class="btn btn-default waves-effect waves-light">Save</button>
                <button type="button" class="btn btn-danger waves-effect waves-light m-l-10" onclick="Custombox.close();">Cancel</button>
            </form>

        </div>
    </div>


    <div id="edit_password_modal" class="modal-demo">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only">Close</span>
        </button>
        <h4 class="custom-modal-title">Edit Password</h4>
        <div class="custom-modal-text text-left">

            <div role="form">

                <div class="form-group">
                    <label>Old Password</label>
                    <input type="password" class="form-control" id="old_password">
                </div>


                <div class="form-group">
                    <label>New Password</label>
                    <input type="password" class="form-control" id="new_password">
                </div>


                <div class="form-group">
                    <label>Retype Password</label>
                    <input type="password" class="form-control" id="retype_password">
                </div>




                <button type="button" id="edit_password_button" class="btn btn-default waves-effect waves-light" onclick="Custombox.close();">Save</button>
                <button type="button" class="btn btn-danger waves-effect waves-light m-l-10" onclick="Custombox.close();">Cancel</button>
            </div>

        </div>
    </div>






@endsection
