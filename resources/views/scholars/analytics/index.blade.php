@extends('layouts.main')

@section('title', 'Student Analytics')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="">Analytics</a></li>
    <li><a href="/student-analytics">Student Analytics</a></li>
  </ol>
@endsection


@section('content')

  <style>
    @media print {
      body * {
        visibility: hidden;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
      }
      #analytics_table, #analytics_table * {
        visibility: visible;
        margin: 0;
        padding: 0;
        font-size: 10px;
      }
      #analytics_table {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
      }

    }
  </style>

  <div class="row">

    <div class="col-sm-12">
      <div class="card-box">
        <div class="row">


          <div class="col-lg-12">
            <h4 class="m-t-0 header-title"><b>Student Ranking</b></h4>

            <div class="dt-buttons btn-group"><a class="btn btn-default buttons-copy buttons-html5 btn-sm" tabindex="0" aria-controls="datatable-buttons">
                <span>CSV</span></a><a class="btn btn-default buttons-excel buttons-html5 btn-sm" tabindex="0" aria-controls="datatable-buttons">
                <span>Excel</span></a><a class="btn btn-default buttons-pdf buttons-html5 btn-sm" tabindex="0" aria-controls="datatable-buttons">
                <span>PDF</span></a><a class="btn btn-default buttons-print btn-sm" tabindex="0" aria-controls="datatable-buttons">
                <span onclick="window.print();">Print</span></a>
            </div>

            <div class="m-t-40">
              <table class="table table-striped m-0 table-hover" id="analytics_table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Course</th>
                  <th>Avg Quiz Score(%)</th>
                  <th>Avg Exam Score(%)</th>
                  <th>Percentage(%)</th>
                  <th>Percentile</th>

                </tr>
                </thead>
                <tbody>

                @php $student_count=1; @endphp

                @foreach($students as $student)

                  @if($student->course_count)
                    <tr>
                      <th scope="row">{{ $student_count++ }}</th>
                      <td><a href="/student-analytics/{{ $student->id }}">{{ $student->name }}</a></td>
                      <td>{{ $student->course_count }}</td>
                      <td>{{ $student->avg_quiz_score or 0 }}</td>
                      <td>{{ $student->avg_exam_score or 0 }}</td>
                      <td>{{ $student->course_completed_sum or 0 }}</td>
                      <td>{{ $student->course_completed_sum or 0 }}</td>
                    </tr>
                  @endif



                @endforeach



                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>


@endsection
