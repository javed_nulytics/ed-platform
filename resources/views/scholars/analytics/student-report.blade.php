@extends('layouts.main')

@section('title', 'Student Analytics Report')

@section('breadcrumb')

    <div class="btn-group pull-right">
        <button type="button" class="btn btn-inverse waves-effect" onclick="window.print();">Print
            <span class="m-l-5">
                <i class="glyphicon glyphicon-print"></i>
            </span>
        </button>
    </div>

    <ol class="breadcrumb">
        <li><a href="/">3Scholars</a></li>
        <li><a href="">Analytics</a></li>
        <li><a href="/student-analytics">Student Analytics</a></li>
        <li><a href="/student-analytics/{{ $student_data->id }}">{{ $student_data->name }}</a></li>
    </ol>
@endsection

@section('content')

    <script src="/js/student-dashboard.js"></script>

    @foreach($progress_analytics as $analytics)
        <script type="application/javascript">
            <?php echo 'progress_analytics_push('.$analytics->MONTH.', '.$analytics->COUNT.', '.$analytics->SCORE.');'; ?>
        </script>
    @endforeach


    @foreach($exam_analytics as $analytics)
        <script type="application/javascript">
            <?php echo 'exam_analytics_push('.$analytics->MONTH.', '.$analytics->COUNT.', '.$analytics->SCORE.');'; ?>
        </script>
    @endforeach


    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="widget-profile-one">
                <div class="card-box bg-primary p-lg text-center">
                    <div class="m-b-30">
                        <h3 class="text-white m-b-5">
                            {{ $student_data->name }}
                        </h3>
                        <small>Joined {{ date('F j, Y', strtotime( $student_data->created_at )) }}</small>
                    </div>
                    <img src="/images/users/{{ $student_data->user_img }}" class="img-circle thumb-lg" alt="profile">
                    <div class="m-t-10">
                        <span>Enrolled in <b>{{ count($courses) }}</b> {{ count($courses)>1?'Courses':'Course' }}</span>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="row">


        <div class="col-md-4 col-lg-4">
            <div class="widget-bg-color-icon card-box fadeInDown animated">
                <div class="bg-icon bg-icon-pink pull-left">
                    <i class="md md-add-shopping-cart text-pink"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b class="counter">{{ count($courses) }}</b></h3>
                    <p class="text-muted">{{ count($courses)>1?'Courses':'Course' }}</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-md-4 col-lg-4">
            <div class="widget-bg-color-icon card-box fadeInDown animated">
                <div class="bg-icon bg-icon-purple pull-left">
                    <i class="md md-equalizer text-purple"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b class="counter">{{ ceil($student_data->avg_quiz_score) }}</b>%</h3>
                    <p class="text-muted">Avg Quiz Score</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-md-4 col-lg-4">
            <div class="widget-bg-color-icon card-box fadeInDown animated">
                <div class="bg-icon bg-icon-success pull-left">
                    <i class="md md-equalizer text-success"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b class="counter">{{ ceil($student_data->avg_exam_score) }}</b>%</h3>
                    <p class="text-muted">Avg Exam Score</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0 m-b-30">Quiz Progress</h4>

                <div class="widget-chart text-center">
                    <div id="sparkline1">
                        <canvas id="total_progress_analytics"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0 m-b-30">Quiz Taken</h4>

                <div class="widget-chart text-center">
                    <div id="sparkline1">
                        <canvas id="total_taken_analytics"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0 m-b-30">Exam Progress</h4>

                <div class="widget-chart text-center">
                    <div id="sparkline1">
                        <canvas id="exam_analytics"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="text-dark  header-title m-t-0 m-b-30">Exam Taken</h4>

                <div class="widget-chart text-center">
                    <div id="sparkline1">
                        <canvas id="exam_taken_analytics"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="portlet">
                <div class="portlet-heading">
                    <h3 class="text-dark header-title m-t-30 m-b-20">Enrolled Courses</h3>
                    <div class="clearfix"></div>
                </div>
                <div id="portlet2" class="panel-collapse collapse in" aria-expanded="true" style="">
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Course Name</th>
                                    <th>Start Date</th>
                                    <th>Price</th>
                                    <th>Completed</th>
                                </tr>
                                </thead>
                                <tbody>

                                @php $course_count=1; @endphp

                                @foreach($courses as $course)

                                    <tr>
                                        <td>{{ $course_count++ }}</td>
                                        <td><a href="/course/{{ $course->course_id }}">{{ $course->course_name }}</a></td>
                                        <td>{{ date('F j, Y', strtotime( $course->started_date )) }}</td>
                                        <td>{{ $course->free_course==0?$course->price:'Free' }}</td>
                                        <td>{{  $course->completed }}%</td>
                                    </tr>

                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->

    </div>


    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="portlet">
                <div class="portlet-heading">
                    <h3 class="text-dark header-title m-t-30 m-b-20">Exam Results</h3>
                    <div class="clearfix"></div>
                </div>
                <div id="portlet2" class="panel-collapse collapse in" aria-expanded="true" style="">
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Exam Title</th>
                                    <th>Course</th>
                                    <th>Answered</th>
                                    <th>Score</th>
                                    <th>Percentile</th>
                                    <th>Exam Taken</th>
                                    <th>View Result</th>
                                </tr>
                                </thead>
                                <tbody>

                                @php $exam_count=1; @endphp

                                @foreach($exam_results as $result)

                                    <tr>
                                        <td>{{ $exam_count++ }}</td>
                                        <td><span class="pointer">{{ $result->exam_title }}</span></td>
                                        <td><a href="/course/{{ $result->course_id }}">{{ $result->course_name }}</a></td>
                                        <td>{{ $result->answered_question }} out of {{ $result->total_question }}</td>
                                        <td>{{ $result->score }}%</td>
                                        <td>{{ $result->percentile }}%</td>
                                        <td>{{ date('F j, Y', strtotime( $result->created_at )) }}</td>

                                        <td class="div-to-center">
                                            <a href="/exam/result/{{ $result->result_id }}" type="button" class="btn btn-instagram waves-effect waves-light"><i class="ion-ios7-eye"></i></a>
                                        </td>
                                    </tr>

                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->

    </div>


    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="portlet">
                <div class="portlet-heading">
                    <h3 class="text-dark header-title m-t-30 m-b-20">Quiz Results</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-collapse collapse in" aria-expanded="true" style="">
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Course</th>
                                    <th>Lecture</th>
                                    <th>Answered</th>
                                    <th>Score</th>
                                    <th>Date</th>
                                    <th class="div-to-center">View Result</th>
                                </tr>
                                </thead>
                                <tbody>

                                @php $quiz_result_count=1; @endphp

                                @foreach($quiz_results as $result)

                                    <tr>
                                        <td>{{ $quiz_result_count++ }}</td>
                                        <td><span class="pointer">{{ $result->quiz_title }}</span></td>
                                        <td><a href="/course/{{ $result->course_id }}">{{ $result->course_name }}</a></td>
                                        <td><a href="/lecture/{{ $result->lecture_id }}">{{ $result->lecture_title }}</a></td>
                                        <td>{{ $result->answered_question }} out of {{ $result->total_question }}</td>
                                        <td>{{ $result->score }}%</td>
                                        <td>{{ date('F j, Y', strtotime( $result->created_at )) }}</td>

                                        <td class="div-to-center">
                                            <a href="/quiz/result/{{ $result->result_id }}" type="button" class="btn btn-instagram waves-effect waves-light">
                                                <i class="ion-ios7-eye"></i>
                                            </a>
                                        </td>
                                    </tr>

                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




@endsection
