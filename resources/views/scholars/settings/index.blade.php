@extends('layouts.main')

@section('title', 'Settings')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/">3Scholars</a></li>
        <li><a href="">Settings</a></li>
        <li><a href="">App Settings</a></li>
    </ol>
@endsection

@section('content')

    <script src="/js/settings.js"></script>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card-box">
                <h3 class="text-dark header-title m-t-15 ">App Settings</h3>

                <div class="row">
                    <div class="col-lg-10 col-md-10 col-sm-12 m-t-30">
                        <div class="form-horizontal" role="form">

                            <div class="form-group">
                                <label class="col-md-2 control-label">Application Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="app_name" value="{{ $settings['app_name'] }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Application Title</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="app_title" value="{{ $settings['app_title'] }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Application Sub-Title</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="app_subtitle" value="{{ $settings['app_subtitle'] }}">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label">Application Version</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="app_version" value="{{ $settings['app_version'] }}">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label">YouTube Link</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="youtube_video_link" value="{{ $settings['youtube_video_link'] }}">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-12 m-t-20">
                                    <button id="save_settings_button" class="btn btn-default waves-effect waves-light btn-md" >Save Settings</button>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card-box">
                <h3 class="text-dark header-title m-t-15 m-b-20">LookUp</h3>


                <div class="row">
                    <div class="col-sm-6 text-xs-center m-t-15">
                        <div class="form-group">
                            <button id="demo-btn-addrow" class="btn btn-inverse m-b-20" data-toggle="modal" data-target="#add_lookup_modal">
                                <i class="fa fa-plus m-r-5"></i> Add New LookUp</button>
                        </div>
                    </div>
                </div>


                <div>
                    <table class="table table-striped m-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>LookUp Name</th>
                            <th>Created By</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody id="lookup_table">

                        @php $lookup_count=1; @endphp

                        @foreach($lookup as $item)
                            <tr>
                                <th scope="row">{{ $lookup_count++ }}</th>
                                <td>{{ ucfirst($item->lookup_name) }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ date('F j, Y', strtotime( $item->created_at )) }}</td>
                                <td>
                                    <button type="button" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button>
                                    <button type="button" class="btn btn-youtube waves-effect waves-light"><i class="ion-close"></i></button>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card-box">
                <h3 class="text-dark header-title m-t-15 m-b-20">Course Level</h3>


                <div class="row">
                    <div class="col-sm-6 text-xs-center m-t-15">
                        <div class="form-group">
                            <button id="demo-btn-addrow" class="btn btn-inverse m-b-20" data-toggle="modal" data-target="#add_level_modal">
                                <i class="fa fa-plus m-r-5"></i> Add New Level</button>
                        </div>
                    </div>
                </div>


                <div>
                    <table class="table table-striped m-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Level Name</th>
                            <th>Created By</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody id="course_level_table">

                        @php $level_count=1; @endphp

                        @foreach($course_level as $item)
                            <tr>
                                <th scope="row">{{ $level_count++ }}</th>
                                <td>{{ ucfirst($item->course_level_name) }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ date('F j, Y', strtotime( $item->created_at )) }}</td>
                                <td>
                                    <button type="button" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button>
                                    <button type="button" class="btn btn-youtube waves-effect waves-light"><i class="ion-close"></i></button>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>


    </div>


    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card-box">
                <h3 class="text-dark header-title m-t-15 m-b-20">Role</h3>
                <div class="row">
                    <div class="col-sm-6 text-xs-center m-t-15">
                        <div class="form-group">
                            <button id="demo-btn-addrow" class="btn btn-inverse m-b-20" data-toggle="modal" data-target="#add_role_modal">
                                <i class="fa fa-plus m-r-5"></i> Add New Role</button>
                        </div>
                    </div>
                </div>


                <div>
                    <table class="table table-striped m-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Role</th>
                            <th>Created By</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody id="role_table">

                        @php $role_count=1; @endphp

                        @foreach($role_lookup as $item)
                            <tr>
                                <th scope="row">{{ $role_count++ }}</th>
                                <td>{{ ucfirst($item->role_name) }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ date('F j, Y', strtotime( $item->created_at )) }}</td>
                                <td>
                                    <button type="button" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button>
                                    <button type="button" class="btn btn-youtube waves-effect waves-light"><i class="ion-close"></i></button>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>



    </div>





    <div id="add_lookup_modal" class="modal bounceInDown animated"  role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Add New LookUp</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">LookUp Name</label>
                                <input type="text" class="form-control" id="lookup_name">
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info waves-effect waves-light" id="add_lookup_button" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>


    <div id="add_level_modal" class="modal bounceInDown animated"  role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Add New Course Level</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Level Name</label>
                                <input type="text" class="form-control" id="course_level_name">
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info waves-effect waves-light" id="add_level_button" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>


    <div id="add_role_modal" class="modal bounceInDown animated"  role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Add New Role</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="role_name" class="control-label">Role Name</label>
                                <input type="text" class="form-control" id="role_name">
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info waves-effect waves-light" id="add_role_button" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>




    <script src="/js/settings.js"></script>

@endsection
