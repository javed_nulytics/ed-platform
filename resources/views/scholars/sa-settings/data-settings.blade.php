@extends('layouts.main')

@section('title', 'Data Settings')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/">3Scholars</a></li>
        <li><a href="/view/courses">Courses</a></li>
    </ol>
@endsection

@section('content')

    <script src="/js/data-settings.js"></script>


    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card-box">
                <h3 class="text-dark header-title m-t-15 m-b-20">Truncate</h3>


                <div class="row">
                    <div class="col-sm-12 text-xs-center m-t-15">
                        <div class="form-group">
                            <button type="button" class="btn btn-warning" id="table_truncate_toggle">Check/Uncheck All</button>
                            <button type="button" class="btn btn-danger" id="table_truncate_button">Truncate</button>
                        </div>
                    </div>
                </div>


                <div>

                    <table class="table table-striped table-hover">
                        <thead>

                        <tr class="headings">
                            <th class="column-title">#</th>
                            <th class="column-title"></th>
                            <th class="column-title">Table Name</th>
                        </tr>

                        </thead>

                        <tbody>

                        @php $table_count=1; @endphp

                        @foreach($tables as $table)
                            @if($table->$db_name != 'migrations')
                                <tr>
                                    <td scope="row"> {{ $table_count++ }}</td>
                                    <td>
                                        <input class="table_truncate" value="{{ $table->$db_name }}" type="checkbox">
                                    </td>
                                    <td>{{ $table->$db_name }}</td>
                                </tr>
                            @endif

                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card-box">
                <h3 class="text-dark header-title m-t-15 m-b-20">Drop</h3>


                <div class="row">
                    <div class="col-sm-12 text-xs-center m-t-15">
                        <div class="form-group">
                            <button type="button" class="btn btn-warning" id="table_drop_toggle">Check/Uncheck All</button>
                            <button type="button" class="btn btn-danger" id="table_drop_button">Drop</button>
                        </div>
                    </div>
                </div>


                <div>
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr class="headings">
                            <th class="column-title">#</th>
                            <th class="column-title"></th>
                            <th class="column-title">Table Name</th>
                        </tr>
                        </thead>

                        <tbody>

                        @php $table_count=1; @endphp

                        @foreach($tables as $table)
                            @if($table->$db_name != 'migrations')
                                <tr>
                                    <td scope="row"> {{ $table_count++ }}</td>
                                    <td>
                                        <input class="table_drop" value="{{ $table->$db_name }}" type="checkbox">
                                    </td>
                                    <td>{{ $table->$db_name }}</td>
                                </tr>
                            @endif

                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>


    </div>





@endsection
