@extends('layouts.main')

@section('title', 'Scheduled Scripts')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/">3Scholars</a></li>
        <li><a href="/view/courses">Courses</a></li>
    </ol>
@endsection

@section('content')

    <script src="/js/schedule-scripts.js"></script>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-12 text-right m-t-15">
                        <div class="form-group">
                            <button type="button" class="btn btn-danger" id="generate_data_button">Generate Data</button>
                            <button type="button" class="btn btn-pink" id="db_fix_scripts_button">DB Fixing Script</button>
                            <button type="button" class="btn btn-primary" id="run_scripts_button">Run Scripts</button>
                        </div>
                    </div>
                </div>

                <div>
                    <table class="table table-striped m-0">
                        <thead>
                        <tr>
                            <th>Job Id</th>
                            <th>Script Name</th>
                            <th>Scheduled</th>
                            <th>Executed At</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($schedule_scripts as $script)
                            <tr>
                                <td>{{ $script->job_id }}</td>
                                <td>{{ $script->script_name }}</td>
                                <td>{{ $script->schedule_date != null ? date('F j, Y', strtotime( $script->schedule_date )):"" }}</td>
                                <td>{{ date('F j, Y h:i:s A', strtotime( $script->created_at )) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $schedule_scripts->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection
