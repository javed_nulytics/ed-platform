@extends('layouts.main')

@section('title', 'About')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/">3Scholars</a></li>
        <li><a href="/about">About 3Scholars</a></li>
    </ol>
@endsection

@section('content')

    <div class="row">
        <div class="card-box">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <blockquote>
                        <p>Interested in improving your professional life? Do you want to learn how to get ahead on your career path? Keep reading to find out how you can achieve all of these and more through</p>
                        <footer>
                            3Scholars - Education Center!
                        </footer>
                    </blockquote>
                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 m-t-40">
                    <h4 class="m-t-0 header-title">
                        <i class=" glyphicon glyphicon-info-sign"></i>&nbsp;
                        <b>About 3Scholars - Education Center</b>
                    </h4>

                    <p>
                        3Scholars - Education Center was launched with the purpose of helping you to learn valuable new skills that will help you make the most out of your work environment and keep you updated with the most recent developments in the tech world. Each online course on 3Scholars - Education Center is designed and reviewed by the 3Scholars Review Team with special attention given to the quality of the content.
                    </p>

                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 m-t-40">
                    <h4 class="m-t-0 header-title">
                        <i class=" glyphicon glyphicon-book"></i>&nbsp;
                        <b>Course Topics</b>
                    </h4>

                    <p>
                        3Scholars - Education Center delivers premium courses in Entrepreneurship, Freelancing, Computer Science, Software Development, Graphics Design, Business, Marketing and Communication. Still hungry for more? You can also find miscellaneous courses which feature everything from Rocket Science to Guitar Playing.
                    </p>

                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 m-t-40">
                    <h4 class="m-t-0 header-title">
                        <i class="fa fa-graduation-cap"></i>&nbsp;
                        <b>Certification</b>
                    </h4>

                    <p>
                        At 3Scholars - Education Center, your accomplishments do not go unnoticed. After completing an online course you'll be awarded an official 3Scholars Certificate commending your newly learned skill and we also provide Verified Certificates that you can include in your CVs and Résumés.
                    </p>

                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 m-t-40">
                    <h4 class="m-t-0 header-title">
                        <i class="glyphicon glyphicon-question-sign"></i>&nbsp;
                        <b>Afraid that a virtual academy will lack the special attention and personalization of a real class?</b>
                    </h4>

                    <p>
                        Worry not! Every course featured in 3Scholars - Education Center is uniquely structured and created by experts specifically for your class. At 3Scholars - Education Center you won't be falling asleep on your keyboard. In order to make the learning experience enthralling and fun, we use diverse teaching methods throughout the online classes.
                    </p>

                    <p>After enrolling in a course, you will find yourself:</p>

                    <p><i class="ion-arrow-right-a"></i>&nbsp; &nbsp;Watching instructive videos</p>
                    <p><i class="ion-arrow-right-a"></i>&nbsp; &nbsp;Improving your knowledge using various reading materials</p>
                    <p><i class="ion-arrow-right-a"></i>&nbsp; &nbsp;Answering challenging quizzes</p>
                    <p><i class="ion-arrow-right-a"></i>&nbsp; &nbsp;Discussing topics with your course teacher</p>
                    <p><i class="ion-arrow-right-a"></i>&nbsp; &nbsp;And even putting these skills to real use as part of the program</p>

                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 m-t-40">
                    <h4 class="m-t-0 header-title">
                        <i class="glyphicon glyphicon-question-sign"></i>&nbsp;
                        <b>Who are the teachers?</b>
                    </h4>

                    <p>
                        3Scholars - Education Center features classes from leading online course creators who've spent thousands of hours teaching classes that aim to help students advance in their professional lives. We ensure that each of our teachers meet our high standards and truly care about helping their students.</p>
                    <p>
                        Still missing something in our course catalog? Let us know! If there is a skill you wish to learn that is not yet featured in our portfolio, we'd love to hear from you and use your feedback to improve 3Scholars - Education Center to meet your needs! Email us .
                    </p>

                </div>

            </div>
        </div>
    </div>




@endsection
