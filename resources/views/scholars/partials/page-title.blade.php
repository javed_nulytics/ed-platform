<div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">
        <h4 class="page-title"><a href="@yield('title-link')">@yield('title')</a></h4>
        {{--<p class="text-muted page-title-alt">@yield('subtitle')</p>--}}
        @yield('breadcrumb')
    </div>
</div>