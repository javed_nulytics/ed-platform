
<script>
var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/detect.js"></script>
<script src="/js/fastclick.js"></script>

<script src="/js/jquery.slimscroll.js"></script>
<script src="/js/jquery.blockUI.js"></script>
<script src="/js/waves.js"></script>
<script src="/js/wow.min.js"></script>
<script src="/js/jquery.nicescroll.js"></script>
<script src="/js/jquery.scrollTo.min.js"></script>

<script src="/js/jquery.peity.min.js"></script>

<!-- jQuery  -->
<script src="/js/jquery.waypoints.js"></script>
<script src="/js/jquery.counterup.min.js"></script>



<script src="/js/morris.min.js"></script>
<script src="/js/raphael-min.js"></script>

<script src="/js/jquery.knob.js"></script>

<script src="/js/jquery.dashboard.js"></script>

<script src="/js/jquery.core.js"></script>
<script src="/js/jquery.app.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>

<!-- Modal-Effect -->
<script src="/js/custombox.min.js"></script>
<script src="/js/legacy.min.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($) {
  $('.counter').counterUp({
    delay: 100,
    time: 1200
  });

  $(".knob").knob();

});
</script>

<!-- Sweet Alert js -->
<script src="/js/sweetalert2.all.min.js"></script>
<script src="/js/chart.min.js"></script>


<script src="/js/notify.js"></script>
<script src="/js/notify-metro.js"></script>




<script src="/js/moment.js"></script>
<script src="/js/bootstrap-timepicker.js"></script>
<script src="/js/bootstrap-colorpicker.min.js"></script>
<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/bootstrap-clockpicker.min.js"></script>
<script src="/js/daterangepicker.js"></script>


<script src="/js/jquery.form-pickers.init.js"></script>


<script src="/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>


