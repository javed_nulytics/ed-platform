<!--Morris Chart CSS -->
<link rel="stylesheet" href="/css/morris.css">


<link href="/css/bootstrap-timepicker.min.css" rel="stylesheet">
{{--<link href="/css/bootstrap-colorpicker.min.css" rel="stylesheet">--}}
<link href="/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="/css/bootstrap-clockpicker.min.css" rel="stylesheet">
<link href="/css/daterangepicker.css" rel="stylesheet">

<link href="/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />


<link href="/css/custombox.css" rel="stylesheet">
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/css/core.css" rel="stylesheet" type="text/css" />
<link href="/css/components.css" rel="stylesheet" type="text/css" />
<link href="/css/icons.css" rel="stylesheet" type="text/css" />
<link href="/css/pages.css" rel="stylesheet" type="text/css" />
<link href="/css/responsive.css" rel="stylesheet" type="text/css" />

<link href="/css/animate.min.css" rel="stylesheet" type="text/css" />

<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link href="/css/mobile.css" rel="stylesheet" type="text/css" />

<link href="/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />


<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

{{--<script src="/js/modernizr.min.js"></script>--}}
<link href="/css/sweetalert2.css" rel="stylesheet" type="text/css" />

<script src="/js/jquery.min.js"></script>


<!-- Custom JS File -->
<script src="/js/application.js"></script>
