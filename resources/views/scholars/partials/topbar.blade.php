<div class="topbar">

  <!-- Logo container-->
@include('scholars.partials.logo-container')
<!-- End Logo container-->

  <!-- Button mobile view to collapse sidebar menu -->
  @include('scholars.partials.topbar-right')

</div>
<!-- Top Bar End -->