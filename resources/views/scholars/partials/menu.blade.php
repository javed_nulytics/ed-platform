<div class="left side-menu">
  <div class="sidebar-inner slimscrollleft">
    <!--- Divider -->
    <div id="sidebar-menu">
      <ul>

        <li><a href="/" class="waves-effect"><i class="ti-home"></i> <span> Home </span></a></li>

        @auth


          <li class="text-muted menu-title">{{ ucfirst(Auth::user()->role) }}</li>



          <li><a href="/dashboard" class="waves-effect"><i class="md md-dashboard"></i> <span> Dashboard </span></a></li>


          @if(Auth::user()->role == 'superadmin')
            <li class="has_sub">
              <a href="javascript:void(0);" class="waves-effect"><i class="glyphicon glyphicon-wrench"></i> <span> SuperAdmin </span> <span class="menu-arrow"></span> </a>
              <ul class="list-unstyled">
                <li><a href="/data-settings">Data</a>
                <li><a href="/schedule-scripts">Schedule Scripts</a>
              </ul>
            </li>
          @endif


          <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="md md-insert-comment"></i> <span> Courses </span> <span class="menu-arrow"></span> </a>
            <ul class="list-unstyled">
              @if(Auth::user()->role != 'student')
                <li><a href="/create-course">Create Course</a></li>
                <li><a href="/view/courses">View Courses</a></li>
                <li><a href="/view/quizzes">View Quizzes</a></li>
              @else
                <li><a href="/enrolled-courses">Enrolled Courses</a></li>
                <li><a href="/view/quizzes">View Quizzes</a></li>
              @endif
            </ul>
          </li>




          <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-edge"></i> <span> Exams </span> <span class="menu-arrow"></span> </a>
            <ul class="list-unstyled">
              @if(Auth::user()->role != 'student')
                <li><a href="/create/exam">Create Exam</a></li>
                <li><a href="/view/exams">View Exams</a></li>
              @else
                <li><a href="/view/exams">Browse Exams</a></li>
                <li><a href="/exam/history">Exam Results</a></li>
              @endif
            </ul>
          </li>



          @if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin')

            <li class="has_sub">
              <a href="javascript:void(0);" class="waves-effect">
                <i class="glyphicon glyphicon-stats"></i> <span> Analytics </span> <span class="menu-arrow"></span> </a>
              <ul class="list-unstyled">
                <li><a href="/student-analytics">Student Analytics</a></li>
              </ul>
            </li>

            <li class="has_sub">
              <a href="javascript:void(0);" class="waves-effect"><i class="ion-gear-a"></i> <span> Settings </span> <span class="menu-arrow"></span> </a>
              <ul class="list-unstyled">
                <li><a href="/users">Users</a></li>
                <li><a href="/settings">App Settings</a></li>
                <li><a href="#">Activities</a></li>
              </ul>
            </li>
          @endif

          <li><a href="/logout" class="waves-effect" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>

        @else

          <li>
            <a href="/login" class="waves-effect"><i class="fa fa-sign-in"></i> <span> Sign In</span></a>
          </li>

          <li>
            <a href="/register" class="waves-effect"><i class="fa fa-cubes"></i> <span> Sign Up</span></a>
          </li>


        @endauth


      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
