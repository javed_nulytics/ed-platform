@extends('layouts.main')

@section('title', 'Dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/">3Scholars</a></li>
        <li><a href="/dashboard">Dashboard</a></li>
    </ol>
@endsection

@section('content')

<script src="/js/tutor.js"></script>


    <form id="edit_course_form" action="/edit/course" method="POST" style="display: none;">
        {{ csrf_field() }}
        <input type="hidden" name="form_course_id" id="form_course_id" value="0">
        <input type="hidden" name="redirect_url" id="redirect_url" value="dashboard">
    </form>

    <div class="row">
        <div class="col-md-6 col-lg-4">
            <div class="widget-bg-color-icon card-box fadeInDown animated">
                <div class="bg-icon bg-icon-primary pull-left">
                    <i class="glyphicon glyphicon-user text-primary"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b class="counter">{{ $total_students }}</b></h3>
                    <p class="text-muted">Total Students</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-md-6 col-lg-4">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-pink pull-left">
                    <i class="glyphicon glyphicon-book text-pink"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b class="counter">{{ count($courses) }}</b></h3>
                    <p class="text-muted">Total Courses</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-md-6 col-lg-4">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-info pull-left">
                    <i class="glyphicon glyphicon-filter text-info"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b class="counter">0.16</b>%</h3>
                    <p class="text-muted">Pending requests</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>


    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="portlet">
                <div class="portlet-heading">
                    <a href="/view/courses" class="pull-right btn btn-primary waves-effect waves-light w-lg m-t-30 m-r-15">
                      <span class="btn-label">
                      <i class="glyphicon glyphicon-eye-open"></i>
                      </span>
                        View All Courses
                    </a>
                    <h3 class="text-dark header-title m-t-30 m-b-20">My Courses</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-collapse collapse in" aria-expanded="true">
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table" id ="tutor_courses_table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Course Name</th>
                                    <th>Start Date</th>
                                    <th>Instructor</th>
                                    <th>Price</th>
                                    <th>Status</th>
                                    <th>No of Student</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @php $course_count=1; @endphp

                                @foreach($courses as $course)

                                    <tr>
                                        <td>{{ $course_count++ }}</td>
                                        <td><a href="/course/{{ $course->course_id }}">{{ $course->course_name }}</a></td>
                                        <td>{{ date('F j, Y', strtotime( $course->created_at )) }}</td>
                                        <td><a href="/profile/{{ $course->id }}">{{ $course->name }}</a></td>
                                        <td>{{ $course->price }}</td>
                                        <td>{{ ucfirst($course->status_name) }}</td>
                                        <td>{{ $course->no_student }}</td>
                                        <td style="width: 10%">

                                            <!-- <button type="button" class="btn btn-twitter waves-effect waves-light"><i class="ion-ios7-eye"></i></button> -->
                                            <button type="button" onclick="editCourse({{ $course->course_id }})" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button>
                                            <button type="button" onclick="delete_course({{ $course->course_id }})" class="btn btn-youtube waves-effect waves-light"><i class="ion-close"></i></button>

                                        </td>
                                    </tr>

                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->

    </div>


    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="portlet"><!-- /primary heading -->
                <div class="portlet-heading">
                    <a href="/view/course-request" class="pull-right btn btn-primary waves-effect waves-light w-lg m-t-30 m-r-15">
          <span class="btn-label">
            <i class="glyphicon glyphicon-eye-open"></i>
          </span>
                        View All Requests
                    </a>

                    <button class="pull-right btn btn-warning waves-effect waves-light w-lg m-t-30 m-r-15" id="clear_request_button">
                        <i class="ion-alert-circled m-r-5"></i> Clear All Request
                    </button>

                    <h3 class="text-dark header-title m-t-30 m-b-20">Course Requests</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-collapse collapse in" aria-expanded="true" >
                    <div class="portlet-body">
                        <div class="table-responsive" >
                            <table class="table" id="course_request_tutor">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Student Name</th>
                                    <th>Course Name</th>
                                    <th>Requested At</th>
                                    <th>Status</th>
                                </tr>
                                </thead>

                                <tbody id="course_request_table">
                                @php $request_count=1; @endphp

                                @foreach($course_requests as $request)
                                    <tr>
                                        <td>{{ $request_count++ }}</td>
                                        <td><a href="/profile/{{ $request->student_id }}">{{ $request->name }}</a></td>
                                        <td><a href="/course/{{ $request->course_id }}">{{ $request->course_name }}</a></td>
                                        <td>{{ date('F j, Y', strtotime( $request->created_at )) }}</td>
                                        <td>
                                            @if($request->status_name == 'accepted')
                                                <button class="btn btn-success waves-effect waves-light">{{ ucfirst($request->status_name) }}</button>
                                            @elseif($request->status_name == 'requested')
                                                <button id="cs_button_{{ $request->course_student_id }}" onclick="course_request('{{ $request->course_student_id }}', '{{ $request->status_name }}')" class="btn btn-warning waves-effect waves-light">{{ ucfirst($request->status_name) }}</button>
                                            @endif
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                            {{ $course_requests->links() }}
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="portlet"><!-- /primary heading -->
                <div class="portlet-heading">
                    <button class="pull-right btn btn-inverse waves-effect waves-light w-lg m-t-30 m-r-15" data-toggle="modal" data-target="#add_category_modal">
          <span class="btn-label">
            <i class="glyphicon glyphicon-plus"></i>
          </span>
                        Add Category
                    </button>
                    <h3 class="text-dark header-title m-t-30 m-b-20">Course Categories</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-collapse collapse in" aria-expanded="true">
                    <div class="portlet-body">
                        <div class="table-responsive" >
                            <table class="table" id="table_category">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category Name</th>
                                    <th>Created By</th>
                                    <th style="width: 25%">Action</th>
                                </tr>
                                </thead>

                                <tbody id="category_table">
                                @php $cat_count=1; @endphp

                                @foreach($course_category as $category)
                                    <tr>
                                        <td>{{ $cat_count++ }}</td>
                                        <td>{{ $category->category_name }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>
                                            <button type="button" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button>
                                            <button type="button" class="btn btn-youtube waves-effect waves-light" onclick="delete_category({{ $category->category_id }})"><i class="ion-close"></i></button>
                                        </td>

                                    </tr>

                                @endforeach
                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>



@endsection
