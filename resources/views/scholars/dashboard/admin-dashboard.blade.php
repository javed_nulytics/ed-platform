@extends('layouts.main')

@section('title', 'Admin Dashboard')

@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="/">3Scholars</a></li>
    <li><a href="/dashboard">Dashboard</a></li>
  </ol>
@endsection

@section('content')

  <script src="/js/admin.js"></script>


  <form id="edit_course_form" action="/edit/course" method="POST" style="display: none;">
    {{ csrf_field() }}
    <input type="hidden" name="form_course_id" id="form_course_id" value="0">
    <input type="hidden" name="redirect_url" id="redirect_url" value="dashboard">
  </form>

  <script type="application/javascript">
      <?php echo 'student_array_push("'.$analytics->st_1.'", "'.$analytics->st_2.'", "'.$analytics->st_3.'", "'.$analytics->st_4.'", "'.$analytics->st_5.'");'; ?>
      <?php echo 'tutor_array_push("'.$analytics->tu_1.'", "'.$analytics->tu_2.'", "'.$analytics->tu_3.'", "'.$analytics->tu_4.'", "'.$analytics->tu_5.'");'; ?>
      <?php echo 'course_array_push("'.$analytics->co_1.'", "'.$analytics->co_2.'", "'.$analytics->co_3.'", "'.$analytics->co_4.'", "'.$analytics->co_5.'");'; ?>
  </script>

  @foreach($course_category as $category)
    <script type="application/javascript">
        <?php echo 'pushToCategoryArray("'.$category->category_name.'", "'.$category->category_count.'");'; ?>
    </script>
  @endforeach


  <div class="row">
    <div class="col-lg-3 col-md-6 col-sm-12">
      <div class="widget-bg-color-icon card-box tada animated">
        <div class="bg-icon bg-icon-primary pull-left">
          <i class="md md-attach-money text-primary"></i>
        </div>
        <div class="text-right">
          <h3 class="text-dark"><b class="counter">{{ $revenue }}</b></h3>
          <p class="text-muted">Total Revenue</p>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-12">
      <div class="widget-bg-color-icon card-box tada animated">
        <div class="bg-icon bg-icon-primary pull-left">
          <i class="md md-store-mall-directory text-info"></i>
        </div>
        <div class="text-right">
          <h3 class="text-dark"><b class="counter">{{ $data->total_courses }}</b></h3>
          <p class="text-muted">Courses</p>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-12">
      <div class="widget-bg-color-icon card-box tada animated">
        <div class="bg-icon bg-icon-info pull-left">
          <i class="md md-account-child text-custom"></i>
        </div>
        <div class="text-right">
          <h3 class="text-dark"><b class="counter">{{ $data->total_student }}</b></h3>
          <p class="text-muted">Students</p>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-12">
      <div class="widget-bg-color-icon card-box tada animated">
        <div class="bg-icon bg-icon-success pull-left">
          <i class="md md-account-child text-custom"></i>
        </div>
        <div class="text-right">
          <h3 class="text-dark"><b class="counter">{{ $data->total_tutor }}</b></h3>
          <p class="text-muted">Tutors</p>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>


  <div class="row">

    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="card-box">
        <h4 class="text-dark header-title m-t-20 m-b-20">Course Category Analytics</h4>
        <div class="row">

          <div>
            <canvas id="analytics_category_course"></canvas>
          </div>

        </div>
      </div>
    </div>

    <div class="col-lg-8 col-md-8 col-sm-12">
      <div class="card-box">
        <h4 class="text-dark header-title m-t-20 m-b-20">Analytics</h4>
        <div class="text-center">
          <ul class="list-inline chart-detail-list">
            <li>
              <h5><i class="fa fa-circle m-r-5" style="color: #ff6384;"></i>Students</h5>
            </li>
            <li>
              <h5><i class="fa fa-circle m-r-5" style="color: #36a2eb;"></i>Teachers</h5>
            </li>
            <li>
              <h5><i class="fa fa-circle m-r-5" style="color: #ffce56 ;"></i>Courses</h5>
            </li>
          </ul>
        </div>

        <div>
          <canvas id="analytics_barChart"></canvas>
        </div>
      </div>
    </div>

  </div>


  <div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="portlet">
        <div class="portlet-heading">
          <a href="/view/courses" class="pull-right btn btn-primary waves-effect waves-light w-lg m-t-30 m-r-15">
          <span class="btn-label">
            <i class="glyphicon glyphicon-eye-open"></i>
          </span>
            View All Courses
          </a>
          <h3 class="text-dark header-title m-t-30 m-b-20">Top Courses</h3>
          <div class="clearfix"></div>
        </div>
        <div id="portlet2" class="panel-collapse collapse in" aria-expanded="true" style="">
          <div class="portlet-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Course Name</th>
                  <th>Start Date</th>
                  <th>Instructor</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th>No of Student</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @php $course_count=1; @endphp

                @foreach($courses as $course)

                  <tr>
                    <td>{{ $course_count++ }}</td>
                    <td><a href="/course/{{ $course->course_id }}">{{ $course->course_name }}</a></td>
                    <td>{{ date('F j, Y', strtotime( $course->created_at )) }}</td>
                    <td><a href="/profile/{{ $course->id }}">{{ $course->name }}</a></td>
                    <td>{{ $course->price }}</td>
                    <td>{{ ucfirst($course->status_name) }}</td>
                    <td>{{ $course->no_student }}</td>
                    <td style="width: 10%">

                      <!-- <button type="button" class="btn btn-twitter waves-effect waves-light"><i class="ion-ios7-eye"></i></button> -->
                      <button type="button" onclick="editCourse({{ $course->course_id }})" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button>
                      <button type="button" class="btn btn-youtube waves-effect waves-light" onclick="delete_course({{ $course->course_id }})"><i class="ion-close"></i></button>

                    </td>
                  </tr>

                @endforeach


                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- end col -->

  </div>


  <div class="row">

    <div class="col-lg-6 col-md-6 col-sm-12">
      <div class="portlet"><!-- /primary heading -->
        <div class="portlet-heading">
          <a href="/view/course-request" class="pull-right btn btn-primary waves-effect waves-light w-lg m-t-30 m-r-15">
          <span class="btn-label">
            <i class="glyphicon glyphicon-eye-open"></i>
          </span>
            View All Requests
          </a>

          <button class="pull-right btn btn-warning waves-effect waves-light w-lg m-t-30 m-r-15" id="clear_request_button">
            <i class="ion-alert-circled m-r-5"></i> Clear All Request
          </button>

          <h3 class="text-dark header-title m-t-30 m-b-20">Course Requests</h3>
          <div class="clearfix"></div>
        </div>
        <div id="portlet2" class="panel-collapse collapse in" aria-expanded="true" style="">
          <div class="portlet-body">
            <div class="table-responsive" >
              <table class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Student Name</th>
                  <th>Course Name</th>
                  <th>Requested At</th>
                  <th>Status</th>
                </tr>
                </thead>

                <tbody id="course_request_table">
                @php $request_count=1; @endphp

                @foreach($course_requests as $request)
                  <tr>
                    <td>{{ $request_count++ }}</td>
                    <td><a href="/profile/{{ $request->student_id }}">{{ $request->name }}</a></td>
                    <td><a href="/course/{{ $request->course_id }}">{{ $request->course_name }}</a></td>
                    <td>{{ date('F j, Y', strtotime( $request->created_at )) }}</td>
                    <td>
                      @if($request->status_name == 'accepted')
                        <button class="btn btn-success waves-effect waves-light">{{ ucfirst($request->status_name) }}</button>
                      @elseif($request->status_name == 'requested')
                        <button id="cs_button_{{ $request->course_student_id }}" onclick="course_request('{{ $request->course_student_id }}', '{{ $request->status_name }}')" class="btn btn-warning waves-effect waves-light">{{ ucfirst($request->status_name) }}</button>
                      @endif
                    </td>
                  </tr>

                @endforeach
                </tbody>
              </table>
              {{ $course_requests->links() }}
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="col-lg-6 col-md-6 col-sm-12">
      <div class="portlet"><!-- /primary heading -->
        <div class="portlet-heading">
          <button class="pull-right btn btn-inverse waves-effect waves-light w-lg m-t-30 m-r-15" data-toggle="modal" data-target="#add_category_modal">
          <span class="btn-label">
            <i class="glyphicon glyphicon-plus"></i>
          </span>
            Add Category
          </button>
          <h3 class="text-dark header-title m-t-30 m-b-20">Course Categories</h3>
          <div class="clearfix"></div>
        </div>
        <div id="portlet2" class="panel-collapse collapse in" aria-expanded="true" style="">
          <div class="portlet-body">
            <div class="table-responsive" >
              <table class="table" id="table_category">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Category Name</th>
                  <th>Created By</th>
                  <th style="width: 25%">Action</th>
                </tr>
                </thead>

                <tbody id="category_table">
                @php $cat_count=1; @endphp

                @foreach($course_category as $category)
                  <tr>
                    <td>{{ $cat_count++ }}</td>
                    <td>{{ $category->category_name }}</td>
                    <td>{{ $category->name }}</td>
                    <td>
                      <button type="button" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button>
                      <button type="button" class="btn btn-youtube waves-effect waves-light" onclick="delete_category({{ $category->category_id }})"><i class="ion-close"></i></button>
                    </td>

                  </tr>

                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>


  <div class="row">

    <div class="col-lg-6 col-md-6 col-sm-12">
      <div class="portlet"><!-- /primary heading -->
        <div class="portlet-heading">
          <button class="pull-right btn btn-inverse waves-effect waves-light w-lg m-t-30 m-r-15" data-toggle="modal" data-target="#add_level_modal">
          <span class="btn-label">
            <i class="glyphicon glyphicon-plus"></i>
          </span>
            Add Course Level
          </button>
          <h3 class="text-dark header-title m-t-30 m-b-20">Course Level</h3>
          <div class="clearfix"></div>
        </div>
        <div id="portlet2" class="panel-collapse collapse in" aria-expanded="true" style="">
          <div class="portlet-body">
            <div class="table-responsive" >
              <table class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Course Level Name</th>
                  <th>Created By</th>
                  <th>Action</th>
                </tr>
                </thead>

                <tbody id="level_table">
                @php $level_count=1; @endphp

                @foreach($course_level as $item)
                  <tr>
                    <td>{{ $level_count++ }}</td>
                    <td>{{ ucfirst($item->course_level_name) }}</td>
                    <td>{{ $item->name }}</td>
                    <td>
                      <button type="button" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button>
                      <button type="button" class="btn btn-youtube waves-effect waves-light"><i class="ion-close"></i></button>
                    </td>

                  </tr>

                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </div>

    <!-- col -->

    <div class="col-lg-6">
      <div class="card-box">
        <a href="#" class="pull-right btn btn-primary waves-effect waves-light w-lg m-t-10 m-r-15">
        <span class="btn-label">
          <i class="glyphicon glyphicon-eye-open"></i>
        </span>
          View All Activities
        </a>
        <h4 class="text-dark header-title m-t-20 m-b-20">Recent Activities</h4>

        <p class="text-muted m-b-30 font-13">

        </p>

        <div class="nicescroll p-20" style="height: 295px;">
          <div class="timeline-2">

            @foreach($activities as $activity)

              <div class="time-item">
                <div class="item-info">
                  <div class="text-muted"><small>{{ timeDifference($activity->created_at) }}</small></div>
                  <p><strong><a href="#" class="text-info">{{ $activity->name }}</a></strong> {{ $activity->lookup_activity }} a {{ $activity->lookup_category }} <strong>{{ $activity->lookup_name_child }}</strong></p>
                </div>
              </div>

            @endforeach




          </div>
        </div>

      </div>
    </div>


  </div>


  <div class="row">



    <div class="col-lg-8 col-md-8 col-sm-12">
      <div class="portlet">
        <div class="portlet-heading">
          <a href="/view/courses" class="pull-right btn btn-primary waves-effect waves-light w-lg m-t-30 m-r-15">
          <span class="btn-label">
            <i class="glyphicon glyphicon-eye-open"></i>
          </span>
            View All Course Revenue
          </a>
          <h3 class="text-dark header-title m-t-30 m-b-20">Course Revenue</h3>
          <div class="clearfix"></div>
        </div>
        <div id="portlet2" class="panel-collapse collapse in" aria-expanded="true" style="">
          <div class="portlet-body">
            <div class="table-responsive" >
              <table class="table table-striped m-0">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Course Name</th>
                  <th>Created By</th>
                  <th>Started At</th>
                  <th>Revenue</th>
                </tr>
                </thead>

                <tbody id="course_revenue_table">
                @php $course_revenue_count=1; @endphp

                @foreach($revenues as $item)
                  <tr>
                    <td>{{ $course_revenue_count++ }}</td>
                    <td>{{ $item->course_name }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ date('F j, Y', strtotime( $item->created_at )) }}</td>
                    <td>{{ $item->price * $item->course_count }}</td>
                  </tr>

                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </div>






    <!-- Todos app -->
    <div class="col-lg-4">
      <div class="card-box">
        <h4 class="m-t-0 m-b-20 header-title"><b>Todo</b></h4>
        <div class="todoapp">
          <div class="row">
            <div class="col-sm-6">
              <h4 id="todo-message">
                <span id="todo_remaining"></span> remaining out of
                <span id="todo_total">{{ count($todo_list) }}</span>
              </h4>
            </div>
          </div>

          <ul class="list-group no-margn nicescroll todo-list" style="height: 280px" id="todo_list_span">
            @foreach($todo_list as $item)
              <li class="list-group-item">
                <div class="checkbox checkbox-primary">
                  <input class="todo_done" value="{{ $item->todo_id }}" type="checkbox" {{ $item->checked==1? 'checked':'' }}>
                  <label>{{ $item->todo_name }}</label>
                </div>
              </li>
            @endforeach
          </ul>

          <div name="todo-form" id="todo-form" role="form" class="m-t-20">
            <div class="row">
              <div class="col-sm-9 todo-inputbar">
                <input type="text" id="todo_name" class="form-control" placeholder="Add new todo">
              </div>
              <div class="col-sm-3 todo-send">
                <button class="btn-primary btn-md btn-block btn waves-effect waves-light" type="button" id="todo_add_button">Add</button>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>


  <div id="add_category_modal" class="modal fade" role="dialog" style="padding-right: 17px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Add Course Category</h4>
        </div>
        <div class="modal-body">

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">Category Name</label>
                <input type="text" class="form-control" id="category_name">
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-inverse waves-effect waves-light" id="add_category_button" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
  </div>


  <div id="add_level_modal" class="modal fade" role="dialog" style="padding-right: 17px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Add Course Level</h4>
        </div>
        <div class="modal-body">

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">Level Name</label>
                <input type="text" class="form-control" id="course_level_name">
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-inverse waves-effect waves-light" id="add_level_button" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
  </div>

@endsection
