@extends('layouts.login')

@section('content')


    <div class="wrapper-page">
        <div class=" card-box">
            <div class="panel-heading">
                <h3 class="text-center">Sign In to <strong class="text-custom">3Scholars</strong> </h3>
            </div>


            <div class="panel-body">

                <form class="form-horizontal m-t-20" method="POST" action="{{ route('login') }}" >
                    {{ csrf_field() }}

                    @if ($errors->has('email'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input type="email" name="email" placeholder="Email" class="form-username form-control" id="email">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input type="password" name="password" placeholder="Password" class="form-password form-control" id="password">
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup">
                                    Remember me
                                </label>
                            </div>

                        </div>
                    </div>

                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                        </div>
                    </div>

                    <div class="form-group m-t-30 m-b-0">
                        <div class="col-sm-12">
                            <a href="page-recoverpw.html" class="text-dark"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <p>Don't have an account? <a href="register" class="text-primary m-l-5"><b>Sign Up</b></a></p>

            </div>
        </div>

    </div>


@endsection

