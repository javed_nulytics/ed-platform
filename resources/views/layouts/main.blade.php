<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Javed Nayeem">

    <link rel="shortcut icon" href="/images/icon.png">

    <title>3 Scholars - Scholars.Inspired.</title>

    @include('scholars.partials.pre-scriprts')

</head>

<body class="fixed-left">

<div id="wrapper">

    @include('scholars.partials.topbar')

    @include('scholars.partials.menu')

    <div class="content-page">
        <div class="content">
            <div class="container">

                @include('scholars.partials.page-title')

                <input type="hidden" id="token" value="{{ csrf_token() }}">

                @yield('content')

            </div>
        </div>

        @include('scholars.partials.footer')

    </div>
</div>

@include('scholars.partials.post-scripts')

</body>
</html>