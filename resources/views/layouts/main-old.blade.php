<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Javed Nayeem">

    <link rel="shortcut icon" href="/images/icon.png">

    <title>3 Scholars - Scholars.Inspired.</title>

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="/css/morris.css">

    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/css/components.css" rel="stylesheet" type="text/css" />
    <link href="/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <![endif]-->

    {{--<script src="/js/modernizr.min.js"></script>--}}

    <!-- Sweet Alert css -->
    <link href="/css/sweetalert2.css" rel="stylesheet" type="text/css" />

    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>--}}
    <script src="/js/jquery.min.js"></script>

</head>


<body>


<!-- Navigation Bar-->
@include('scholars.partials.navigation-bar')
<!-- End Navigation Bar-->


<div class="wrapper">
    <div class="container">

        <!-- Page-Title -->
        @include('scholars.partials.page-title')

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        @yield('content')


        <!-- Footer -->
        @include('scholars.partials.footer')
        <!-- End Footer -->

    </div>
</div>



<!-- jQuery  -->
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/detect.js"></script>
<script src="/js/fastclick.js"></script>

<script src="/js/jquery.slimscroll.js"></script>
<script src="/js/jquery.blockUI.js"></script>
<script src="/js/waves.js"></script>
<script src="/js/wow.min.js"></script>
<script src="/js/jquery.nicescroll.js"></script>
<script src="/js/jquery.scrollTo.min.js"></script>

<script src="/js/jquery.peity.min.js"></script>

<!-- jQuery  -->
<script src="/js/jquery.waypoints.js"></script>
<script src="/js/jquery.counterup.min.js"></script>

<script src="/js/morris.min.js"></script>
<script src="/js/raphael-min.js"></script>

<script src="/js/jquery.knob.js"></script>

<script src="/js/jquery.dashboard.js"></script>

<script src="/js/jquery.core.js"></script>
<script src="/js/jquery.app.js"></script>

<!-- Sweet Alert js -->
<script src="/js/sweetalert2.all.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });

        $(".knob").knob();

    });

</script>

<!-- Chart JS -->
<script src="/js/chart.min.js"></script>




</body>
</html>