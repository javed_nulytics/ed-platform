$(document).ready(function() {


    $("#run_scripts_button").click(function(){

        $.ajax({
            url: '/run-scripts',
            type: 'POST',
            format: 'JSON',
            data: {"_token": $('#token').val()},

            success: function (response) {

                swal('Done!', 'You Just Run All The Scripts Manually', 'success');

                reloadCurrentPage();

            },
            error: function (error) {
                swal('Error!', 'Something Went Wrong', 'error');
            }
        });


    });


    $("#db_fix_scripts_button").click(function(){

        $.ajax({
            url: '/db-fix-scripts',
            type: 'POST',
            format: 'JSON',
            data: {"_token": $('#token').val()},

            success: function (response) {

                swal('Done!', 'Database Fixing Scripts', 'success');

                reloadCurrentPage();

            },
            error: function (error) {
                swal('Error!', 'Something Went Wrong', 'error');
            }
        });


    });


    $("#generate_data_button").click(function(){

        $.ajax({
            url: '/generate-data',
            type: 'POST',
            format: 'JSON',
            data: {"_token": $('#token').val()},

            success: function (response) {
                showSuccessNotification('Demo Data Created');
            },
            error: function (error) {
                swal('Error!', 'Something Went Wrong', 'error');
            }
        });


    });



});










