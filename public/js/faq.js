
$(document).ready(function() {


    $('#add_faq_button').click(function(){

        var faq_question = $('#faq_question').val();
        var faq_answer = $('#faq_answer').val();

        if (faq_question != null && faq_answer != null) {

            var params = {
                faq_question: faq_question,
                faq_answer: faq_answer,
                course_id: $('#course_id').val()
            };

            $.ajax({
                url: '/add/faq',
                type: 'POST',
                format: 'JSON',
                data: {'_token': $('#token').val(), params: params},
                success: function(response) {

                    $('#faq_question').val('');
                    $('#faq_answer').val('');

                    showSuccessNotification('Frequently Asked Questions Has Been Saved!');

                    var htmlstr = '<div class="faq-box">' +
                        '<h4 class="question" data-wow-delay=".1s">'+ faq_question +'</h4>' +
                        '<p class="answer">'+ faq_answer +'</p>' +
                        '<div class="m-t-30">' +
                        '<button type="button" class="btn btn-instagram waves-effect waves-light" ' +
                        'onclick="edit_faq(\''+ response +'\', \''+ faq_question +'\', \''+ faq_answer +'\')">' +
                        '<i class="ion-edit"></i>' +
                        '</button> ' +
                        '<button type="button" class="btn btn-youtube waves-effect waves-light" ' +
                        'onclick="delete_faq(\''+ response +'\')">\n' +
                        '<i class="ion-close"></i>' +
                        '</button></div></div>';

                    $('#faq_span').append(htmlstr);

                },
                error: function(error) {
                    showErrorNotification();

                }

            });
        }


    });


    $('#edit_faq_button').click(function () {

        var faq_id = $("#edit_faq_id").val();
        var faq_question = $("#edit_faq_question").val();
        var faq_answer = $("#edit_faq_answer").val();

        var params = {
            faq_id: faq_id,
            faq_question: faq_question,
            faq_answer: faq_answer
        };

        $.ajax({
            url: '/edit/faq',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                showSuccessNotification('FAQ Has Been Edited');
                reloadCurrentPage();

            },
            error: function (error) {
                swal("Error!", "Something went wrong.", "error");
            }
        });

    });



});



function edit_faq(faq_id, faq_question, faq_answer) {

    $('#edit_faq_id').val(faq_id);
    $('#edit_faq_question').val(faq_question);
    $('#edit_faq_answer').val(faq_answer);

    $('#edit_faq_modal').modal('show');
}


function delete_faq(faq_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            faq_id: faq_id
        };

        $.ajax({
            url: '/delete/faq',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $( '#faq_' + faq_id ).remove();
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });

}
