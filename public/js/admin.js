var student_data = [];
var tutor_data = [];
var course_data = [];
var course_category = [];
var course_count = 0;
var years = [];
var year_limits = 5;
var temp_bg_color_previous_color = -1;

var background_color = [
    "#26A65B",
    "#F89406",
    "#913D88",
    "#FF5A5E",
    "#46BFBD",
    "#34495e",
    "#947CB0"
];

$(document).ready(function() {

    $(".pagination").addClass('pull-right');

    $('#table_category').dataTable({
        "searching": false,
        "lengthChange": false,
        "info": false,
        "pageLength": 5
    });


    /*
     ANALYTICS
     */


    for (var i=0; i<year_limits; i++) {
        years.push(parseInt((new Date()).getFullYear() - i));
    }

    var analytics_barChartData = {
        labels: years,
        datasets: [
            {
                backgroundColor: '#ff6384',
                borderWidth: 1,
                data: student_data
            },
            {
                backgroundColor: "#36a2eb",
                borderWidth: 1,
                data: tutor_data
            },
            {
                backgroundColor: "#ffce56",
                borderWidth: 1,
                data: course_data
            }
        ]
    };

    var analytics_chartOptions = {
        responsive: true,
        legend: {
            position: "top",
            display: false
        },
        title: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    userCallback: function(label, index, labels) {
                        if (Math.floor(label) === label) {
                            return label;
                        }

                    }
                }
            }]
        }
    };

    var ctx = document.getElementById("analytics_barChart").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: "bar",
        data: analytics_barChartData,
        options: analytics_chartOptions
    });



    /*
     COURSE CATEGORY ANALYTICS
     */

    var temp_course_category_label = [];
    var temp_course_category_data = [];
    var temp_course_category_color = [];


    for (var i = 0; i < course_category.length; i++) {
        var percentage = Math.ceil(((course_category[i][1]/course_count)*100));
        temp_course_category_label.push(course_category[i][0] + ' ('+ percentage +'%)');
        temp_course_category_data.push(course_category[i][1] );
        temp_course_category_color.push(background_color[getRandomNumber(0, background_color.length-1)]);
    }



    var ctx = document.getElementById("analytics_category_course").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: temp_course_category_label,
            datasets: [{
                backgroundColor: temp_course_category_color,
                data: temp_course_category_data
            }]
        }
    });





    $('#add_category_button').click(function () {

        var category_name = $('#category_name').val();

        if (category_name != '') {

            var params = {
                category_name: category_name
            };

            $.ajax({
                url: '/add/category',
                type: 'POST',
                format: 'JSON',
                data: {'_token': $('#token').val(), params:params},
                success: function(response) {
                    swal('Done!', 'Category Has Been Added', 'success');

                    var htmlstr = '<tr>' +
                        '<td>#</td>' +
                        '<td>'+ category_name +'</td>' +
                        '<td>You</td>' +
                        '<td>'+ getCurrentDate() +'</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button> ' +
                        '<button type="button" class="btn btn-youtube waves-effect waves-light"><i class="ion-close"></i></button></td></tr>';

                    $('#category_table').append(htmlstr);
                },
                error: function (error) {
                    swal('Error!', 'Something Went Wrong', 'error');
                }
            });
        }

    });


    $('#add_level_button').click(function () {

        var course_level_name = $('#course_level_name').val();

        if (course_level_name != '') {

            var params = {
                course_level_name: course_level_name
            };

            $.ajax({
                url: '/add/level',
                type: 'POST',
                format: 'JSON',
                data: {'_token': $('#token').val(), params:params},
                success: function(response) {
                    swal('Done!', 'Level Has Been Added', 'success');

                    var htmlstr = '<tr>' +
                        '<td>#</td>' +
                        '<td>'+ course_level_name +'</td>' +
                        '<td>You</td>' +
                        '<td>'+ getCurrentDate() +'</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button>' +
                        '<button type="button" class="btn btn-youtube waves-effect waves-light"><i class="ion-close"></i></button></td></tr>';

                    $('#level_table').append(htmlstr);
                },
                error: function (error) {
                    swal('Error!', 'Something Went Wrong', 'error');
                }
            });
        }

    });


    



});


function course_request(course_student_id, status) {

    $("#cs_button_" + course_student_id ).removeClass("btn-warning");
    $("#cs_button_" + course_student_id).addClass("btn-danger");
    $("#cs_button_" + course_student_id).text("Accepting ..");

    var params = {
        course_student_id: course_student_id,
        status: status
    };


    $.ajax({
        url: '/course/student/status',
        type: 'POST',
        format: 'JSON',
        data: {params: params, '_token': $('#token').val()},
        success: function (response) {
            //swal('Done!', 'Student Added To The Course', 'success');

            $("#cs_button_" + course_student_id ).removeClass("btn-danger");
            $("#cs_button_" + course_student_id).addClass("btn-success");
            $("#cs_button_" + course_student_id).text("Accepted");

        },
        error: function (error) {
            swal('Error!', 'Something Went Wrong', 'error');
        }

    });



}


function student_array_push(st_1, st_2, st_3, st_4, st_5) {

    student_data.push(parseInt([st_1]));
    student_data.push(parseInt([st_2]));
    student_data.push(parseInt([st_3]));
    student_data.push(parseInt([st_4]));
    student_data.push(parseInt([st_5]));

}


function tutor_array_push(tu_1, tu_2, tu_3, tu_4, tu_5) {

    tutor_data.push(parseInt([tu_1]));
    tutor_data.push(parseInt([tu_2]));
    tutor_data.push(parseInt([tu_3]));
    tutor_data.push(parseInt([tu_4]));
    tutor_data.push(parseInt([tu_5]));

}


function course_array_push(co_1, co_2, co_3, co_4, co_5) {

    course_data.push(parseInt([co_1]));
    course_data.push(parseInt([co_2]));
    course_data.push(parseInt([co_3]));
    course_data.push(parseInt([co_4]));
    course_data.push(parseInt([co_5]));

}


function pushToCategoryArray(category_name, category_count) {
    course_count += parseInt(category_count);
    if (category_count != '0') course_category.push([category_name, category_count]);
}


function getRandomNumber(min, max) {
    var number = Math.floor(Math.random() * (max - min + 1)) + min;
    if (temp_bg_color_previous_color == number) {
        temp_bg_color_previous_color = number;
        return getRandomNumber(min, max);
    }
    else {
        temp_bg_color_previous_color = number;
        return number;
    }
}
