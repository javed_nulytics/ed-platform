var student_data = [];
var tutor_data = [];
var course_data = [];
var course_category = [];
var progress_analytics = [];
var exam_analytics = [];
var course_count = 0;
var years = [];
var year_limits = 5;
var temp_bg_color_previous_color = -1;

var background_color = [
    "#26A65B",
    "#F89406",
    "#913D88",
    "#FF5A5E",
    "#46BFBD",
    "#34495e",
    "#947CB0"
];

var d = new Date();
var current_month = d.getMonth();

for (var i = 0; i <= current_month+1; i++) progress_analytics.push([i, 0, 0]);
for (var i = 0; i <= current_month+1; i++) exam_analytics.push([i, 0, 0]);


$(document).ready(function() {



    /*
     Total Ouiz Progress Analytics
     */
    generateQuizAnalytics();
    generateExamAnalytics();




});


function viewResult(quiz_result_id){
    $('#quiz_result_id').val(quiz_result_id);
    $( "#view_result_form" ).submit();
}


function progress_analytics_push(month, count, score) {
    progress_analytics[month][1] = count;
    progress_analytics[month][2] = score;
}


function exam_analytics_push(month, count, score) {
    exam_analytics[month][1] = count;
    exam_analytics[month][2] = score;
}


function generateQuizAnalytics() {

    var temp_data_count = [];
    var temp_data_score = [];


    for (var i = 1; i < progress_analytics.length; i++) {
        temp_data_count = temp_data_count.concat([progress_analytics[i][1]]);
        temp_data_score = temp_data_score.concat([progress_analytics[i][2]]);
    }


    var ctx = document.getElementById("total_progress_analytics").getContext('2d');

    var options = {
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                },
                ticks: {
                    max: 100,
                    beginAtZero: true
                }
            }]
        }
    };

    var total_progress_analytics = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [
                {
                    label: "Avg Quiz Scores Over Month",
                    borderColor: "#F89406",
                    fill: false,
                    data: temp_data_score
                }
            ]
        },
        options: options
    });

    ctx = document.getElementById("total_taken_analytics").getContext('2d');

    options = {
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    };

    var total_taken_analytics = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [
                {
                    label: "Quiz Taken Over Month",
                    backgroundColor: '#ff6384',
                    fill: true,
                    data: temp_data_count
                }


            ]
        },
        options: options
    });

}


function generateExamAnalytics() {

    var temp_data_count = [];
    var temp_data_score = [];


    for (var i = 1; i < exam_analytics.length; i++) {
        temp_data_count = temp_data_count.concat([exam_analytics[i][1]]);
        temp_data_score = temp_data_score.concat([exam_analytics[i][2]]);
    }


    var ctx = document.getElementById("exam_analytics").getContext('2d');

    var options = {
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                },
                ticks: {
                    max: 100,
                    beginAtZero: true
                }
            }]
        }
    };

    var total_progress_analytics = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [
                {
                    label: "Avg Exam Scores Over Month",
                    borderColor: "#F89406",
                    fill: false,
                    data: temp_data_score
                }
            ]
        },
        options: options
    });

    ctx = document.getElementById("exam_taken_analytics").getContext('2d');

    options = {
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    };

    var total_taken_analytics = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [
                {
                    label: "Exam Taken Over Month",
                    backgroundColor: '#ff6384',
                    fill: true,
                    data: temp_data_count
                }


            ]
        },
        options: options
    });

}


