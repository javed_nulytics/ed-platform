$(document).ready(function() {

    $(".pagination").addClass('pull-right');

    $('#table_category').dataTable({
        "searching": false,
        "lengthChange": false,
        "info": false,
        "pageLength": 5
    });

    $('#tutor_courses_table').dataTable({
        "searching": false,
        "lengthChange": false,
        "info": false,
        "pageLength": 5
    });

    $('#course_request_tutor').dataTable({
        "searching": false,
        "lengthChange": false,
        "info": false,
        "pageLength": 5
    });

});


