var section_count = 1;
var lecture_count = 1;


$(document).ready(function() {

    $("#join_course_button").click(function(){

        var course_id = $("#course_id").val();
        var free_course = $("#free_course").val();

        var params = {
            course_id: course_id,
            free_course: free_course
        };

        $.ajax({
            url: '/enrollment/course',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                showSuccessNotification('You have requested for this course');

                if (free_course == 1) {

                    setTimeout(function(){
                        window.location.href = "/course/" + course_id;
                    },1500);

                }
                else {
                    $('#course_enrollment_div').empty();

                    var htmlstr = '<button type="button" class="btn btn-warning waves-effect waves-light m-l-10">' +
                        '<span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>' +
                        'You have requested for this course</button>';

                    $('#course_enrollment_div').append(htmlstr);
                }

            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });


    $('#edit_course_button').click(function () {
        $( "#edit_course_form" ).submit();
    });


    $('#add_section_button').click(function () {

        var course_id = $("#course_id").val();
        var section_name = $("#modal_section_name").val();

        if (section_name != '') {

            var params = {
                course_id: course_id,
                section_name: section_name
            };

            $.ajax({
                url: '/add/section',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    $('#no_section_msg_div').hide();
                    swal("Done!", "New Section Has Been Created", "success");

                    var key = generate_random_string();

                    $("#modal_section_name").val("");

                    var htmlstr = '<div class="panel panel-default">' +
                        '<div class="panel-heading"><h4 class="panel-title">' +
                        '<a data-toggle="collapse" data-parent="#section_div" href="#collapseOne-'+ section_count +'" aria-expanded="false" class="collapsed">' +
                        'Section #' + section_count;

                    if (section_name != '') htmlstr += ': ' + section_name;

                    htmlstr += '</a></h4></div>' +
                        '<div id="collapseOne-'+ section_count +'" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">' +
                        '<div class="panel-body">' +
                        '<ul class="list-unstyled" style="margin-left: 20px" id="section_'+ response+'">' +
                        '</ul></div></div></div>';

                    $( "#section_div" ).append(htmlstr);


                    htmlstr = '<option value="'+response+'">Section #'+ section_count + ': ' + section_name + '</option>';

                    $( "#select_section_div" ).append(htmlstr);

                    section_count++;

                },
                error: function (error) {
                    swal("Error!", "Something went wrong.", "error");
                }
            });

        }


    });


    $('#add_lecture_button').click(function () {

        var course_id = $("#course_id").val();
        var section_id = $("#select_section_div").val();
        var lecture_title = $("#lecture_title").val();

        if (lecture_title != '') {

            var params = {
                course_id: course_id,
                section_id: section_id,
                lecture_title: lecture_title
            };

            $.ajax({
                url: '/add/lecture',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    swal("Done!", "Lecture Has Been Added", "success");

                    var htmlstr = '<a href="/lecture/'+ response +'">' +
                        '<li><i class="fa fa-play-circle-o"></i> Lecture-'+ lecture_count +': '+ lecture_title +'</li></a>';

                    $( "#span_section_" + section_id ).append(htmlstr);

                },
                error: function (error) {
                    swal("Error!", "Something went wrong.", "error");
                }
            });

        }



    });


    $('#edit_section_button').click(function () {

        var section_id = $("#edit_section_id").val();
        var section_name = $("#edit_section_name").val();

        var params = {
            section_id: section_id,
            section_name: section_name
        };

        $.ajax({
            url: '/edit/section',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                showSuccessNotification('Section Has Been Edited');
                reloadCurrentPage();

            },
            error: function (error) {
                swal("Error!", "Something went wrong.", "error");
            }
        });

    });


});



function generate_random_string() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}


function set_section_count(n) {
    section_count = n;
}


function set_lecture_count(n) {
    lecture_count = n;
}


function edit_section(section_id, section_name) {

    $('#edit_section_id').val(section_id);
    $('#edit_section_name').val(section_name);

    $('#edit_section_modal').modal('show');
}


function delete_section(section_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            section_id: section_id
        };

        $.ajax({
            url: '/delete/section',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $( '#section_' + section_id ).remove();
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });

}








