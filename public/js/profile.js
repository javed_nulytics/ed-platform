$(document).ready(function() {

    $("#edit_password_button").click(function(){

        var old_password = $("#old_password").val();
        var new_password = $("#new_password").val();
        var retype_password = $("#retype_password").val();


        if (old_password == "") swal("", "Old Password Needed", "warning");

        else if (new_password != retype_password) swal("", "Password Mismatched", "warning");

        else {

            var params = {
                old_password: old_password,
                new_password: new_password
            };

            $.ajax({
                url: '/edit/password',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    var json_message = $.parseJSON(response);

                    $("#old_password").val('');
                    $("#new_password").val('');
                    $("#retype_password").val('');

                    if (json_message == 'success') showSuccessNotification('Successfully Changed Your Password');
                    else showErrorNotification('Something Went Wrong.');
                },
                error: function (error) {
                    showErrorNotification('Something Went Wrong.');
                }
            });
        }

    });

});










