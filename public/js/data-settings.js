$(document).ready(function() {


    $('#table_truncate_button').click(function () {

        var table_names = [];
        var count = 0;

        $('.table_truncate:checked').each(function () {
            table_names[count++] = $(this).val();
        });

        var params = {
            table_names: table_names
        };

        $.ajax({

            url: '/table/truncate',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                swal('Done!', 'Table Truncated', 'success');

                reloadCurrentPage();
            },
            error: function (error) {
                swal('Error!', 'Something Went Wrong', 'error');
            }

        });

    });


    $('#table_truncate_toggle').click(function () {

        $('.table_truncate').each(function () { this.checked = !this.checked; });

    });


    $('#table_drop_button').click(function () {

        var table_names = [];
        var count = 0;

        $('.table_drop:checked').each(function () {
            table_names[count++] = $(this).val();
        });

        var params = {
            table_names: table_names
        };

        $.ajax({

            url: '/table/drop',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                swal('Done!', 'Table Dropped', 'success');

                reloadCurrentPage();
            },
            error: function (error) {
                swal('Error!', 'Something Went Wrong', 'error');
            }

        });

    });


    $('#table_drop_toggle').click(function () {

        $('.table_drop').each(function () { this.checked = !this.checked; });

    });





});

