var question_number = 1;
var correct_answer = [];
var submitted_ans = [];
var duration = 0;
var timeinterval;


$( window ).load(function() {

    var exam_started = sessionStorage.getItem('exam_started');
    var duration_flag = sessionStorage.getItem('duration_flag');
    var h = sessionStorage.getItem('h');
    var m = sessionStorage.getItem('m');
    var s = sessionStorage.getItem('s');

    if (exam_started == '1') {

        $('.preExamSpan').hide();
        $('.postExamSpan').show();


        if (duration_flag == '1') {

            var temp_duration = (parseInt(h) * 60) + parseInt(m) + (parseInt(s)>0? 1:0);
            check_duration(temp_duration);

        }


    }

});


$(document).ready(function() {


    $('#scheduled').change(function () {

        var scheduled = ( $("#scheduled").is(':checked') ) ? 1 : 0;
        if (scheduled) $('#schedule_div').show();
        else $('#schedule_div').hide();

    });


    $('.bootstrap-touchspin-down').click(function () {
        var duration = $('#duration').val();
        duration = parseInt(duration);
        if (duration > 0) duration--;
        $('#duration').val(duration);
    });


    $('.bootstrap-touchspin-up').click(function () {
        var duration = $('#duration').val();
        duration = parseInt(duration);
        duration++;
        $('#duration').val(duration);
    });



});


function pushToDurationArray(time) {
    duration = parseInt(time);
}


function pushToAnswerArray(answer, question_type, question_id) {
    correct_answer.push([answer, question_type, question_id]);
}


function create_question(question_type) {

    if (question_type == 'mcq') {

        var htmlstr = '<div class="card-box" id="question_div_'+ question_number +'">' +
            '<input type="hidden" name="question_type[]" value="mcq">'+
            '<div class="form-group">' +
            '<label class="col-md-2 control-label">Question '+ question_number +'</label>' +
            '<div class="col-md-8">' +
            '<input type="text" class="form-control" placeholder="Write your question here" name="question[]">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-md-2 control-label">Option 1</label>' +
            '<div class="col-md-8">' +
            '<input type="text" class="form-control" placeholder="Option 1" id="q_'+ question_number +'_o_1" name="option[]" onkeyup="lookup(this);">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-md-2 control-label">Option 2</label>' +
            '<div class="col-md-8"><input type="text" class="form-control" id="q_'+ question_number +'_o_2" placeholder="Option 2" name="option[]" onkeyup="lookup(this);">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Option 3</label><div class="col-md-8">' +
            '<input type="text" class="form-control" placeholder="Option 3" id="q_'+ question_number +'_o_3" name="option[]" onkeyup="lookup(this);">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Option 4</label><div class="col-md-8">' +
            '<input type="text" class="form-control" placeholder="Option 4" id="q_'+ question_number +'_o_4" name="option[]" onkeyup="lookup(this);">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-md-2 control-label">Select Correct Answer</label>' +
            '<div class="col-md-8">' +
            '<select class="form-control" name="answer[]" id="select_answer_'+ question_number +'">' +
            '<option value="1" id="select_'+ question_number +'_option_1"></option>' +
            '<option value="2" id="select_'+ question_number +'_option_2"></option>' +
            '<option value="3" id="select_'+ question_number +'_option_3"></option>' +
            '<option value="4" id="select_'+ question_number +'_option_4"></option>' +
            '</select>' +
            '</div>' +
            '</div>'+
            '<div class="form-group"><label class="col-md-2 control-label">Media</label><div class="col-md-8">' +
            '<input type="file" class="form-control" name="media_'+ question_number +'" accept="image/*">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Explanation</label><div class="col-md-8">' +
            '<input type="text" class="form-control" name="explanation[]">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Explanation Media</label><div class="col-md-8">' +
            '<input type="file" class="form-control" name="explanation_media_'+ question_number +'" accept="image/*">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<div class="col-md-12 text-center m-t-15"><div class="btn-group">' +
            '<button type="button" class="btn btn-success btn-rounded waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">' +
            '<span class="btn-label"><i class="fa fa-plus"></i></span>Add Question &nbsp;<span class="caret"></span>' +
            '</button>' +
            '<ul class="dropdown-menu" role="menu"><li onclick="create_question(\'mcq\')"><a href="#">MCQ</a></li><li onclick="create_question(\'text\')">' +
            '<a href="#">Text</a></li></ul>' +
            '</div>' +
            '<button type="button" class="btn btn-danger btn-rounded waves-effect waves-light m-l-10">' +
            '<span class="btn-label"><i class="fa fa-times"></i></span>Delete Question '+ question_number +'' +
            '</button>' +
            '</div>' +
            '</div>' +
            '</div>';

        $('#exam_div').append(htmlstr);
    }

    else if (question_type == 'text') {

        var htmlstr = '<div class="card-box" id="question_div_'+question_number+'">' +
            '<input type="hidden" name="question_type[]" value="text">' +
            '<div class="form-group">' +
            '<label class="col-md-2 control-label">Question '+question_number+'</label>' +
            '<div class="col-md-8">' +
            '<input type="text" class="form-control" placeholder="Write your question here" name="question[]">' +
            '</div></div>' +
            '<div class="form-group">' +
            '<label class="col-md-2 control-label">Answer</label>' +
            '<div class="col-md-8">' +
            '<input type="text" class="form-control" placeholder="Answer" name="answer[]">' +
            '</div></div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Media</label><div class="col-md-8">' +
            '<input type="file" class="form-control" name="media_'+ question_number +'" accept="image/*">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Explanation</label><div class="col-md-8">' +
            '<input type="text" class="form-control" name="explanation[]">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Explanation Media</label><div class="col-md-8">' +
            '<input type="file" class="form-control" name="explanation_media_'+ question_number +'" accept="image/*">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><div class="col-md-12 text-center m-t-15"><div class="btn-group"><button type="button" class="btn btn-success btn-rounded waves-effect waves-light" data-toggle="dropdown" aria-expanded="false"><span class="btn-label"><i class="fa fa-plus"></i></span>Add Question &nbsp;<span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li onclick="create_question(\'mcq\')"><a href="#">MCQ</a></li><li onclick="create_question(\'text\')"><a href="#">Text</a></li></ul></div><button type="button" class="btn btn-danger btn-rounded waves-effect waves-light m-l-10"><span class="btn-label"><i class="fa fa-times"></i></span>Delete Question '+question_number+'</button></div></div></div>';

        $('#exam_div').append(htmlstr);
    }

    question_number++;
}


function clear_question() {
    $('#exam_div').empty();
    question_number = 1;
}


function form_submit() {
    if (question_number>1) $('#exam_form').submit();
    else {
        showWarningNotification('Please add minimun one question');
    }
}


function lookup(arg){
    var id = arg.getAttribute('id');
    var value = arg.value;
    var res = id.split("_");

    var question_number = res[1];
    var option_number = res[3];

    $('#select_'+question_number+'_option_'+option_number+'').text(value);
    $('#select_'+question_number+'_option_'+option_number+'').val(value);
}


function start_exam(exam_id) {
    $('#exam_id').val(exam_id);
    $('#start_exam_form').submit();
}


function startExam() {

    sessionStorage.setItem("exam_started", "1");
    sessionStorage.setItem("duration_flag", duration==0? '0':'1');

    $('.preExamSpan').hide();
    $('.postExamSpan').show();

    check_duration(duration);

}


function check_duration(duration) {

    if (duration > 0) {
        var time_in_minutes = duration;
        var current_time = Date.parse(new Date());
        var deadline = new Date(current_time + time_in_minutes*60*1000);

        run_clock('clockdiv',deadline);
    }
    else {
        $('#time_span_div').hide();
    }

}


function time_remaining(endtime){
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor( (t/1000) % 60 );
    var minutes = Math.floor( (t/1000/60) % 60 );
    var hours = Math.floor( (t/(1000*60*60)) % 24 );
    var days = Math.floor( t/(1000*60*60*24) );
    return {'total':t, 'days':days, 'hours':hours, 'minutes':minutes, 'seconds':seconds};
}


function run_clock(id,endtime){
    var clock = document.getElementById(id);
    function update_clock(){
        var t = time_remaining(endtime);

        var h = t.hours < 10 ? '0' + t.hours : t.hours;
        var m = t.minutes < 10 ? '0' + t.minutes : t.minutes;
        var s = t.seconds < 10 ? '0' + t.seconds : t.seconds;

        sessionStorage.setItem("h", t.hours);
        sessionStorage.setItem("m", t.minutes);
        sessionStorage.setItem("s", t.seconds);

        clock.innerHTML = h + ':' + m + ':' + s;
        //console.log('t.total: ' + t.total);
        if (t.total == 0) {
            submit_exam_button();
        }
        else if(t.total<=0){ clearInterval(timeinterval); }

    }
    update_clock();
    timeinterval = setInterval(update_clock,1000);
}


function submit_exam_button() {

    sessionStorage.setItem("exam_started", "0");

    var total_question = correct_answer.length;

    for (var i=0; i<correct_answer.length; i++) {

        var type = correct_answer[i][1];
        var question_id = correct_answer[i][2];
        var temp_submitted_ans = '';

        if (type == 'mcq') temp_submitted_ans = $('input[name=question_'+ i +']:checked').val();
        else if (type == 'text') temp_submitted_ans = $('input[name=question_'+ i +']').val();

        if (temp_submitted_ans == undefined) temp_submitted_ans = '';

        submitted_ans.push([temp_submitted_ans, question_id]);
    }

    var true_answer = checkResults();

    var score = (true_answer / total_question) * 100;
    score = score.toPrecision(3);

    document.getElementById("submit_exam_button").disabled = true;

    var params = {
        exam_id: $("#exam_id").val(),
        answered_question: true_answer,
        total_question: total_question,
        score: score,
        submitted_ans: submitted_ans
    };

    $.ajax({
        url: '/store/exam/result',
        type: 'POST',
        format: 'JSON',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {
            window.location.href = '/exam/result/' + response;
        },
        error: function (error) {
            showErrorNotification();
        }
    });

}


function checkResults() {

    var total_question = correct_answer.length;
    var true_answer = 0;

    for (var i=0; i<correct_answer.length; i++) {

        var c_ans = correct_answer[i][0];
        var s_ans = submitted_ans[i][0];

        if (s_ans == c_ans) true_answer++;
    }

    return true_answer;
}


function delete_exam(exam_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            exam_id: exam_id
        };

        $.ajax({
            url: '/delete/exam',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $( '#exam_' + exam_id ).remove();
                showSuccessNotification('This Exam Has Been Deleted');
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });

}
