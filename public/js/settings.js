var default_user_image = 'default_user.png';
var user_role = [];

$(document).ready(function() {

    $('#save_settings_button').click(function () {

        var app_name = $('#app_name').val();
        var app_title = $('#app_title').val();
        var app_subtitle = $('#app_subtitle').val();
        var app_version = $('#app_version').val();
        var youtube_video_link = $('#youtube_video_link').val();

        var params = {
            app_name: app_name,
            app_title: app_title,
            app_subtitle: app_subtitle,
            app_version: app_version,
            youtube_video_link: youtube_video_link
        };

        $.ajax({
            url: '/save/app-settings/',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), params: params},

            success: function (response) {
                showSuccessNotification('App Settings Successfully Saved');
            },
            error: function (error) {
                showErrorNotification();
            }
        });
    });


    $('#add_lookup_button').click(function () {

        var lookup_name = $('#lookup_name').val();

        if (lookup_name != null) {
            var params = {
                lookup_name: lookup_name
            };

            $.ajax({
                url: '/add/lookup/',
                type: 'POST',
                format: 'JSON',
                data: {'_token': $('#token').val(), params: params},
                success: function (response) {
                    $('#lookup_name').val('');
                    swal('Done!', 'Look Up Added', 'success');

                    var htmlstr = '<tr>' +
                        '<th scope="row">#</th>' +
                        '<td>'+ lookup_name +'</td>' +
                        '<td>You</td>' +
                        '<td>'+ getCurrentDate() +'</td>' +
                    '</tr>';

                    $('#lookup_table').append(htmlstr);
                },
                error: function (error) {
                    swal('Error!', 'Something Went Wrong', 'error');
                }
            });
        }
    });


    $('#add_level_button').click(function () {

        var course_level_name = $('#course_level_name').val();

        if (course_level_name != null) {
            var params = {
                course_level_name: course_level_name
            };

            $.ajax({
                url: '/add/course/level',
                type: 'POST',
                format: 'JSON',
                data: {'_token': $('#token').val(), params: params},
                success: function (response) {
                    $('#course_level_name').val('');
                    swal('Done!', 'Course Level Added', 'success');

                    var htmlstr = '<tr>' +
                        '<th scope="row">#</th>' +
                        '<td>'+ course_level_name +'</td>' +
                        '<td>You</td>' +
                        '<td>'+ getCurrentDate() +'</td>' +
                        '</tr>';

                    $('#course_level_table').append(htmlstr);
                },
                error: function (error) {
                    swal('Error!', 'Something Went Wrong', 'error');
                }
            });
        }
    });


    $('#add_role_button').click(function () {

        var role_name = $('#role_name').val();

        if (role_name != null) {
            var params = {
                role_name: role_name
            };

            $.ajax({
                url: '/add/role',
                type: 'POST',
                format: 'JSON',
                data: {'_token': $('#token').val(), params: params},
                success: function (response) {
                    $('#role_name').val('');
                    swal('Done!', 'Role Added', 'success');

                    var htmlstr = '<tr>' +
                        '<th scope="row">#</th>' +
                        '<td>'+ role_name +'</td>' +
                        '<td>You</td>' +
                        '<td>'+ getCurrentDate() +'</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button>' +
                        '<button type="button" class="btn btn-youtube waves-effect waves-light"><i class="ion-close"></i></button>' +
                        '</td></tr>';

                    $('#role_table').append(htmlstr);
                },
                error: function (error) {
                    swal('Error!', 'Something Went Wrong', 'error');
                }
            });
        }
    });


    $('#add_user_button').click(function () {

        var name = $('#name').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var role = $('#role_selected').val();
        var user_description = $('#user_description').val();


        if (name != null && email != null && password.length>=6) {
            var params = {
                name: name,
                email: email,
                password: password,
                role: role,
                user_description: user_description
            };

            $.ajax({
                url: '/add/user',
                type: 'POST',
                format: 'JSON',
                data: {'_token': $('#token').val(), params: params},
                success: function (response) {

                    $('#name').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('#user_description').val('');

                    showSuccessNotification('User Has Been Added');

                    var role_str = '';
                    for(var i=0; i<user_role.length; i++) {
                        console.log(user_role[i]);
                        role_str += '<option value="'+user_role[i]+'-' + response +'">'+ user_role[i] +'</option>';
                    }
                    console.log(role_str);

                    var htmlstr = '<tr>' +
                        '<td>#</td>' +
                        '<td><img src="/images/users/default_user.png" alt="image" class="img-responsive thumb-md img-circle"></td>' +
                        '<td><a href="/profile/'+response+'">'+ name +'</a></td>' +
                        '<td>'+ email +'</td>' +
                        '<td><button type="button" class="btn btn-warning btn-custom waves-effect waves-light">Change Password</button></td>' +
                        '<td>' +
                        '<select class="form-control user-role">' + role_str + '</select>' +
                        '</td>' +
                        '<td>Active</td>' +
                        '<td>'+ getCurrentDate() +'</td>' +
                        '<td style="width: 14%">' +
                        '<a href="/profile/'+ response +'" class="btn btn-twitter waves-effect waves-light"><i class="ion-ios7-eye"></i></a>' +
                        '<button type="button" class="btn btn-instagram waves-effect waves-light"><i class="ion-edit"></i></button>' +
                        '<button type="button" class="btn btn-youtube waves-effect waves-light"><i class="ion-close"></i></button>' +
                        '</td></tr>';

                    $('#user_table').append(htmlstr);
                },
                error: function (error) {
                    showErrorNotification('Something Went Wrong');
                }
            });
        }

        else {
            showWarningNotification('Missing Field');
        }
    });


    $('.user-role').on('change', function() {

        var str = this.value;
        var key = str.split("-");

        var params = {
            user_id: key[1],
            role: key[0]
        };

        $.ajax({
            url: '/edit/user/role',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                showSuccessNotification('User Role Has Been Changed');
            },
            error: function (error) {
                showErrorNotification('Something Went Wrong');
            }
        });
    });


    $('#edit_user_button').click(function () {

        var id = $('#edit_user_id').val();
        var name = $('#edit_name').val();
        var role = $('#edit_role_selected').val();
        var user_description = $('#edit_user_description').val();


        if (name != null) {
            var params = {
                id: id,
                name: name,
                role: role,
                user_description: user_description
            };

            $.ajax({
                url: '/edit/user',
                type: 'POST',
                format: 'JSON',
                data: {'_token': $('#token').val(), params: params},
                success: function (response) {

                    reloadCurrentPage();

                    showSuccessNotification('User Information Has Been Updated');

                },
                error: function (error) {
                    showErrorNotification('Something Went Wrong');
                }
            });
        }

        else {
            showWarningNotification('Missing Field');
        }
    });


    $("#edit_password_button").click(function(){

        var new_password = $("#new_password").val();
        var retype_password = $("#retype_password").val();
        var user_id = $("#new_password_user_id").val();

        if (new_password != retype_password) swal("", "Password Mismatched", "warning");

        else {

            var params = {
                new_password: new_password,
                user_id: user_id
            };

            $.ajax({
                url: '/change/password',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    var json_message = $.parseJSON(response);

                    $("#new_password").val('');
                    $("#retype_password").val('');

                    if (json_message == 'success') showSuccessNotification('Successfully Changed Password');
                    else showErrorNotification('Something Went Wrong.');
                },
                error: function (error) {
                    showErrorNotification('Something Went Wrong.');
                }
            });
        }

    });



});


function pushToRoleArray(role) {
    user_role.push(role);
}


function editCourse(course_id) {

  $( "#form_course_id" ).val(course_id);
  $( "#edit_course_form" ).submit();

}


function edit_user(id, name, email, role, user_description) {

    $('#edit_user_id').val(id);
    $('#edit_name').val(name);
    $('#edit_email').val(email);
    $('#edit_role_selected').val(role);
    $('#edit_user_description').val(user_description);

    $('#edit_user_modal').modal('show');



}


function change_password(user_id) {

    $('#new_password_user_id').val(user_id);
    
}
