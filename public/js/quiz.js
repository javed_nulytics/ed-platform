var question_number = 1;
var correct_answer = [];
var submitted_ans = [];

$(document).ready(function() {


    $("#submit_quiz_button").click(function(){


        var total_question = correct_answer.length;


        for (var i=0; i<correct_answer.length; i++) {

            var type = correct_answer[i][1];
            var question_id = correct_answer[i][2];
            var temp_submitted_ans = '';

            if (type == 'mcq') temp_submitted_ans = $('input[name=question_'+ i +']:checked').val();
            else if (type == 'text') temp_submitted_ans = $('input[name=question_'+ i +']').val();

            if (temp_submitted_ans == undefined) temp_submitted_ans = '';
            submitted_ans.push([temp_submitted_ans, question_id]);
        }

        var true_answer = checkResults();
        var score = (true_answer / total_question) * 100;
        score = score.toPrecision(3);

        document.getElementById("submit_quiz_button").disabled = true;

        var params = {
            quiz_id: $("#quiz_id").val(),
            answered_question: true_answer,
            total_question: total_question,
            score: score,
            submitted_ans: submitted_ans
        };


        $.ajax({
            url: '/store/result',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                window.location.href = '/quiz/result/' + response;
            },
            error: function (error) {
                showErrorNotification();
            }
        });
    });


});


function pushToAnswerArray(answer, question_type, question_id) {
    correct_answer.push([answer, question_type, question_id]);
}


function checkResults() {

    var total_question = correct_answer.length;
    var true_answer = 0;

    for (var i=0; i<correct_answer.length; i++) {

        var c_ans = correct_answer[i][0];
        var s_ans = submitted_ans[i][0];

        if (s_ans == c_ans) true_answer++;
    }

    return true_answer;
}


function create_question(question_type) {

    if (question_type == 'mcq') {

        var htmlstr = '<div class="card-box" id="question_div_'+ question_number +'">' +
            '<input type="hidden" name="question_id[]" value="0">'+
            '<input type="hidden" name="question_type[]" value="mcq">'+
            '<div class="form-group">' +
            '<label class="col-md-2 control-label">Question '+ question_number +'</label>' +
            '<div class="col-md-8">' +
            '<input type="text" class="form-control" placeholder="Write your question here" name="question[]">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-md-2 control-label">Option 1</label>' +
            '<div class="col-md-8">' +
            '<input type="text" class="form-control" placeholder="Option 1" id="q_'+ question_number +'_o_1" name="option[]" onkeyup="lookup(this);">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-md-2 control-label">Option 2</label>' +
            '<div class="col-md-8"><input type="text" class="form-control" id="q_'+ question_number +'_o_2" placeholder="Option 2" name="option[]" onkeyup="lookup(this);">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Option 3</label><div class="col-md-8">' +
            '<input type="text" class="form-control" placeholder="Option 3" id="q_'+ question_number +'_o_3" name="option[]" onkeyup="lookup(this);">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Option 4</label><div class="col-md-8">' +
            '<input type="text" class="form-control" placeholder="Option 4" id="q_'+ question_number +'_o_4" name="option[]" onkeyup="lookup(this);">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-md-2 control-label">Select Correct Answer</label>' +
            '<div class="col-md-8">' +
            '<select class="form-control" name="answer[]" id="select_answer_'+ question_number +'">' +
            '<option value="1" id="select_'+ question_number +'_option_1"></option>' +
            '<option value="2" id="select_'+ question_number +'_option_2"></option>' +
            '<option value="3" id="select_'+ question_number +'_option_3"></option>' +
            '<option value="4" id="select_'+ question_number +'_option_4"></option>' +
            '</select>' +
            '</div>' +
            '</div>'+
            '<div class="form-group"><label class="col-md-2 control-label">Media</label><div class="col-md-8">' +
            '<input type="file" class="form-control" name="media_'+ question_number +'" accept="image/*">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Explanation</label><div class="col-md-8">' +
            '<input type="text" class="form-control" name="explanation[]">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Explanation Media</label><div class="col-md-8">' +
            '<input type="file" class="form-control" name="explanation_media_'+ question_number +'" accept="image/*">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<div class="col-md-12 text-center m-t-15"><div class="btn-group">' +
            '<button type="button" class="btn btn-success btn-rounded waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">' +
            '<span class="btn-label"><i class="fa fa-plus"></i></span>Add Question &nbsp;<span class="caret"></span>' +
            '</button>' +
            '<ul class="dropdown-menu" role="menu"><li onclick="create_question(\'mcq\')"><a href="#">MCQ</a></li><li onclick="create_question(\'text\')">' +
            '<a href="#">Text</a></li></ul>' +
            '</div>' +
            '<button type="button" onclick="delete_question('+ question_number +')" class="btn btn-danger btn-rounded waves-effect waves-light m-l-10">' +
            '<span class="btn-label"><i class="fa fa-times"></i></span>Delete Question '+ question_number +'' +
            '</button>' +
            '</div>' +
            '</div>' +
            '</div>';

        $('#quiz_div').append(htmlstr);
    }

    else if (question_type == 'text') {

        var htmlstr = '<div class="card-box" id="question_div_'+question_number+'">' +
            '<input type="hidden" name="question_id[]" value="0">' +
            '<input type="hidden" name="question_type[]" value="text">' +
            '<div class="form-group">' +
            '<label class="col-md-2 control-label">Question '+question_number+'</label>' +
            '<div class="col-md-8">' +
            '<input type="text" class="form-control" placeholder="Write your question here" name="question[]">' +
            '</div></div>' +
            '<div class="form-group">' +
            '<label class="col-md-2 control-label">Answer</label>' +
            '<div class="col-md-8">' +
            '<input type="text" class="form-control" placeholder="Answer" name="answer[]">' +
            '</div></div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Media</label><div class="col-md-8">' +
            '<input type="file" class="form-control" name="media_'+ question_number +'" accept="image/*">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Explanation</label><div class="col-md-8">' +
            '<input type="text" class="form-control" name="explanation[]">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><label class="col-md-2 control-label">Explanation Media</label><div class="col-md-8">' +
            '<input type="file" class="form-control" name="explanation_media_'+ question_number +'" accept="image/*">' +
            '</div>' +
            '</div>' +
            '<div class="form-group"><div class="col-md-12 text-center m-t-15"><div class="btn-group"><button type="button" class="btn btn-success btn-rounded waves-effect waves-light" data-toggle="dropdown" aria-expanded="false"><span class="btn-label"><i class="fa fa-plus"></i></span>Add Question &nbsp;<span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li onclick="create_question(\'mcq\')"><a href="#">MCQ</a></li><li onclick="create_question(\'text\')"><a href="#">Text</a></li></ul></div>' +
            '<button type="button" onclick="delete_question('+ question_number +')" class="btn btn-danger btn-rounded waves-effect waves-light m-l-10"><span class="btn-label"><i class="fa fa-times"></i></span>Delete Question '+question_number+'</button></div></div></div>';

        $('#quiz_div').append(htmlstr);
    }

    question_number++;
}


function clear_question() {
    $('#quiz_div').empty();
    question_number = 1;
}


function form_submit() {
    if (question_number>1) $('#quiz_form').submit();
    else {
        swal('', 'Please add minimun one question', 'warning');
    }
}


function lookup(arg){
    var id = arg.getAttribute('id');
    var value = arg.value;
    var res = id.split("_");

    var question_number = res[1];
    var option_number = res[3];

    $('#select_'+question_number+'_option_'+option_number+'').text(value);
    $('#select_'+question_number+'_option_'+option_number+'').val(value);
}


function delete_question(question_no) {

    $('#question_div_' + question_no).remove();

}


function set_question_no(q_no) {

    this.question_number = q_no;

}


function delete_quiz_question(question_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            question_id: question_id
        };

        $.ajax({
            url: '/delete/quiz-question',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $( '#quiz_question_' + question_id ).remove();
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });

}








