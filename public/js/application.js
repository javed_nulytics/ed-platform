var months    = ['January','February','March','April','May','June','July','August','September','October','November','December'];

$(document).ready(function() {

    var todo_list_checked_count = 0;

    $(".todo_done:checked").each(function() {
        todo_list_checked_count++;
    });

    var todo_remain = ($('#todo_total').text() - todo_list_checked_count);

    $('#todo_remaining').text(todo_remain);


    $('#todo_add_button').click(function () {

        var todo_name = $('#todo_name').val();
        if (todo_name != '') {

            var params = {
                todo_name: todo_name
            };

            $.ajax({
                url: '/add/todo',
                type: 'POST',
                format: 'JSON',
                data: {'_token': $("#token").val(), params: params},
                success: function (response) {

                    $('#todo_name').val('');

                    var todo_total = $('#todo_total').text();
                    $('#todo_total').text(++todo_total);

                    var todo_remaining = $('#todo_remaining').text();
                    $('#todo_remaining').text(++todo_remaining);

                    var htmlstr = '<li class="list-group-item">' +
                        '<div class="checkbox checkbox-primary">' +
                        '<input class="todo-done" value="'+ response +'" type="checkbox">' +
                        '<label>'+ todo_name +'</label>' +
                        '</div>' +
                        '</li>';

                    $( '#todo_list_span' ).prepend( htmlstr );

                    showSuccessNotification('Successfully ToDo List Has Been Saved');

                },
                error: function (error) {
                    showErrorNotification();
                }
            });
        }

    });


    $('.todo_done').change(function() {

        var todo_id = $(this).val();
        var checked = 0;

        if ($(this).is(":checked")) checked = 1;

        var params = {
            todo_id: todo_id,
            checked: checked
        };

        $.ajax({
            url: '/change/todo/status',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), params: params},
            success: function(response) {

                var todo_remain = $('#todo_remaining').text();

                if (checked) {
                    $('#todo_remaining').text(--todo_remain);
                    showSuccessNotification('Task Checked');
                }
                else {
                    $('#todo_remaining').text(++todo_remain);
                    showWarningNotification('Task Unchecked');
                }
            },
            error: function(error) {
                showErrorNotification();
            }

        });

    });


    $('#clear_request_button').click(function () {

        $.ajax({
            url: '/clear/course/request',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val()},
            success: function (response) {
                showSuccessNotification('All Course Request Has Been Cleared Out');
                $('#course_request_table').empty();
            },
            error: function (error) {
                showErrorNotification('Something Went Wrong');
            }
        });
    });


    $('#search_course').on('keypress', function (e) {
        if(e.which === 13){
            var search_key = $('#search_course').val();

            window.location.href = '/courses/' + search_key;

        }
    });


});


function getCurrentDate() {

    var now = new Date();
    var thisMonth = months[now.getMonth()];
    var date = now.getDate();
    var year = now.getFullYear();

    var today = thisMonth + ' ' + date + ', ' + year;
    return today;
}


function showSuccessNotification(message) {
    $.Notification.notify('success','top left','Done!', message);
}


function showWarningNotification(message) {
    $.Notification.notify('warning','top left','Warning!', message);
}


function showErrorNotification(message) {
    message = typeof message !== 'undefined' ? message : 'Something Went Wrong!';
    $.Notification.notify('error','top left','Error!', message);
}


function editCourse(course_id){
    $('#form_course_id').val(course_id);
    $( "#edit_course_form" ).submit();
}


function delete_course(course_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            course_id: course_id
        };

        $.ajax({
            url: '/delete/course',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                showSuccessNotification('This Course Has Been Deleted');
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });
}


function reloadCurrentPage() {
    window.location = window.location.pathname;
}


function delete_enrollment(course_student_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            course_student_id: course_student_id
        };

        $.ajax({
            url: '/delete/enrollment',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $('#course_student_' + course_student_id).remove();
                showSuccessNotification('This Enrollment Has Been Deleted');
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });
}


function reloadCurrentPage() {
    window.location = window.location.pathname;
}


function delete_category(category_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            category_id: category_id
        };

        $.ajax({
            url: '/delete/category',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                showSuccessNotification('This Category Has Been Deleted');
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });
}


function delete_quiz(quiz_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            quiz_id: quiz_id
        };

        $.ajax({
            url: '/delete/quiz',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $( '#quiz_' + quiz_id ).remove();
                showSuccessNotification('This Quiz Has Been Deleted');
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });

}


function delete_user(user_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            user_id: user_id
        };

        $.ajax({
            url: '/delete/user',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $( '#user_' + user_id ).remove();
                showSuccessNotification('This User Has Been Deleted');
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });

}
