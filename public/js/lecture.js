$(document).ready(function() {


    $('#edit_lecture_button').click(function () {

        var section_id = $('#select_section_div').val();
        var lecture_title = $('#lecture_title').val();
        var lecture_id = $('#lecture_id').val();
        var lecture_description = $('#lecture_description').val();

        var params = {
            lecture_id: lecture_id,
            section_id: section_id,
            lecture_title: lecture_title,
            lecture_description: lecture_description
        };

        $.ajax({
            url: '/edit/lecture',
            type: 'POST',
            format: 'JSON',
            data: {params: params, '_token': $('#token').val()},
            success: function (response) {
                window.location.href = "/lecture/" + lecture_id;
            },
            error: function(error) {
                swal('Error!', 'Something went wrong', 'error');
            }
        });

    });


    $('#delete_lecture').click(function () {

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-success',
            buttonsStyling: false
        }).then(function () {

            var lecture_id = $("#lecture_id").val();
            var course_id = $("#course_id").val();

            var params = {
                lecture_id: lecture_id
            };

            $.ajax({
                url: '/delete/lecture/',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    showSuccessNotification('Lecture has been deleted');

                    setTimeout(function(){
                        window.location.href = "/course/" + course_id;
                    },2000);
                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });

        });

    });


    $('#text_content_button').click(function () {

        var content_title = $('#content_title').val();
        var content = $('#content').val();
        var mime_type = 'text/plain';
        var lecture_id = $('#lecture_id').val();
        var course_id = $('#course_id').val();

        var params = {
            content_title: content_title,
            content: content,
            mime_type: mime_type,
            lecture_id: lecture_id,
            course_id: course_id
        };

        $.ajax({
            url: '/add/text/content',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), params: params},
            success: function (response) {

                $('#content_title').val('');
                $('#content').val('');
                swal("Done!", "Text Content Has Been Added" , "success");


                var htmlstr = '<div class="card-box">' +
                    '<div class="row">' +
                    '<div class="col-sm-12">' +
                    '<h2><b>' + content_title +'</b></h2>' +
                    '<h4 class="m-t-30">'+ content +'</h4>' +
                    '</div></div></div>';

                $('#lecture_content_div').append(htmlstr);
            },
            error: function (error) {
                swal("Error!", "Something Went Wrong" , "error");
            }

        });

    });


    $('#onlineVideo_content_button').click(function () {

        var content_title = $('#onlineVideo_content_title').val();
        var content = $('#onlineVideo_content').val();
        var link_array = content.split("/");
        var mime_type = link_array[2];
        var lecture_id = $('#lecture_id').val();
        var course_id = $('#course_id').val();
        var link = '';

        if (mime_type == 'www.youtube.com' || mime_type == 'youtu.be') mime_type = 'youtube.com';

        if (mime_type == 'youtu.be' || mime_type == 'vimeo.com') link = link_array[link_array.length-1];
        else if (mime_type == 'youtube.com') {
            var temp_array = link_array[link_array.length-1].split("=");
            link = temp_array[temp_array.length-1];
        }


        var params = {
            content_title: content_title,
            content: content,
            mime_type: mime_type,
            lecture_id: lecture_id,
            course_id: course_id
        };

        if (content != '') {

            $.ajax({
                url: '/add/text/content',
                type: 'POST',
                format: 'JSON',
                data: {'_token': $('#token').val(), params: params},
                success: function (response) {

                    $('#onlineVideo_content_title').val('');
                    $('#onlineVideo_content').val('');

                    swal("Done!", "Online Video Content Has Been Added" , "success");

                    var htmlstr = '';

                    if(mime_type == 'vimeo.com') {
                        htmlstr = '<div class="card-box">' +
                            '<h2><b>'+ content_title +'</b></h2>' +
                            '<div class="embed-responsive embed-responsive-16by9 m-t-30">' +
                            '<iframe class="embed-responsive-item" src="http://player.vimeo.com/video/'+ link +'?title=0&amp;byline=0&amp;"></iframe>' +
                            '</div></div>';
                    }
                    else if (mime_type == 'youtube.com') {
                        htmlstr = '<div class="card-box">' +
                            '<h2><b>'+ content_title +'</b></h2>' +
                            '<div class="embed-responsive embed-responsive-16by9 m-t-30">' +
                            '<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'+ link +'" frameborder="0" allow="autoplay; encrypted-media"></iframe>' +
                            '</div></div>';
                    }

                    $('#lecture_content_div').append(htmlstr);
                },
                error: function (error) {
                    swal("Error!", "Something Went Wrong" , "error");
                }

            });

        }
        else {
            swal("Error!", "Video Link Cannot Be Null" , "error");
        }

    });


    $('#edit_text_content_button').click(function () {

        var content_id = $('#edit_content_id').val();
        var content_title = $('#edit_content_title').val();
        var content = $('#edit_content').val();

        editContent(content_id, content_title, content);
    });


    $('#edit_onlineVideo_content_button').click(function () {

        var content_id = $('#onlineVideo_content_id').val();
        var content_title = $('#edit_onlineVideo_content_title').val();
        var content = $('#edit_onlineVideo_content').val();

        editContent(content_id, content_title, content);
    });


    $("#join_course_button").click(function(){

        var course_id = $("#course_id").val();
        var lecture_id = $("#lecture_id").val();
        var free_course = $("#free_course").val();

        var params = {
            course_id: course_id,
            free_course: free_course
        };

        $.ajax({
            url: '/enrollment/course',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                showSuccessNotification('You have requested for this course');

                if (free_course == 1) {

                    setTimeout(function(){
                        window.location.href = "/lecture/" + lecture_id;
                    },1000);

                }
                else {
                    $('#course_enrollment_div').empty();

                    var htmlstr = '<button type="button" class="btn btn-warning waves-effect waves-light m-l-10">' +
                        '<span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>' +
                        'You have requested for this course</button>';

                    $('#course_enrollment_div').append(htmlstr);
                }

            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });


});



function start_quiz(quiz_id) {
    $('#quiz_id').val(quiz_id);
    $('#start_quiz_form').submit();
}


function edit_quiz(quiz_id) {
    $('#edit_quiz_id').val(quiz_id);
    $('#edit_quiz_form').submit();
}


function flashcard_view(flashcard_title, flashcard_description, media) {

    $("#fc_title").text(flashcard_title);
    $("#fc_description").text(flashcard_description);
    $('#fc_image').attr('src','/media/'+ media);

    $('#flash_card_modal').modal('show');

}


function edit_content(content_id, mime_type, content_title, content) {

    if (mime_type == 'text/plain') {

        $('#edit_content_id').val(content_id);
        $('#edit_content_title').val(content_title);
        $('#edit_content').val(content);

        $('#edit_text_content_modal').modal('show');

    }

    else if (mime_type == 'vimeo.com' || mime_type == 'youtube.com') {

        $('#onlineVideo_content_id').val(content_id);
        $('#edit_onlineVideo_content_title').val(content_title);
        $('#edit_onlineVideo_content').val(content);

        $('#edit_onlineVideo_content_modal').modal('show');

    }


    else {

        $('#media_content_id').val(content_id);
        $('#edit_media_content_title').val(content_title);

        $('#edit_media_content_modal').modal('show');
    }


}


function delete_content(content_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            content_id: content_id
        };

        $.ajax({
            url: '/delete/content',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $( '#lecture_content_' + content_id ).remove();
                showSuccessNotification('This Lecture Content Has Been Deleted');
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });

}


function editContent(content_id, content_title, content) {

    var params = {
        content_id: content_id,
        content_title: content_title,
        content: content
    };

    $.ajax({
        url: '/edit/content',
        type: 'POST',
        format: 'JSON',
        data: {params: params, '_token': $('#token').val()},
        success: function (response) {
            reloadCurrentPage();
        },
        error: function(error) {
            swal('Error!', 'Something went wrong', 'error');
        }
    });

}



