<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExamScripts extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('exam_scripts', function (Blueprint $table) {

            $table->increments('script_id');

            $table->integer('result_id')->unsigned();
            $table->foreign('result_id')->references('result_id')->on('exam_results')->onDelete('cascade');

            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('question_id')->on('exam_question')->onDelete('cascade');

            $table->longText('submitted_answer')->nullable();

        });

    }


    public function down() {
        Schema::dropIfExists('exam_scripts');
    }
}
