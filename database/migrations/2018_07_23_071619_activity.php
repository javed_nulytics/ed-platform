<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Activity extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('activity', function (Blueprint $table) {
            $table->increments('activity_id');
            $table->integer('user_id')->references('id')->on('users');
            $table->integer('lookup_activity')->references('lookup_id')->on('lookup'); //created, updated, deleted
            $table->integer('lookup_category')->references('lookup_id')->on('lookup'); // course, section, lecture, student, tutor
            $table->integer('lookup_name_id')->references('lookup_id')->on('lookup'); //1, 5
            $table->string('lookup_name_child')->references('lookup_id')->on('lookup')->nullable(); //Java, Introduction
            $table->timestamp('created_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('activity');
    }
}
