<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuizResults extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('quiz_results', function (Blueprint $table) {

            $table->increments('result_id')->unsigned();

            $table->integer('quiz_id')->unsigned();
            $table->foreign('quiz_id')->references('quiz_id')->on('quiz')->onDelete('cascade');

            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('answered_question');
            $table->integer('total_question');
            $table->double('score');
            $table->double('percentile')->default(0.0);
            $table->timestamp('created_at')->useCurrent();

        });

    }

    public function down() {
        Schema::dropIfExists('quiz_results');
    }
}
