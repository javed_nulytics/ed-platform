<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Exams extends Migration {

    public function up() {

        Schema::create('exams', function (Blueprint $table) {
            $table->increments('exam_id')->unsigned();

            $table->string('exam_title')->nullable();
            $table->longText('exam_description')->nullable();

            $table->tinyInteger('scheduled')->default(0);
            $table->timestamp('schedule_start')->nullable();
            $table->integer('duration')->default(0);

            $table->integer('tutor_id')->unsigned();
            $table->foreign('tutor_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('course_id')->unsigned();
            $table->foreign('course_id')->references('course_id')->on('courses')->onDelete('cascade');

            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('published')->default(1);
            $table->integer('order')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('exams');
    }
}
