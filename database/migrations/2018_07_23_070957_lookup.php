<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Lookup extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('lookup', function (Blueprint $table) {
            $table->increments('lookup_id');
            $table->string('lookup_name');

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('lookup');
    }
}
