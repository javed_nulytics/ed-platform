<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration{

    public function up() {
        Schema::defaultStringLength(191);

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('role')->default("student");
            $table->longText('user_description')->nullable();
            $table->string('phone')->nullable();
            $table->string('user_img')->default("default_user.png");
            $table->double('avg_quiz_score')->default(0);
            $table->double('avg_exam_score')->default(0);
            $table->integer('status')->default(1);
            $table->rememberToken();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    public function down() {
        Schema::dropIfExists('users');
    }
}
