<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Lectures extends Migration {

    public function up() {

        Schema::create('lectures', function (Blueprint $table) {

            $table->increments('lecture_id');
            $table->string('lecture_title');
            $table->longText('lecture_description')->nullable();
            $table->string('media')->nullable();

            $table->integer('tutor_id')->unsigned();
            $table->foreign('tutor_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('section_id')->unsigned();
            $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');

            $table->integer('course_id')->unsigned();
            $table->foreign('course_id')->references('course_id')->on('courses')->onDelete('cascade');

            $table->tinyInteger('status')->default(1);
            $table->integer('order')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();

        });

    }


    public function down() {
        Schema::dropIfExists('lectures');
    }
}
