<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LectureContents extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('lecture_contents', function (Blueprint $table) {

            $table->increments('content_id');
            $table->string('content_title')->nullable();
            $table->longText('content');
            $table->string('mime_type');

            $table->integer('tutor_id')->unsigned();
            $table->foreign('tutor_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('lecture_id')->unsigned();
            $table->foreign('lecture_id')->references('lecture_id')->on('lectures')->onDelete('cascade');

            $table->integer('course_id')->unsigned();
            $table->foreign('course_id')->references('course_id')->on('courses')->onDelete('cascade');

            $table->tinyInteger('status')->default(1);
            $table->integer('order')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();

        });

    }

    public function down() {
        Schema::dropIfExists('lecture_contents');

    }
}
