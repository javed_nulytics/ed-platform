<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Courses extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('courses', function (Blueprint $table) {
            $table->increments('course_id');
            $table->string('course_name');
            $table->text('course_title')->nullable();

            $table->longText('course_overview')->nullable();
            $table->longText('what_will_i_learn')->nullable();
            $table->longText('course_requirement')->nullable();
            $table->integer('instructor')->default(1);

            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('category_id')->on('course_category')->onDelete('cascade');

            $table->integer('price')->default(0);
            $table->integer('discount')->default(0);
            $table->tinyInteger('free_course')->nullable();
            $table->string('course_level')->nullable();
            $table->string('course_image')->default('default.jpg');
            $table->tinyInteger('status')->default(1);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('courses');
    }
}
