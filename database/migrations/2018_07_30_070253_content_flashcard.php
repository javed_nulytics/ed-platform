<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContentFlashcard extends Migration {

    public function up() {

        Schema::create('content_flashcard', function (Blueprint $table) {
            $table->increments('flashcard_id');

            $table->integer('lecture_id')->unsigned();
            $table->foreign('lecture_id')->references('lecture_id')->on('lectures')->onDelete('cascade');

            $table->string('flashcard_title');
            $table->longText('flashcard_description');
            $table->string('media');

            $table->integer('tutor_id')->unsigned();
            $table->foreign('tutor_id')->references('id')->on('users')->onDelete('cascade');

            $table->tinyInteger('status')->default(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('content_flashcard');
    }
}
