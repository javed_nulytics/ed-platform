<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StatusLookup extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('status_lookup', function (Blueprint $table) {
            $table->increments('status_id');
            $table->string('status_name');

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('status_lookup');
    }
}
