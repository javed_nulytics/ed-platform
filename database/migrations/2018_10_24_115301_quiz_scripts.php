<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuizScripts extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('quiz_scripts', function (Blueprint $table) {

            $table->increments('script_id');

            $table->integer('result_id')->unsigned();
            $table->foreign('result_id')->references('result_id')->on('quiz_results')->onDelete('cascade');

            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('question_id')->on('quiz_question')->onDelete('cascade');

            $table->longText('submitted_answer')->nullable();

        });

    }


    public function down() {
        Schema::dropIfExists('quiz_scripts');
    }
}
