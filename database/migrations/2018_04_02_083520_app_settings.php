<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AppSettings extends Migration {

    public function up() {
        Schema::defaultStringLength(191);

        Schema::create('app_settings', function (Blueprint $table) {
            $table->increments('setting_id');
            $table->string('setting_key');
            $table->string('setting_value')->nullable();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    public function down() {
        Schema::dropIfExists('app_settings');

    }
}
