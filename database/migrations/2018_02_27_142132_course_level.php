<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseLevel extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('course_level', function (Blueprint $table) {
            $table->increments('course_level_id');
            $table->string('course_level_name');
            $table->integer('created_by')->default(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('course_level');
    }
}
