<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseFaq extends Migration {

    public function up() {

      Schema::create('course_faq', function (Blueprint $table) {
          $table->increments('faq_id');

          $table->integer('course_id')->unsigned();
          $table->foreign('course_id')->references('course_id')->on('courses')->onDelete('cascade');

          $table->longText('faq_question');
          $table->longText('faq_answer');

          $table->tinyInteger('status')->default(1);

          $table->integer('created_by')->unsigned();
          $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');

          $table->timestamp('created_at')->useCurrent();
          $table->timestamp('updated_at')->useCurrent();
      });

    }


    public function down() {
      Schema::dropIfExists('course_faq');
    }
}
