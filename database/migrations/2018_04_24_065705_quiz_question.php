<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuizQuestion extends Migration {

    public function up() {

        Schema::create('quiz_question', function (Blueprint $table) {
            $table->increments('question_id');

            $table->integer('quiz_id')->unsigned();
            $table->foreign('quiz_id')->references('quiz_id')->on('quiz')->onDelete('cascade');

            $table->string('question_type');

            $table->longText('question');
            $table->longText('option_1')->nullable();
            $table->longText('option_2')->nullable();
            $table->longText('option_3')->nullable();
            $table->longText('option_4')->nullable();
            $table->longText('answer');
            $table->string('question_media')->nullable();
            $table->string('explanation')->nullable();
            $table->string('explanation_media')->nullable();

            $table->tinyInteger('status')->default(1);
            $table->integer('order')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();

        });
    }

    public function down() {
        Schema::dropIfExists('quiz_question');
    }
}
