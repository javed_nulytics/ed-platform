<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@welcome')->name('home');
Route::get('/about', 'HomeController@about');










/*
* Course Routes for Guest
*/

Route::get('/courses/category/{id}', 'CourseController@category');
Route::get('/courses/{id}', 'CourseController@search');
Route::get('/course/{id}', 'CourseController@course');










/*
* FAQ Routes for Guest
*/

Route::get('/course/faq/{id}', 'FaqController@index');





Auth::routes();





/*
 * test routes
 */

Route::get('/test', 'HomeController@test');










/*
 * Course Routes for Auth
 */

Route::get('create-course', 'CourseController@createCourseLayout')->middleware('tutor');
Route::post('create-course', 'CourseController@createCourse')->middleware('tutor');
Route::post('/delete/course', 'CourseController@deleteCourse')->middleware('tutor');
Route::post('edit/course', 'CourseController@editCourseLayout')->middleware('tutor');
Route::post('edited/course', 'CourseController@editCourse')->middleware('tutor');
Route::post('/course/student/status', 'CourseController@courseStudentStatus')->middleware('tutor');
Route::post('/add/category', 'CourseController@addCategory')->middleware('tutor');
Route::post('/add/level', 'CourseController@addLevel')->middleware('admin');
Route::get('/view/courses', 'CourseController@viewCourses')->middleware('tutor');
Route::get('/enrolled-courses', 'CourseController@enrolledCourses')->middleware('auth');
Route::get('/view/course-request', 'CourseController@viewCourseRequest')->middleware('tutor');
Route::get('/view/course-request/{id}', 'CourseController@viewCourseRequest')->middleware('tutor');

Route::post('/delete/category', 'CourseController@deleteCategory')->middleware('tutor');









/*
 * Course Enrollment Routes for Auth
 */

Route::post('/enrollment/course', 'CourseController@enrollmentCourse')->middleware('auth');
Route::post('/delete/enrollment', 'CourseController@deleteEnrollment')->middleware('auth');










/*
 * Section Routes for Auth
 */

Route::post('/add/section', 'CourseController@addSection')->middleware('tutor');
Route::post('/edit/section', 'CourseController@editSection')->middleware('tutor');
Route::post('/delete/section', 'CourseController@deleteSection')->middleware('tutor');










/*
 * Lecture Routes for Auth
 */

Route::post('/add/lecture', 'LectureController@addLecture')->middleware('tutor');
Route::get('/lecture/{id}', 'LectureController@index')->middleware('auth');
Route::post('/edit/lecture', 'LectureController@editLecture')->middleware('tutor');
Route::post('/delete/lecture/', 'LectureController@deleteLecture')->middleware('tutor');
Route::post('/add/text/content', 'LectureController@addTextContent')->middleware('tutor');
Route::post('/add/media/content', 'LectureController@addMediaContent')->middleware('tutor');
Route::post('/edit/media/content', 'LectureController@editMediaContent')->middleware('tutor');
Route::post('/add/flashcard/content', 'LectureController@addFlashcardContent')->middleware('tutor');
Route::post('/delete/content', 'LectureController@deleteContent')->middleware('tutor');
Route::post('/edit/content', 'LectureController@editContent')->middleware('tutor');










/*
 * Quiz Routes for Auth
 */

Route::get('add/quiz/{id}', 'QuizController@addQuizLayout')->middleware('tutor');
Route::post('/add/quiz/', 'QuizController@storeQuiz')->middleware('tutor');
Route::post('quiz', 'QuizController@quiz')->middleware('auth');
Route::post('/store/result', 'QuizController@storeResult')->middleware('auth');
//Route::post('view/quiz/result/', 'QuizController@displayQuizResult');
Route::get('/quiz/result/{id}', 'QuizController@displayQuizResult')->middleware('auth');
Route::get('view/quizzes', 'QuizController@viewQuizLayout')->middleware('tutor');
Route::post('/delete/quiz', 'QuizController@deleteQuiz')->middleware('tutor');
Route::post('/edit/quiz', 'QuizController@editQuiz')->middleware('tutor');
Route::post('/update/quiz/', 'QuizController@updateQuiz')->middleware('tutor');
Route::post('/delete/quiz-question', 'QuizController@deleteQuizQuestion')->middleware('tutor');










/*
 * Dashboard Routes
 */

Route::get('dashboard', 'ProfileController@dashboard');
Route::post('/clear/course/request', 'CourseController@clearCourseRequest')->middleware('admin');










/*
 * User Settings Routes for Auth
 */

Route::get('/users', 'UserController@viewUsers')->middleware('admin');
Route::post('/add/user', 'UserController@addUser')->middleware('admin');
Route::post('/delete/user', 'UserController@deleteUser')->middleware('admin');
Route::post('/edit/user/role', 'UserController@editUserRole')->middleware('admin');
Route::get('/generate/user', 'UserController@generateUser')->middleware('admin');
Route::post('/edit/user', 'UserController@editUser')->middleware('admin');
Route::post('/change/password', 'UserController@changePassword')->middleware('admin');










/*
 * Settings Routes for Auth
 */

Route::get('/settings', 'SettingsController@index')->middleware('admin');
Route::post('/add/lookup/', 'SettingsController@addLookup')->middleware('admin');
Route::post('/add/course/level', 'SettingsController@addCourseLevel')->middleware('admin');
Route::post('/add/role', 'SettingsController@addRole')->middleware('admin');
Route::post('/save/app-settings/', 'SettingsController@saveAppSettings')->middleware('admin');










/*
 * Profile Routes for Auth
 */

Route::get('/profile', 'ProfileController@index')->middleware('auth');
Route::get('/profile/{id}', 'ProfileController@index')->middleware('auth');
Route::post('/edit/profile', 'ProfileController@editProfile');
Route::post('/edit/password', 'ProfileController@editPassword');










/*
 * FAQ Routes for Auth
 */

Route::post('/add/faq', 'FaqController@addFAQ')->middleware('tutor');
Route::post('/edit/faq', 'FaqController@editFAQ')->middleware('tutor');
Route::post('/delete/faq', 'FaqController@deleteFAQ')->middleware('tutor');










/*
 * Todo List Routes for Auth
 */

Route::post('/add/todo', 'TodoController@addTodo')->middleware('auth');
Route::post('/change/todo/status', 'TodoController@changeTodoStatus')->middleware('auth');










/*
 * Exam Routes for Auth
 */

Route::get('/view/exams', 'ExamController@viewExam')->middleware('auth');
Route::get('/create/exam', 'ExamController@createExamLayout')->middleware('tutor');
Route::post('/create/exam', 'ExamController@createExam')->middleware('tutor');
Route::get('/exam/course/{id}', 'ExamController@viewCourseExam')->middleware('auth');
Route::post('/exam', 'ExamController@exam')->middleware('auth');
Route::post('/store/exam/result', 'ExamController@storeResult')->middleware('auth');
Route::get('/exam/result/{id}', 'ExamController@viewExamResult')->middleware('auth');
Route::get('/exam/history', 'ExamController@examHistory')->middleware('auth');
Route::post('/delete/exam', 'ExamController@deleteExam')->middleware('tutor');









/*
 * Analytics Routes for Auth
 */

Route::get('/student-analytics', 'AnalyticsController@index')->middleware('tutor');
Route::get('/student-analytics/{id}', 'AnalyticsController@viewStudentAnalytics')->middleware('tutor');









/*
* SuperAdmin Settings Routes
*/

Route::get('/create-user', 'SuperAdminController@createUserLayout')->middleware('admin');
Route::get('/app-settings', 'SuperAdminController@appSettingsLayout')->middleware('admin');
Route::get('/data-settings', 'SuperAdminController@dataSettingsLayout')->middleware('admin');
Route::get('/schedule-scripts', 'SuperAdminController@scheduleScriptsLayout')->middleware('admin');
Route::post('/table/truncate', 'SuperAdminController@truncateTable')->middleware('admin');
Route::post('/table/drop', 'SuperAdminController@dropTable')->middleware('admin');










/*
* SuperAdmin Scripts Routes
*/

Route::post('/run-scripts', 'ScriptController@runScripts')->middleware('admin');
Route::post('/db-fix-scripts', 'ScriptController@dbFixScript')->middleware('admin');
Route::post('/generate-data', 'ScriptController@generateDemoData')->middleware('admin');
