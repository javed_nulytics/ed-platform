<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TodoController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function addTodo(Request $request) {

        $data = $request->input('params');
        $user_id = Auth::user()->id;

        $todo_id = DB::table('todo_list')->insertGetId(
            array(
                'user_id' => $user_id,
                'todo_name' => $data['todo_name']
            )
        );

        return json_encode($todo_id);

    }


    public function changeTodoStatus(Request $request) {

      $data = $request->input('params');

      DB::table('todo_list')
        ->where('todo_id', $data['todo_id'])
        ->update([
          'checked' => $data['checked']
        ]);

        return json_encode('success');

    }

}
