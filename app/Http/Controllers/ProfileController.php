<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index($id=0) {

        if ($id == 0) $id = Auth::user()->id;

        $user = DB::table('users')
            ->select('id', 'name', 'email', 'role', 'user_description', 'phone', 'user_img', 'status', 'created_at')
            ->where('id', $id)
            ->first();


        $courses = DB::table('courses')
            ->select('courses.*', 'users.id', 'users.name', 'users.user_img', 'course_category.category_name'
                , DB::raw("(SELECT COUNT(course_student_id) FROM course_student WHERE course_student.course_id=courses.course_id) as student_count")
            )
            ->join('users', 'users.id', '=', 'courses.instructor')
            ->join('course_category', 'course_category.category_id', '=', 'courses.category_id')
            ->where('instructor', $id)
            ->get();


        return view('scholars.profile.index', [
            'user' => $user,
            'courses' => $courses,
        ]);
    }


    public function dashboard() {

        $role = Auth::user()->role;
        $user_id = Auth::user()->id;

        if ($role == 'superadmin' || $role == 'admin') {

            set_lookup();

            $year_0 = date("Y");
            $year_1 = $year_0 - 1 ;
            $year_2 = $year_0 - 2;
            $year_3 = $year_0 - 3;
            $year_4 = $year_0 - 4;

            $analytics = DB::table('users')
                ->select(
                    DB::raw("(SELECT COUNT(*) FROM users WHERE role='student' AND YEAR(created_at)=$year_0) as st_1"),
                    DB::raw("(SELECT COUNT(*) FROM users WHERE role='tutor' AND YEAR(created_at)=$year_0) as tu_1"),
                    DB::raw("(SELECT COUNT(*) FROM courses WHERE YEAR(created_at)=$year_0) as co_1"),

                    DB::raw("(SELECT COUNT(*) FROM users WHERE role='student' AND YEAR(created_at)=$year_1) as st_2"),
                    DB::raw("(SELECT COUNT(*) FROM users WHERE role='tutor' AND YEAR(created_at)=$year_1) as tu_2"),
                    DB::raw("(SELECT COUNT(*) FROM courses WHERE YEAR(created_at)=$year_1) as co_2"),

                    DB::raw("(SELECT COUNT(*) FROM users WHERE role='student' AND YEAR(created_at)=$year_2) as st_3"),
                    DB::raw("(SELECT COUNT(*) FROM users WHERE role='tutor' AND YEAR(created_at)=$year_2) as tu_3"),
                    DB::raw("(SELECT COUNT(*) FROM courses WHERE YEAR(created_at)=$year_2) as co_3"),

                    DB::raw("(SELECT COUNT(*) FROM users WHERE role='student' AND YEAR(created_at)=$year_3) as st_4"),
                    DB::raw("(SELECT COUNT(*) FROM users WHERE role='tutor' AND YEAR(created_at)=$year_3) as tu_4"),
                    DB::raw("(SELECT COUNT(*) FROM courses WHERE YEAR(created_at)=$year_3) as co_4"),

                    DB::raw("(SELECT COUNT(*) FROM users WHERE role='student' AND YEAR(created_at)=$year_4) as st_5"),
                    DB::raw("(SELECT COUNT(*) FROM users WHERE role='tutor' AND YEAR(created_at)=$year_4) as tu_5"),
                    DB::raw("(SELECT COUNT(*) FROM courses WHERE YEAR(created_at)=$year_4) as co_5")

                )
                ->first();


            $data = DB::table('users')
                ->select(
                    DB::raw("(SELECT COUNT(course_id)FROM courses) as total_courses")
                    , DB::raw("(SELECT COUNT(id) FROM users WHERE role='student') as total_student")
                    , DB::raw("(SELECT COUNT(id) FROM users WHERE role='tutor') as total_tutor")

                )
                ->first();


            $course_requests = DB::table('course_student')
                ->select('course_student.*', 'users.name', 'courses.course_name', 'status_lookup.status_name')
                ->join('users', 'users.id', '=', 'course_student.student_id')
                ->join('courses', 'courses.course_id', '=', 'course_student.course_id')
                ->join('status_lookup', 'status_lookup.status_id', '=', 'course_student.status')
                ->orderBy('course_student.created_at', 'desc')
                ->paginate(5);




            $courses = DB::table('courses')
                ->select('courses.*', 'users.id', 'users.name', 'course_category.category_name', 'course_level.course_level_name', 'status_lookup.status_name'
                    , DB::raw("(SELECT COUNT(course_student_id) FROM `course_student` WHERE course_id= courses.course_id limit 1) as no_student")
                )
                ->join('users', 'users.id', '=', 'courses.instructor')
                ->join('course_category', 'course_category.category_id', '=', 'courses.category_id')
                ->join('course_level', 'course_level.course_level_id', '=', 'courses.course_level')
                ->join('status_lookup', 'status_lookup.status_id', '=', 'courses.status')
                ->orderBy('no_student', 'desc')
                ->limit(5)
                ->get();


            $course_category = DB::table('course_category')
                ->select('course_category.*', 'users.name'
                    , DB::raw("(SELECT COUNT(course_id) FROM courses WHERE category_id=course_category.category_id) as category_count")
                )
                ->join('users', 'users.id', '=', 'course_category.created_by')
                ->get();


            $course_level = DB::table('course_level')
                ->select('course_level.*', 'users.name')
                ->join('users', 'users.id', '=', 'course_level.created_by')
                ->get();


            $activities = DB::table('activity')
                ->select('activity.*', 'users.id', 'users.name'
                    , DB::raw("(SELECT lookup_name FROM lookup WHERE activity.lookup_activity=lookup.lookup_id) as lookup_activity")
                    , DB::raw("(SELECT lookup_name FROM lookup WHERE activity.lookup_category=lookup.lookup_id) as lookup_category")
                )
                ->join('users', 'users.id', '=', 'activity.user_id')
                ->orderBy('created_at', 'desc')
                //->orderBy('created_at', 'asc')
                ->limit(10)
                ->get();


            $revenue = calculateRevenue();


            $revenues = DB::table('courses')
                ->where('courses.free_course', 0)
                ->where('course_student.status', 5)
                ->select('courses.*', 'users.id', 'users.name'
                    , DB::raw("(SELECT COUNT(course_id) FROM course_student WHERE course_id=courses.course_id) as course_count")
                )
                ->join('users', 'users.id', '=', 'courses.instructor')
                ->join('course_student', 'course_student.course_id', '=', 'courses.course_id')
                ->limit(10)
                ->get();



            $todo_list = DB::table('todo_list')
                ->where('user_id', $user_id)
                ->orderBy('checked', 'asc')
                ->get();


            return view('scholars.dashboard.admin-dashboard', [
                'courses' => $courses,
                'data' => $data,
                'course_requests' => $course_requests,
                'course_category' => $course_category,
                'course_level' => $course_level,
                'analytics' => $analytics,
                'activities' => $activities,
                'revenue' => $revenue,
                'todo_list' => $todo_list,
                'revenues' => $revenues,
            ]);
        }


        else if ($role == 'tutor') {

            $courses = DB::table('courses')
                ->select('courses.*', 'users.id', 'users.name', 'course_category.category_name', 'course_level.course_level_name', 'status_lookup.status_name'
                    , DB::raw("(SELECT COUNT(course_student_id) FROM `course_student` WHERE course_id= courses.course_id limit 1) as no_student")
                )
                ->join('users', 'users.id', '=', 'courses.instructor')
                ->join('course_category', 'course_category.category_id', '=', 'courses.category_id')
                ->join('course_level', 'course_level.course_level_id', '=', 'courses.course_level')
                ->join('status_lookup', 'status_lookup.status_id', '=', 'courses.status')
                ->where('instructor', $user_id)
                ->get();

            $total_students = 0;
            foreach ($courses as $course) $total_students += intval($course->no_student);

            $course_requests = DB::table('course_student')
                ->where('courses.instructor', $user_id)
                ->select('course_student.*', 'users.name', 'courses.course_name', 'status_lookup.status_name')
                ->join('users', 'users.id', '=', 'course_student.student_id')
                ->join('courses', 'courses.course_id', '=', 'course_student.course_id')
                ->join('status_lookup', 'status_lookup.status_id', '=', 'course_student.status')
                ->orderBy('course_student.created_at', 'desc')
                ->paginate(5);

            $course_category = DB::table('course_category')
                ->select('course_category.*', 'users.name'
                    , DB::raw("(SELECT COUNT(course_id) FROM courses WHERE category_id=course_category.category_id) as category_count")
                )
                ->join('users', 'users.id', '=', 'course_category.created_by')
                ->get();



            return view('scholars.dashboard.tutor-dashboard', [
                'courses' => $courses,
                'total_students' => $total_students,
                'course_requests' => $course_requests,
                'course_category' => $course_category,

            ]);
        }


        else if ($role == 'student') {

            $year = date("Y");

            $progress_analytics = DB::select("SELECT MONTH(created_at) MONTH, COUNT(*) COUNT, AVG(score) SCORE FROM quiz_results WHERE YEAR(created_at)=$year AND student_id=$user_id GROUP BY MONTH(created_at)");


            $exam_analytics = DB::select("SELECT MONTH(created_at) MONTH, COUNT(*) COUNT, AVG(score) SCORE FROM exam_results WHERE YEAR(created_at)=$year AND student_id=$user_id GROUP BY MONTH(created_at)");


            $student_data = DB::table('quiz_results')
                ->select(
                    DB::raw("(SELECT AVG(score) FROM quiz_results WHERE student_id=$user_id) as avg_quiz_score"),
                    DB::raw("(SELECT AVG(score) FROM exam_results WHERE student_id=$user_id) as avg_exam_score")
                )
                ->first();

            $avg_quiz_score = $student_data->avg_quiz_score==null? '0':$student_data->avg_quiz_score;
            $avg_exam_score = $student_data->avg_exam_score==null? '0':$student_data->avg_exam_score;


            DB::table('users')
                ->where('id', $user_id)
                ->update([
                    'avg_quiz_score' => $avg_quiz_score,
                    'avg_exam_score' => $avg_exam_score,
                ]);

            $todo_list = DB::table('todo_list')
                ->where('user_id', $user_id)
                ->orderBy('checked', 'asc')
                ->get();

            $courses = DB::table('course_student')
                ->where('student_id', $user_id)
                ->select('course_student.course_student_id', 'course_student.created_at as started_date', 'courses.*')
                ->join('courses', 'courses.course_id', '=', 'course_student.course_id')
                ->get();


            $quiz_results = DB::table('quiz_results')
                ->where('student_id', $user_id)
                ->select('quiz_results.*', 'quiz.quiz_title', 'courses.course_id', 'courses.course_name', 'lectures.lecture_id', 'lectures.lecture_title')
                ->join('quiz', 'quiz.quiz_id', '=', 'quiz_results.quiz_id')
                ->join('courses', 'quiz.course_id', '=', 'courses.course_id')
                ->join('lectures', 'quiz.lecture_id', '=', 'lectures.lecture_id')
                ->orderBy('quiz_results.created_at', 'desc')
                ->take(5)
                ->get();


            $exam_results = DB::table('exam_results')
                ->where('student_id', $user_id)
                ->select('exam_results.*', 'exams.exam_title', 'courses.course_id', 'courses.course_name')
                ->join('exams', 'exams.exam_id', '=', 'exam_results.exam_id')
                ->join('courses', 'exams.course_id', '=', 'courses.course_id')
                ->orderBy('exam_results.created_at', 'desc')
                ->take(5)
                ->get();


            $user = DB::table('users')->where('id', $user_id)->first();





            return view('scholars.dashboard.student-dashboard', [
                'courses' => $courses,
                'todo_list' => $todo_list,
                'quiz_results' => $quiz_results,
                'progress_analytics' => $progress_analytics,
                'student_data' => $student_data,
                'exam_results' => $exam_results,
                'exam_analytics' => $exam_analytics,
                'user' => $user,
            ]);
        }


    }


    public function editProfile(Request $request) {

        $file = $request->file('image');
        $user_id = Auth::user()->id;

        $name = Input::get('name');
        $user_description = Input::get('user_description');
        $phone = Input::get('phone');


        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
            ]);

            $file_name = time() . '.' .$file->getClientOriginalExtension();
            $destinationPath = 'images/users';
            $file->move($destinationPath,$file_name);

            DB::table('users')
                ->where('id', $user_id)
                ->update([
                    'user_img' => $file_name
                ]);
        }


        DB::table('users')
            ->where('id', $user_id)
            ->update([
                'name' => $name,
                'user_description' => $user_description,
                'phone' => $phone,
            ]);

        return redirect('/profile');

    }


    public function editPassword(Request $request) {

        $data = $request->input('params');
        $user_id = Auth::user()->id;

        $old_password = $data['old_password'];
        $new_password = $data['new_password'];

//        $this->validate($request, [
//            'old' => 'required',
//            'password' => 'required|min:6|confirmed',
//        ]);

        $user = DB::table('users')->where('id', $user_id)->first();
        $hashedPassword = $user->password;

        if (Hash::check($old_password, $hashedPassword)) {

            changePassword($user_id, $new_password);

            return json_encode('success');
        }

        else return json_encode('failure');


    }




}
