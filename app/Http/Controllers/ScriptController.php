<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Schema;

class ScriptController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function runScripts(Request $request) {

        checkCourseCompletedStatus();

        DB::table('schedule_scripts')->insert([
            'script_name' => 'check:checkCourseCompletedStatus'
        ]);

        return json_encode('success');
    }


    public function dbFixScript(Request $request) {


        return json_encode('success');
    }


    public function generateDemoData() {

        $lines = preg_split('/\r\n|\n|\r/', trim(file_get_contents('demo-data.txt')));

        $query_str = '';
        $flag = false;

        foreach ($lines as $line) {
            if ($line == '') {
                $flag = false;
                $results = DB::select(DB::raw("$query_str"));
                $query_str = '';
            }
            else {
                $flag = true;
                $query_str .= ' ' . $line;
            }
        }

        if ($flag) $results = DB::select(DB::raw("$query_str"));
        return json_encode('success');
    }

}
