<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;


class SuperAdminController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function createUser(Request $request) {
        $creator_id = Auth::user()->id;
        $data= $request->input('params');
        $date = date('Y-m-d H:i:s');

        $rules = array(
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        );

        $validator = Validator::make($data, $rules);
        if ($validator->passes()) {

            $restaurant = DB::table('restaurants')
                ->select('inventory', 'license')
                ->where('restaurants_id', $data['restaurant_id'])
                ->first();

            $user_id = DB::table('users')->insertGetId(
                array(
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'role' => $data['role'],
                    'restaurant_id' => $data['restaurant_id'],
                    'creator_id' => $creator_id,
                    'inventory' => $restaurant->inventory,
                    'license' => $restaurant->license,
                    'created_at' => $date,
                    'updated_at' => $date,
                )
            );
            echo json_encode($user_id);
        }
        else {
            echo json_encode(array('errors' => $validator->getMessageBag()->toArray()));
        }
    }


    public function createUserLayout() {

        $role_lookup = DB::table('role_lookup')->get();

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*', 'users.name', 'users.user_img')
            ->join('users', 'users.id', '=', 'restaurants.creator_id')
            ->where('restaurants.restaurants_id', '>', 0)
            ->get();


        $users = DB::table('users')
            ->select('users.*', 'restaurants.restaurants_name')
            ->join('restaurants', 'restaurants.restaurants_id', '=', 'users.restaurant_id')
            ->get();

        return view('cupcake.sa-settings.create-user', [
            'role_lookup' => $role_lookup,
            'restaurant_data' => $restaurant_data,
            'users' => $users,


        ]);
    }


    public function dataSettingsLayout() {

        $tables = DB::select('SHOW TABLES');
        $db_name = 'Tables_in_' . DB::connection()->getDatabaseName();

        $users = DB::table('users')->get();



        return view('scholars.sa-settings.data-settings', [
            'tables' => $tables,
            'db_name' => $db_name,
            'users' => $users,
        ]);
    }


    public function scheduleScriptsLayout() {

        $schedule_scripts = DB::table('schedule_scripts')->orderBy('created_at', 'desc')->paginate(20);


        return view('scholars.sa-settings.schedule-scripts', [
            'schedule_scripts' => $schedule_scripts
        ]);
    }


    public function truncateTable(Request $request) {

        $role = Auth::user()->role;
        $data = $request->input('params');

        if ($role == 'superadmin') {

            for ($i=0; $i<count($data["table_names"]); $i++) {
                DB::table($data["table_names"][$i])->truncate();
            }

        }

        return json_encode('success');
    }


    public function dropTable(Request $request) {

        $role = Auth::user()->role;
        $data = $request->input('params');

        if ($role == 'superadmin') {

            for ($i=0; $i<count($data["table_names"]); $i++) {
                Schema::dropIfExists($data["table_names"][$i]);
                //delete from mytable where myvalue like 'findme%';
                DB::delete("delete from migrations where migration like '%". $data['table_names'][$i] . "'");
            }

        }

        return json_encode('success');
    }




}
