<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class QuizController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function addQuizLayout($id) {

        $lecture = DB::table('lectures')
            ->select('lectures.*'
                , DB::raw("(SELECT COUNT(quiz_id) FROM quiz WHERE quiz.lecture_id=$id) as quiz_count")
                , DB::raw("(SELECT name FROM `users` WHERE id= lectures.tutor_id limit 1) as instructor_name")
                , DB::raw("(SELECT course_name FROM `courses` WHERE courses.course_id= lectures.course_id limit 1) as course_name")
            )
            ->where('lecture_id', $id)
            ->first();

        return view('scholars.quiz.create-quiz', [
            'lecture' => $lecture,
        ]);

    }


    public function quiz(Request $request) {

        $quiz_id = $request['quiz_id'];

        $quiz = DB::table('quiz')
            ->where('quiz.quiz_id', $quiz_id)
            ->select('quiz.*', 'courses.course_name', 'lectures.lecture_title')
            ->join('lectures', 'lectures.lecture_id', '=', 'quiz.lecture_id')
            ->join('courses', 'lectures.course_id', '=', 'quiz.course_id')
            ->first();

        $questions = DB::table('quiz_question')
            ->where('quiz_id', $quiz_id)
            ->get();

        return view('scholars.quiz.quiz', [
            'quiz_id' => $quiz_id,
            'quiz' => $quiz,
            'questions' => $questions,

        ]);

    }


    public function storeQuiz(Request $request) {

        $instructor = Auth::user()->id;

        $quiz_title = $request['quiz_title'];
        $quiz_description = $request['quiz_description'];
        $lecture_id = $request['lecture_id'];
        $course_id = $request['course_id'];

        $quiz_id = DB::table('quiz')->insertGetId(
            array(
                'quiz_title' => $quiz_title,
                'quiz_description' => $quiz_description,
                'tutor_id' => $instructor,
                'lecture_id' => $lecture_id,
                'course_id' => $course_id,
            )
        );


        $questions = $request['question'];
        $question_type = $request['question_type'];
        $options = $request['option'];
        $answers = $request['answer'];
        $explanation = $request['explanation'];

        for ($i=0, $j=0; $i<count($questions); $i++) {

            /*
            echo $questions[$i] . ":<br>";
            echo $options[$j] . " ----- " . $options[$j+1] . " ----- " . $options[$j+2] . " ----- " . $options[$j+3] . "<br>";
            echo "answer : " . $answers[$i];
            echo "<br><br><br>";
            */

            if (isset($questions[$i]) && !empty($questions[$i])) {
                /*
                $medianame = "media_" . ($i+1);
                $explanation_medianame = "explanation_media_" . ($i+1);
                $fileName = "";
                $explanation_filename = "";
                */
                /*
                if(Input::hasFile($medianame)){
                    $file = Input::file($medianame);
                    $destinationPath = '/public/questionimage/';
                    $extension = $file->getClientOriginalExtension();
                    $fileName = time() .'.'.$extension;
                    $request->file($medianame)->move(base_path() . $destinationPath, $fileName);
                }

                if(Input::hasFile($explanation_medianame)){
                    $file = Input::file($explanation_medianame);
                    $destinationPath = '/public/explanation/';
                    $extension = $file->getClientOriginalExtension();
                    $explanation_filename = time() .'.'.$extension;
                    $request->file($explanation_medianame)->move(base_path() . $destinationPath, $explanation_filename);
                }
                */

                if ($question_type[$i] == 'text') {

                    DB::table('quiz_question')->insert([
                            'quiz_id' => $quiz_id,
                            'question_type' => $question_type[$i],
                            'question' => $questions[$i],
                            'answer' => $answers[$i],
                            'explanation' => $explanation[$i],
                        ]
                    );

                }
                else {

                    DB::table('quiz_question')->insert([
                            'quiz_id' => $quiz_id,
                            'question_type' => $question_type[$i],
                            'question' => $questions[$i],
                            'option_1' => $options[$j],
                            'option_2' => $options[$j+1],
                            'option_3' => $options[$j+2],
                            'option_4' => $options[$j+3],
                            'answer' => $answers[$i],
                            'explanation' => $explanation[$i],
                        ]
                    );

                    $j += 4;

                }

            }

        }
        return redirect("/lecture/" . $lecture_id);
    }


    public function storeResult(Request $request) {

        $student_id = Auth::user()->id;
        $data = $request->input('params');

        $result_id = DB::table('quiz_results')->insertGetId(
            array(
                'quiz_id' => $data['quiz_id'],
                'student_id' => $student_id,
                'answered_question' => $data['answered_question'],
                'total_question' => $data['total_question'],
                'score' => $data['score'],
            )
        );


        for ($i=0; $i<count($data["submitted_ans"]); $i++) {

            $submitted_answer = $data["submitted_ans"][$i][0];
            $question_id = $data["submitted_ans"][$i][1];

            DB::table('quiz_scripts')->insert([
                'result_id' => $result_id,
                'question_id' => $question_id,
                'submitted_answer' => $submitted_answer,
            ]);

        }

        return json_encode($result_id);

    }


    public function displayQuizResult($id) {

        $result_id = $id;

        $exam_id = DB::table('quiz_results')
            ->where('result_id', $result_id)
            ->select('quiz_id')
            ->first();
        $exam_id = $exam_id->quiz_id;

        $exam_data = DB::table('quiz_results')->where('result_id', $result_id)->first();

        $questions = DB::table('quiz_question')
            ->select('quiz_question.*',
                DB::raw("(SELECT submitted_answer FROM quiz_scripts WHERE result_id=$result_id AND question_id=quiz_question.question_id) as submitted_answer"))
            ->where('quiz_question.quiz_id', $exam_id)
            ->get();


        return view('scholars.quiz.view-quiz-result', [
            'exam_id' => $exam_id,
            'questions' => $questions,
            'exam_data' => $exam_data,
        ]);

    }


    public function viewQuizLayout() {

        $role = Auth::user()->role;
        $user_id = Auth::user()->id;

        if ($role == 'superadmin' || $role == 'admin') {

            $courses = DB::table('courses')->select('course_id', 'course_name')->get();

            $quizzes = DB::table('quiz')
                ->select('quiz.*', 'users.name', 'courses.course_id', 'courses.course_name', 'lectures.lecture_id','lectures.lecture_title' )
                ->join('users', 'users.id', '=', 'quiz.tutor_id')
                ->join('lectures', 'lectures.lecture_id', '=', 'quiz.lecture_id')
                ->join('courses', 'courses.course_id', '=', 'quiz.course_id')
                ->get();
        }

        else if ($role == 'tutor') {
            $courses = DB::table('courses')
                ->where('instructor', $user_id)
                ->select('course_id', 'course_name')
                ->get();

            $quizzes = DB::table('quiz')
                ->where('quiz.tutor_id', $user_id)
                ->select('quiz.*', 'users.name', 'courses.course_id', 'courses.course_name', 'lectures.lecture_id','lectures.lecture_title' )
                ->join('users', 'users.id', '=', 'quiz.tutor_id')
                ->join('lectures', 'lectures.lecture_id', '=', 'quiz.lecture_id')
                ->join('courses', 'courses.course_id', '=', 'quiz.course_id')
                ->get();
        }

        return view('scholars.quiz.view-quiz', [
            'courses' => $courses,
            'quizzes' => $quizzes
        ]);
    }


    public function deleteQuiz(Request $request) {
        $data = $request->input('params');
        $quiz_id = $data['quiz_id'];

        deleteQuiz($quiz_id);

        return json_encode('success');

    }


    public function editQuiz(Request $request) {

        $quiz_id = $request['quiz_id'];

        $quiz = DB::table('quiz')
            ->where('quiz.quiz_id', $quiz_id)
            ->select('quiz.*', 'courses.course_name', 'lectures.lecture_title')
            ->join('lectures', 'lectures.lecture_id', '=', 'quiz.lecture_id')
            ->join('courses', 'lectures.course_id', '=', 'quiz.course_id')
            ->first();

        $questions = DB::table('quiz_question')
            ->where('quiz_id', $quiz_id)
            ->get();


        return view('scholars.quiz.edit-quiz', [
            'quiz' => $quiz,
            'questions' => $questions,

        ]);

    }


    public function updateQuiz(Request $request) {

        $instructor = Auth::user()->id;

        $quiz_id = $request['quiz_id'];
        $lecture_id = $request['lecture_id'];
        $quiz_title = $request['quiz_title'];
        $quiz_description = $request['quiz_description'];

        DB::table('quiz')
            ->where('quiz_id', $quiz_id)
            ->update([
                'quiz_title' => $quiz_title,
                'quiz_description' => $quiz_description,
                'tutor_id' => $instructor,
            ]);

        $question_id = $request['question_id'];
        $questions = $request['question'];
        $question_type = $request['question_type'];
        $options = $request['option'];
        $answers = $request['answer'];
        $explanation = $request['explanation'];


        for ($i=0, $j=0; $i<count($question_id); $i++) {

            if ($question_id[$i] == 0) {

                if (isset($questions[$i]) && !empty($questions[$i])) {

                    if ($question_type[$i] == 'text') {

                        DB::table('quiz_question')->insert([
                                'quiz_id' => $quiz_id,
                                'question_type' => $question_type[$i],
                                'question' => $questions[$i],
                                'answer' => $answers[$i],
                            ]
                        );
                    }
                    else {
                        DB::table('quiz_question')->insert([
                                'quiz_id' => $quiz_id,
                                'question_type' => $question_type[$i],
                                'question' => $questions[$i],
                                'option_1' => $options[$j],
                                'option_2' => $options[$j+1],
                                'option_3' => $options[$j+2],
                                'option_4' => $options[$j+3],
                                'answer' => $answers[$i],
                            ]
                        );

                        $j += 4;
                    }
                }

            }

            else {

                if (isset($questions[$i]) && !empty($questions[$i])) {

                    if ($question_type[$i] == 'text') {

                        DB::table('quiz_question')
                            ->where('question_id', $question_id[$i])
                            ->update([
                                'question' => $questions[$i],
                                'answer' => $answers[$i],
                                'explanation' => $explanation[$i],
                            ]);
                    }
                    else {

                        DB::table('quiz_question')
                            ->where('question_id', $question_id[$i])
                            ->update([
                                'question' => $questions[$i],
                                'option_1' => $options[$j],
                                'option_2' => $options[$j+1],
                                'option_3' => $options[$j+2],
                                'option_4' => $options[$j+3],
                                'answer' => $answers[$i],
                                'explanation' => $explanation[$i],
                            ]);



                        $j += 4;
                    }
                }

            }

        }

        return redirect("/lecture/" . $lecture_id);
    }


    public function deleteQuizQuestion(Request $request) {
        $data = $request->input('params');
        $question_id = $data['question_id'];

        deleteQuizQuestion($question_id);

        return json_encode('success');

    }

}
