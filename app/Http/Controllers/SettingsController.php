<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;


class SettingsController extends Controller {


    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $lookup = DB::table('lookup')
            ->select('lookup.*', 'users.name')
            ->join('users', 'lookup.created_by', '=', 'users.id')
            ->get();

        $course_level = DB::table('course_level')
            ->select('course_level.*', 'users.name')
            ->join('users', 'course_level.created_by', '=', 'users.id')
            ->get();

        $role_lookup = DB::table('role_lookup')
            ->select('role_lookup.*', 'users.name')
            ->join('users', 'role_lookup.created_by', '=', 'users.id')
            ->get();


        $settings_data = DB::table('app_settings')
            ->select('setting_key', 'setting_value')
            ->get();

        $settings = array();
        foreach($settings_data as $row) $settings[$row->setting_key] = $row->setting_value;

        return view('scholars.settings.index', [
            'settings' => $settings,
            'lookup' => $lookup,
            'course_level' => $course_level,
            'role_lookup' => $role_lookup,
        ]);

    }


    public function addLookup(Request $request) {

        $created_by = Auth::user()->id;
        $data = $request->input('params');

        $lookup_id = DB::table('lookup')->insertGetId(
            array(
                'lookup_name' => $data['lookup_name'],
                'created_by' => $created_by,
            )
        );

        return json_encode($lookup_id);
    }


    public function addCourseLevel(Request $request) {

        $created_by = Auth::user()->id;
        $data = $request->input('params');

        $course_level_id = DB::table('course_level')->insertGetId(
            array(
                'course_level_name' => $data['course_level_name'],
                'created_by' => $created_by,
            )
        );

        DB::table('activity')->insert([
            'user_id' => $created_by,
            'lookup_activity' => 1,
            'lookup_category' => 8,
            'lookup_name_id' => $course_level_id,
            'lookup_name_child' => $data['course_level_name']
        ]);

        return json_encode($course_level_id);
    }


    public function addRole(Request $request) {

        $created_by = Auth::user()->id;
        $data = $request->input('params');

        $role_id = DB::table('role_lookup')->insertGetId(
            array(
                'role_name' => $data['role_name'],
                'created_by' => $created_by,
            )
        );

        DB::table('activity')->insert([
            'user_id' => $created_by,
            'lookup_activity' => 1,
            'lookup_category' => 10,
            'lookup_name_id' => $role_id,
            'lookup_name_child' => $data['role_name']
        ]);

        return json_encode($role_id);
    }


    public function saveAppSettings(Request $request) {

        //$created_by = Auth::user()->id;
        $data = $request->input('params');

        DB::table('app_settings')
            ->where('setting_key', 'app_name')
            ->update(['setting_value' => $data['app_name']]);

        DB::table('app_settings')
            ->where('setting_key', 'app_title')
            ->update(['setting_value' => $data['app_title']]);

        DB::table('app_settings')
            ->where('setting_key', 'app_subtitle')
            ->update(['setting_value' => $data['app_subtitle']]);

        DB::table('app_settings')
            ->where('setting_key', 'app_version')
            ->update(['setting_value' => $data['app_version']]);

        DB::table('app_settings')
            ->where('setting_key', 'youtube_video_link')
            ->update(['setting_value' => $data['youtube_video_link']]);

        return json_encode('success');
    }

}
