<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller {

    public function __construct() {
        //$this->middleware('auth');
    }


    public function index($id) {

        $course = DB::table('courses')->where('course_id', $id)->first();
        $faq = DB::table('course_faq')->where('course_id', $id)->get();

        return view('scholars.course.faq-course', [
            'course' => $course,
            'faq' => $faq,
        ]);
    }


    public function addFAQ(Request $request) {
        $data = $request->input('params');
        $created_by = Auth::user()->id;

        $faq_id = DB::table('course_faq')->insertGetId(
            array(
                'course_id' => $data['course_id'],
                'faq_question' => $data['faq_question'],
                'faq_answer' => $data['faq_answer'],
                'created_by' => $created_by,
            )
        );

        return json_encode($faq_id);
    }


    public function editFAQ(Request $request) {

        $data= $request->input('params');

        $faq_id = $data['faq_id'];
        $faq_question = $data['faq_question'];
        $faq_answer = $data['faq_answer'];

        DB::table('course_faq')
            ->where('faq_id', $faq_id)
            ->update([
                'faq_question' => $faq_question,
                'faq_answer' => $faq_answer,
            ]);

        return json_encode('success');
    }


    public function deleteFAQ(Request $request) {

        $data= $request->input('params');
        $faq_id = $data['faq_id'];

        deleteFAQ($faq_id);

        return json_encode('success');
    }

}
