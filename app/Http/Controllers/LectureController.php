<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class LectureController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index($id) {

        $data = DB::table('lectures')
            ->select('lectures.*'
                , DB::raw("(SELECT course_name FROM `courses` WHERE course_id= lectures.course_id limit 1) as course_name")
                , DB::raw("(SELECT free_course FROM `courses` WHERE course_id= lectures.course_id limit 1) as free_course")
                , DB::raw("(SELECT name FROM `users` WHERE id= lectures.tutor_id limit 1) as instructor_name")
                , DB::raw("(SELECT email FROM `users` WHERE id= lectures.tutor_id limit 1) as instructor_email")
                , DB::raw("(SELECT phone FROM `users` WHERE id= lectures.tutor_id limit 1) as instructor_phone")
                , DB::raw("(SELECT user_description FROM `users` WHERE id= lectures.tutor_id limit 1) as user_description")
            )
            ->where('lecture_id', $id)
            ->first();


        $sections = DB::table('sections')
            ->where('course_id', $data->course_id)
            ->get();


        $lectures = DB::table('lectures')
            ->where('course_id', $data->course_id)
            ->get();


        $quizzes = DB::table('quiz')
            ->where('lecture_id', $id)
            ->get();

        $contents = DB::table('lecture_contents')
            ->where('lecture_id', $id)
            ->get();

        $content_flashcard = DB::table('content_flashcard')
            ->where('lecture_id', $id)
            ->get();


        return view('scholars.course.lecture', [
            'data' => $data,
            'sections' => $sections,
            'lectures' => $lectures,
            'quizzes' => $quizzes,
            'contents' => $contents,
            'content_flashcard' => $content_flashcard,
        ]);


    }


    public function addLecture(Request $request) {

        $data= $request->input('params');
        $instructor = Auth::user()->id;

        $lecture_id = DB::table('lectures')->insertGetId(
            array(
                'lecture_title' => $data['lecture_title'],
                'tutor_id' => $instructor,
                'section_id' => $data['section_id'],
                'course_id' => $data['course_id'],
            )
        );

        return json_encode($lecture_id);
    }


    public function addTextContent(Request $request) {
        $data = $request->input('params');
        $tutor_id = Auth::user()->id;

        $content_id = DB::table('lecture_contents')->insertGetId(
            array(
                'content_title' => $data['content_title'],
                'content' => $data['content'],
                'mime_type' => $data['mime_type'],
                'tutor_id' => $tutor_id,
                'lecture_id' => $data['lecture_id'],
                'course_id' => $data['course_id'],
            )
        );

        return json_encode($content_id);
    }


    public function addMediaContent(Request $request) {

        $tutor_id = Auth::user()->id;
        $destinationPath = 'media';
        $lecture_id = $request['lecture_id'];
        create_directory();


        $file = Input::file('content');
        $rules = array('file' => 'required');
        $validator = Validator::make(array('file' => $file), $rules);

        if ($validator->passes()) {
            $post_mime_type = $file->getMimeType();
            $extension = Input::file('content')->getClientOriginalExtension();
            $fileName = 'media_'. time() .'.'.$extension;
            Input::file('content')->move($destinationPath, $fileName);

            $content_id = DB::table('lecture_contents')->insertGetId(
                array(
                    'content_title' => $request['content_title'],
                    'content' => $fileName,
                    'mime_type' => $post_mime_type,
                    'tutor_id' => $tutor_id,
                    'lecture_id' => $request['lecture_id'],
                    'course_id' => $request['course_id'],
                )
            );

        }

        return redirect("/lecture/" . $lecture_id);

    }


    public function addFlashcardContent(Request $request) {

        $tutor_id = Auth::user()->id;
        $destinationPath = 'media';
        $lecture_id = $request['lecture_id'];
        create_directory();


        $file = Input::file('media');
        $rules = array('file' => 'required');
        $validator = Validator::make(array('file' => $file), $rules);

        if ($validator->passes()) {
            $post_mime_type = $file->getMimeType();
            $extension = Input::file('media')->getClientOriginalExtension();
            $fileName = 'flashcard_'. time() .'.'.$extension;
            Input::file('media')->move($destinationPath, $fileName);

            $flashcard_id = DB::table('content_flashcard')->insertGetId(
                array(
                    'flashcard_title' => $request['flashcard_title'],
                    'flashcard_description' => $request['flashcard_description'],
                    'media' => $fileName,
                    'tutor_id' => $tutor_id,
                    'lecture_id' => $request['lecture_id']
                )
            );

        }

        return redirect("/lecture/" . $lecture_id);

    }


    public function editLecture(Request $request) {
        $data = $request->input('params');

        DB::table('lectures')
            ->where('lecture_id', $data['lecture_id'])
            ->update([
                'section_id' => $data['section_id'],
                'lecture_title' => $data['lecture_title'],
                'lecture_description' => $data['lecture_description']
            ]);

        return json_encode('success');
    }


    public function deleteLecture(Request $request) {
        $data = $request->input('params');
        $lecture_id = $data['lecture_id'];

        deleteLecture($lecture_id);

        return json_encode('success');

    }


    public function deleteContent(Request $request) {
        $data = $request->input('params');
        $content_id = $data['content_id'];

        deleteContent($content_id);

        return json_encode('success');

    }


    public function editContent(Request $request) {

        $data = $request->input('params');

        $content_id = $data['content_id'];
        $content_title = $data['content_title'];
        $content = $data['content'];

        DB::table('lecture_contents')
            ->where('content_id', $content_id)
            ->update([
                'content_title' => $content_title,
                'content' => $content
            ]);

        return json_encode('success');
    }


    public function editMediaContent(Request $request) {

        $tutor_id = Auth::user()->id;
        $destinationPath = 'media';
        $lecture_id = $request['lecture_id'];
        $content_id = $request['content_id'];
        $flag = true;
        create_directory();

        $file = Input::file('content');
        $rules = array('file' => 'required');
        $validator = Validator::make(array('file' => $file), $rules);

        if ($validator->passes()) {
            $flag = false;
            $post_mime_type = $file->getMimeType();
            $extension = Input::file('content')->getClientOriginalExtension();
            $fileName = 'media_'. time() .'.'.$extension;
            Input::file('content')->move($destinationPath, $fileName);

            $contents = DB::table('lecture_contents')->select('content')->where('content_id', $content_id)->first();
            deleteFile($destinationPath . '/' . $contents->content);

            DB::table('lecture_contents')
                ->where('content_id', $content_id)
                ->update([
                    'content_title' => $request['content_title'],
                    'content' => $fileName,
                    'mime_type' => $post_mime_type,
                    'tutor_id' => $tutor_id,
                ]);
        }

        if ($flag) {

            DB::table('lecture_contents')
                ->where('content_id', $content_id)
                ->update([
                    'content_title' => $request['content_title'],
                    'tutor_id' => $tutor_id,
                ]);
        }


        return redirect("/lecture/" . $lecture_id);

    }


}
