<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller {

    public function __construct() {
        setup();
    }


    public function index() {
        return view('home');
    }


    public function test() {
        testFunction();
    }


    public function welcome() {

        $catagories = DB::table('course_category')
            ->get();

        $courses = DB::table('courses')
            ->select('courses.*', 'users.name', 'users.user_img'
                , DB::raw("(SELECT COUNT(course_student_id) FROM course_student WHERE course_student.course_id=courses.course_id) as student_count")
                , DB::raw("(SELECT category_name FROM `course_category` WHERE category_id= courses.category_id limit 1) as category_name")
            )
            ->leftJoin('users', 'users.id', '=', 'courses.instructor')
            ->paginate(6);

        $data = DB::table('users')
            ->select(
                DB::raw("(SELECT COUNT(course_id)FROM courses) as total_courses")
                , DB::raw("(SELECT COUNT(id) FROM users WHERE role='student') as total_student")
                , DB::raw("(SELECT COUNT(id) FROM users WHERE role='tutor') as total_tutor")

            )
            ->first();

        $settings_data = DB::table('app_settings')
            ->select('setting_key', 'setting_value')
            ->get();

        $settings = array();
        foreach($settings_data as $row) $settings[$row->setting_key] = $row->setting_value;


        return view('welcome', [
            'catagories' => $catagories,
            'courses' => $courses,
            'settings' => $settings,
            'data' => $data,
        ]);

    }


    public function about() {




        return view('scholars.about.index', [

        ]);

    }

}
