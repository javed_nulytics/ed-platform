<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AnalyticsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $students = DB::table('users')
            ->where('role', 'student')
            ->select('users.*',
                DB::raw("(SELECT COUNT(course_student_id) FROM course_student WHERE course_student.student_id=users.id) as course_count"),
                DB::raw("(SELECT AVG(score) FROM quiz_results WHERE quiz_results.student_id=users.id) as avg_quiz_score"),
                DB::raw("(SELECT AVG(score) FROM exam_results WHERE exam_results.student_id=users.id) as avg_exam_score"),
                DB::raw("(SELECT AVG(completed) FROM course_student WHERE course_student.student_id=users.id) as course_completed_sum")
            )
            ->get();




        return view('scholars.analytics.index', [
            'students' => $students
        ]);

    }


    public function viewStudentAnalytics($id) {

        $user_id = $id;

        $year = date("Y");

        $progress_analytics = DB::select("SELECT MONTH(created_at) MONTH, COUNT(*) COUNT, AVG(score) SCORE FROM quiz_results WHERE YEAR(created_at)=$year AND student_id=$user_id GROUP BY MONTH(created_at)");


        $exam_analytics = DB::select("SELECT MONTH(created_at) MONTH, COUNT(*) COUNT, AVG(score) SCORE FROM exam_results WHERE YEAR(created_at)=$year AND student_id=$user_id GROUP BY MONTH(created_at)");


        $student_data = DB::table('users')
            ->where('users.id', $user_id)
            ->select('users.*',
                DB::raw("(SELECT AVG(score) FROM quiz_results WHERE student_id=$user_id) as avg_quiz_score"),
                DB::raw("(SELECT AVG(score) FROM exam_results WHERE student_id=$user_id) as avg_exam_score")
            )
            ->first();


        $courses = DB::table('course_student')
            ->where('student_id', $user_id)
            ->select('course_student.created_at as started_date', 'course_student.completed', 'courses.*')
            ->join('courses', 'courses.course_id', '=', 'course_student.course_id')
            ->get();


        $quiz_results = DB::table('quiz_results')
            ->where('student_id', $user_id)
            ->select('quiz_results.*', 'quiz.quiz_title', 'courses.course_id', 'courses.course_name', 'lectures.lecture_id', 'lectures.lecture_title')
            ->join('quiz', 'quiz.quiz_id', '=', 'quiz_results.quiz_id')
            ->join('courses', 'quiz.course_id', '=', 'courses.course_id')
            ->join('lectures', 'quiz.lecture_id', '=', 'lectures.lecture_id')
            ->orderBy('quiz_results.created_at', 'desc')
            ->take(5)
            ->get();


        $exam_results = DB::table('exam_results')
            ->where('student_id', $user_id)
            ->select('exam_results.*', 'exams.exam_title', 'courses.course_id', 'courses.course_name')
            ->join('exams', 'exams.exam_id', '=', 'exam_results.exam_id')
            ->join('courses', 'exams.course_id', '=', 'courses.course_id')
            ->orderBy('exam_results.created_at', 'desc')
            ->take(5)
            ->get();




        return view('scholars.analytics.student-report', [
            'courses' => $courses,
            'quiz_results' => $quiz_results,
            'progress_analytics' => $progress_analytics,
            'student_data' => $student_data,
            'exam_results' => $exam_results,
            'exam_analytics' => $exam_analytics,
        ]);

    }

}
