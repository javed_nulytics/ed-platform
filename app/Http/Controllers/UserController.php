<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('admin');
    }


    public function viewUsers() {

        $users = DB::table('users')
            ->select('users.id', 'users.name', 'users.email', 'users.role', 'users.user_description', 'users.user_img', 'users.created_at', 'status_lookup.status_name')
            ->join('status_lookup', 'status_lookup.status_id', '=', 'users.status')
            ->get();
        $role_lookup = DB::table('role_lookup')->get();


        return view('scholars.users.index', [
            'users' => $users,
            'role_lookup' => $role_lookup,
        ]);



    }


    public function addUser(Request $request) {
        $created_by = Auth::user()->id;
        $data = $request->input('params');

        $user_id = DB::table('users')->insertGetId(
            array(
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'role' => $data['role'],
                'user_description' => $data['user_description'],
            )
        );

        DB::table('activity')->insert([
            'user_id' => $created_by,
            'lookup_activity' => 1,
            'lookup_category' => 9,
            'lookup_name_id' => $user_id,
            'lookup_name_child' => $data['name']
        ]);

        return json_encode($user_id);
    }


    public function editUserRole(Request $request) {

        $data = $request->input('params');

        DB::table('users')
            ->where('id', $data['user_id'])
            ->update([
                'role' => $data['role']
            ]);

        return json_encode('success');
    }


    public function generateUser() {

        $file_name = 'names.txt';
        $file = fopen( $file_name, "r");
        $date = date('m-d H:i:s');

        $count = 1;

        while (!feof($file)) {

            $array = fgetcsv($file);
            $name = $array[0];
            $phone = '017' . rand(11000000,99999999);
            $email = 'testuser_' . $count++ . '@example.com';

            if ($count%5 == 0) $role = 'tutor';
            else $role = 'student';

            $random_year = mt_rand(2014,2018);
            $random_date = $random_year . '-' . $date;

            //echo $name . '<br>';

            $user_id = DB::table('users')->insertGetId(
                array(
                    'name' => $name,
                    'email' => $email,
                    'password' => bcrypt('123456'),
                    'role' => $role,
                    'phone' => $phone,
                    'created_at' => $random_date,
                )
            );
        }
        return redirect('/users');
    }


    public function deleteUser(Request $request) {
        $data = $request->input('params');
        $user_id = $data['user_id'];

        deleteUser($user_id);

        return json_encode('success');

    }


    public function editUser(Request $request) {

        $data = $request->input('params');

        $user_id = $data['id'];
        $name = $data['name'];
        $role = $data['role'];
        $user_description = $data['user_description'];

        DB::table('users')
            ->where('id', $user_id)
            ->update([
                'name' => $name,
                'role' => $role,
                'user_description' => $user_description
            ]);

        return json_encode('success');

    }


    public function changePassword(Request $request) {

        $data = $request->input('params');

        $new_password = $data['new_password'];
        $user_id = $data['user_id'];

        changePassword($user_id, $new_password);
        return json_encode('success');

    }

}
