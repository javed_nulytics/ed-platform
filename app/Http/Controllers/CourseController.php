<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller {

    public function __construct() {
        //setup();
        //$this->middleware('auth');
    }


    public function createCourseLayout() {

        $catagories = DB::table('course_category')
            ->get();

        $course_level = DB::table('course_level')
            ->get();


        return view('scholars.course.create-course', [
            'catagories' => $catagories,
            'course_level' => $course_level
        ]);

    }


    public function createCourse(Request $request) {

        $instructor = Auth::user()->id;

        $course_name = $request->input('course_name');
        $course_title = $request->input('course_title');
        $course_overview = $request->input('course_overview');
        $what_will_i_learn = $request->input('what_will_i_learn');
        $course_requirement = $request->input('course_requirement');
        $category = $request->input('category');
        $price = $request->input('price')!=Null?$request->input('price'):0;
        $discount = $request->input('discount')!=Null?$request->input('discount'):0;
        $free_course = $request->input('free_course');
        $course_level = $request->input('course_level');

        if ($free_course == 'on') $free_course = 1;
        else $free_course = 0;


        $id = DB::table('courses')->insertGetId(
            array(
                'course_name' => $course_name,
                'course_title' => $course_title,
                'course_overview' => $course_overview,
                'what_will_i_learn' => $what_will_i_learn,
                'course_requirement' => $course_requirement,
                'category_id' => $category,
                'price' => $price,
                'discount' => $discount,
                'free_course' => $free_course,
                'course_level' => $course_level,
                'instructor' => $instructor,
            )
        );


        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
            ]);

            $file_name = time() . '.' .$file->getClientOriginalExtension();
            $destinationPath = 'images/course';
            $file->move($destinationPath,$file_name);


            DB::table('courses')
                ->where('course_id', $id)
                ->update([
                    'course_image' => $file_name
                ]);
        }

        DB::table('activity')->insert([
            'user_id' => $instructor,
            'lookup_activity' => 1,
            'lookup_category' => 4,
            'lookup_name_id' => $id,
            'lookup_name_child' => $course_name
        ]);

        return redirect('/course/' . $id);

    }


    public function course($id) {

        $course = DB::table('courses')
            ->select('courses.*', 'users.name', 'course_category.category_name', 'course_level.course_level_name', 'status_lookup.status_name'
                , DB::raw("(SELECT COUNT(course_student_id) FROM `course_student` WHERE course_id= courses.course_id limit 1) as student_count")
            )
            ->join('users', 'users.id', '=', 'courses.instructor')
            ->join('course_category', 'course_category.category_id', '=', 'courses.category_id')
            ->join('course_level', 'course_level.course_level_id', '=', 'courses.course_level')
            ->join('status_lookup', 'status_lookup.status_id', '=', 'courses.status')
            ->where('course_id', $id)
            ->first();


        $sections = DB::table('sections')
            ->where('course_id', $id)
            ->get();


        $lectures = DB::table('lectures')
            ->where('course_id', $id)
            ->get();


        $course_student = DB::table('course_student')
            ->where('course_id', $id)
            ->select('course_student.*', 'users.name', 'users.user_img')
            ->join('users', 'users.id', '=', 'course_student.student_id')
            ->get();


        return view('scholars.course.course', [
            'course' => $course,
            'sections' => $sections,
            'lectures' => $lectures,
            'course_student' => $course_student,
        ]);


    }


    public function category($category) {

        $message = '';

        if ($category == 'all') {

            $catagories = DB::table('course_category')
                ->get();

            $courses = DB::table('courses')
                ->select('courses.*', 'users.name', 'users.user_img', 'course_category.category_name'
                    , DB::raw("(SELECT COUNT(course_student_id) FROM course_student WHERE course_student.course_id=courses.course_id) as student_count")
                )
                ->join('users', 'users.id', '=', 'courses.instructor')
                ->join('course_category', 'course_category.category_id', '=', 'courses.category_id')
                ->get();

        }

        else {

            $category_name = DB::table('course_category')
                ->select('category_name')
                ->where('category_id', $category)
                ->first();

            $message = "Showing List of $category_name->category_name category";

            $catagories = DB::table('course_category')
                ->get();

            $courses = DB::table('courses')
                ->select('courses.*', 'users.name', 'users.user_img', 'course_category.category_name'
                    , DB::raw("(SELECT COUNT(course_student_id) FROM course_student WHERE course_student.course_id=courses.course_id) as student_count")
                )
                ->join('users', 'users.id', '=', 'courses.instructor')
                ->join('course_category', 'course_category.category_id', '=', 'courses.category_id')
                ->where('courses.category_id', $category)
                ->get();

        }

        return view('scholars.course.all-course', [
            'catagories' => $catagories,
            'courses' => $courses,
            'message' => $message,
            'category_id' => $category,
        ]);
    }


    public function enrollmentCourse(Request $request) {
        //$this->middleware('auth');

        $data= $request->input('params');
        $userId = Auth::user()->id;
        $course_status = 4;
        $status_id = '';

        if ($data['free_course'] == 1) {
            $status_id = DB::table('status_lookup')
                ->where('status_name', 'accepted')
                ->first();
        }
        else {
            $status_id = DB::table('status_lookup')
                ->where('status_name', 'requested')
                ->first();
        }

        $course_status = $status_id->status_id;

        DB::table('course_student')->insert([
            'course_id' => $data['course_id'],
            'student_id' => $userId,
            'status' => $course_status,
        ]);

        return json_encode($data['free_course']);
    }


    public function editCourseLayout(Request $request) {

        $course_id = $request->input('form_course_id');
        $role = Auth::user()->role;



        $course = DB::table('courses')
            ->select('courses.*', 'users.name')
            ->where('course_id', $course_id)
            ->join('users', 'users.id', '=', 'courses.instructor')
            ->first();


        if ($role == 'superadmin' || $role == 'admin') {
            $users = DB::table('users')
                ->where('role', '<>', 'student')
                ->select('id', 'name')
                ->get();
        }

        else {
            $users = DB::table('users')
                ->where('id', $course->instructor)
                ->select('id', 'name')
                ->get();
        }

        $catagories = DB::table('course_category')
            ->get();

        $course_level = DB::table('course_level')
            ->get();

        $redirect_url = $request->input('redirect_url');

        return view('scholars.course.edit-course', [
            'catagories' => $catagories,
            'course_level' => $course_level,
            'course' => $course,
            'redirect_url' => $redirect_url,
            'users' => $users,
        ]);



    }


    public function editCourse(Request $request) {

        //$instructor = Auth::user()->id;

        $course_id = $request->input('course_id');

        $instructor = $request->input('instructor');
        $course_name = $request->input('course_name');
        $course_title = $request->input('course_title');
        $course_overview = $request->input('course_overview');
        $what_will_i_learn = $request->input('what_will_i_learn');
        $course_requirement = $request->input('course_requirement');
        $category = $request->input('category');
        $price = $request->input('price');
        $discount = $request->input('discount');
        $free_course = $request->input('free_course');
        $course_level = $request->input('course_level');

        if ($free_course == 'on') $free_course = 1;
        else $free_course = 0;


        DB::table('courses')
            ->where('course_id', $course_id)
            ->update([
                'course_name' => $course_name,
                'course_title' => $course_title,
                'course_overview' => $course_overview,
                'what_will_i_learn' => $what_will_i_learn,
                'course_requirement' => $course_requirement,
                'instructor' => $instructor,
                'category_id' => $category,
                'price' => $price,
                'discount' => $discount,
                'free_course' => $free_course,
                'course_level' => $course_level,
            ]);


        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
            ]);

            $file_name = time() . '.' .$file->getClientOriginalExtension();
            $destinationPath = 'images/course';
            $file->move($destinationPath,$file_name);


            DB::table('courses')
                ->where('course_id', $course_id)
                ->update([
                    'course_image' => $file_name
                ]);
        }

        DB::table('activity')->insert([
            'user_id' => $instructor,
            'lookup_activity' => 2,
            'lookup_category' => 4,
            'lookup_name_id' => $course_id,
            'lookup_name_child' => $course_name,
        ]);

        $redirect_url = $request->input('redirect_url');

        return redirect($redirect_url);

    }


    public function addSection(Request $request) {

        $data= $request->input('params');
        $instructor = Auth::user()->id;

        $section_id = DB::table('sections')->insertGetId(
            array(
                'course_id' => $data['course_id'],
                'section_name' => $data['section_name'],
            )
        );


        DB::table('activity')->insert([
            'user_id' => $instructor,
            'lookup_activity' => 1,
            'lookup_category' => 5,
            'lookup_name_id' => $section_id,
            'lookup_name_child' => $data['section_name'],
        ]);



        return json_encode($section_id);
    }


    public function courseStudentStatus(Request $request) {
        $data = $request->input('params');

        if ($data['status'] == 'requested') {

            $status_id = DB::table('status_lookup')
                ->where('status_name', 'accepted')
                ->first();
            $course_status = $status_id->status_id;


            DB::table('course_student')
                ->where('course_student_id', $data['course_student_id'])
                ->update([
                    'status' => $course_status
                ]);
        }

        return json_encode('success');
    }


    public function addCategory(Request $request) {
        $created_by = Auth::user()->id;
        $data = $request->input('params');

        $category_id = DB::table('course_category')->insertGetId(
            array(
                'category_name' => $data['category_name'],
                'created_by' => $created_by
            )
        );

        DB::table('activity')->insert([
            'user_id' => $created_by,
            'lookup_activity' => 1,
            'lookup_category' => 7,
            'lookup_name_id' => $category_id,
            'lookup_name_child' => $data['category_name']
        ]);

        return json_encode($category_id);
    }


    public function addLevel(Request $request) {
        $created_by = Auth::user()->id;
        $data = $request->input('params');

        $course_level_id = DB::table('course_level')->insertGetId(
            array(
                'course_level_name' => $data['course_level_name'],
                'created_by' => $created_by
            )
        );

        DB::table('activity')->insert([
            'user_id' => $created_by,
            'lookup_activity' => 1,
            'lookup_category' => 8,
            'lookup_name_id' => $course_level_id,
            'lookup_name_child' => $data['course_level_name']
        ]);

        return json_encode($course_level_id);
    }


    public function viewCourses() {

        $user_role = Auth::user()->role;

        if ($user_role == 'superadmin' || $user_role == 'admin') {

            $courses = DB::table('courses')
                ->select('courses.*', 'users.id', 'users.name', 'course_category.category_name', 'course_level.course_level_name', 'status_lookup.status_name'
                    , DB::raw("(SELECT COUNT(course_student_id) FROM `course_student` WHERE course_id= courses.course_id limit 1) as no_student")
                )
                ->join('users', 'users.id', '=', 'courses.instructor')
                ->join('course_category', 'course_category.category_id', '=', 'courses.category_id')
                ->join('course_level', 'course_level.course_level_id', '=', 'courses.course_level')
                ->join('status_lookup', 'status_lookup.status_id', '=', 'courses.status')
                ->orderBy('no_student', 'desc')
                ->get();

        }

        else if ($user_role == 'tutor') {

            $user_id = Auth::user()->id;

            $courses = DB::table('courses')
                ->where('instructor', $user_id)
                ->select('courses.*', 'users.id', 'users.name', 'course_category.category_name', 'course_level.course_level_name', 'status_lookup.status_name'
                    , DB::raw("(SELECT COUNT(course_student_id) FROM `course_student` WHERE course_id= courses.course_id limit 1) as no_student")
                )
                ->join('users', 'users.id', '=', 'courses.instructor')
                ->join('course_category', 'course_category.category_id', '=', 'courses.category_id')
                ->join('course_level', 'course_level.course_level_id', '=', 'courses.course_level')
                ->join('status_lookup', 'status_lookup.status_id', '=', 'courses.status')
                ->orderBy('no_student', 'desc')
                ->get();

        }

        return view('scholars.course.view-courses', [
            'courses' => $courses,
        ]);



    }


    public function viewCourseRequest($id=0) {

        $user_role = Auth::user()->role;
        $user_id = Auth::user()->id;
        $course = '';

        if ($id != 0) {
            $course = DB::table('courses')
                ->select('courses.course_id', 'courses.course_name')
                ->where('course_id', $id)
                ->first();
        }

        if ($user_role == 'superadmin' || $user_role == 'admin') {

            $course_requests = DB::table('course_student')
                ->select('course_student.*', 'users.name', 'courses.course_name', 'status_lookup.status_name')
                ->join('users', 'users.id', '=', 'course_student.student_id')
                ->join('courses', 'courses.course_id', '=', 'course_student.course_id')
                ->join('status_lookup', 'status_lookup.status_id', '=', 'course_student.status')
                ->orderBy('course_student.created_at', 'desc')
                ->get();
        }

        else if ($user_role == 'tutor') {

            $course_requests = DB::table('course_student')
                ->where('courses.instructor', $user_id)
                ->select('course_student.*', 'users.name', 'courses.course_name', 'status_lookup.status_name')
                ->join('users', 'users.id', '=', 'course_student.student_id')
                ->join('courses', 'courses.course_id', '=', 'course_student.course_id')
                ->join('status_lookup', 'status_lookup.status_id', '=', 'course_student.status')
                ->orderBy('course_student.created_at', 'desc')
                ->get();

        }


        return view('scholars.course.course-request', [
            'course_requests' => $course_requests,
            'course_id' => $id,
            'course' => $course,
        ]);



    }


    public function deleteCourse(Request $request) {

        $data = $request->input('params');
        $userId = Auth::user()->id;
        $course_id = $data['course_id'];

        deleteCourse($course_id);

        echo json_encode("success");
    }


    public function editSection(Request $request) {

        $data= $request->input('params');

        DB::table('sections')
            ->where('section_id', $data['section_id'])
            ->update([
                'section_name' => $data['section_name']
            ]);

        return json_encode('success');
    }


    public function deleteSection(Request $request) {

        $data= $request->input('params');
        $section_id = $data['section_id'];

        deleteSection($section_id);

        return json_encode('success');
    }


    public function deleteEnrollment(Request $request) {

        $data= $request->input('params');
        $course_student_id = $data['course_student_id'];

        DB::table('course_student')->where('course_student_id', $course_student_id)->delete();

        return json_encode('success');
    }


    public function deleteCategory(Request $request) {

        $data= $request->input('params');
        $category_id = $data['category_id'];

        DB::table('course_category')->where('category_id', $category_id)->delete();

        return json_encode('success');
    }


    public function enrolledCourses() {

        $user_id = Auth::user()->id;

        $courses = DB::table('course_student')
            ->where('student_id', $user_id)
            ->select('course_student.course_student_id', 'course_student.created_at as started_date', 'courses.*')
            ->join('courses', 'courses.course_id', '=', 'course_student.course_id')
            ->get();

        return view('scholars.course.enrolled-courses', [
            'courses' => $courses,
        ]);

    }


    public function clearCourseRequest(Request $request) {

        DB::table('course_student')->truncate();

        return json_encode('success');

    }


    public function search($key) {

        $message = '';

        $catagories = DB::table('course_category')
            ->get();

        $courses = DB::table('courses')
            ->select('courses.*', 'users.name', 'users.user_img', 'course_category.category_name'
                , DB::raw("(SELECT COUNT(course_student_id) FROM course_student WHERE course_student.course_id=courses.course_id) as student_count")
            )
            ->join('users', 'users.id', '=', 'courses.instructor')
            ->join('course_category', 'course_category.category_id', '=', 'courses.category_id')
            ->where('courses.course_name', 'like', '%' . $key . '%')
            ->get();

        return view('scholars.course.all-course', [
            'catagories' => $catagories,
            'courses' => $courses,
            'message' => $message,
            'key' => $key,
        ]);
    }

}
