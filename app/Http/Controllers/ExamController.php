<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use File;


class ExamController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function viewExam() {

        $catagories = DB::table('course_category')
            ->get();

        $courses = DB::table('courses')
            ->select('courses.*', 'users.name', 'users.user_img', 'course_category.category_name'
                , DB::raw("(SELECT COUNT(exam_id) FROM exams WHERE exams.course_id=courses.course_id) as exam_count")
            )
            ->join('users', 'users.id', '=', 'courses.instructor')
            ->join('course_category', 'course_category.category_id', '=', 'courses.category_id')
            ->get();

        return view('scholars.exam.index', [
            'catagories' => $catagories,
            'courses' => $courses,
        ]);

    }


    public function createExamLayout() {

        $role = Auth::user()->role;
        $user_id = Auth::user()->id;

        if ($role == 'superadmin' || $role == 'admin') {
            $courses = DB::table('courses')->select('course_id', 'course_name')->get();
        }

        else if ($role == 'tutor') {
            $courses = DB::table('courses')
                ->where('instructor', $user_id)
                ->select('course_id', 'course_name')
                ->get();
        }


        return view('scholars.exam.create-exam', [
            'courses' => $courses
        ]);

    }


    public function createExam(Request $request) {

        $instructor = Auth::user()->id;




        $exam_title = $request['exam_title'];
        $exam_description = $request['exam_description'];
        $scheduled = $request['scheduled']=='on'?1:0;
        $schedule_start_date = $request['schedule_start_date'];
        $schedule_start_time = $request['schedule_start_time'];
        $duration = $request['duration'];
        $course_id = $request['course_id'];
        $published = $request['published']=='on'?1:0;

        $date=date_create($schedule_start_date . ' ' . $schedule_start_time);
        $schedule_start = date_format($date,"Y-m-d H:i:s");



        /*
                echo '<br><br> Exam Title: ' . $exam_title . '<br>';
                echo 'exam_description: ' . $exam_description . '<br>';
                echo 'schedule: ' . $scheduled . '<br>';
                echo 'schedule start date: ' . $schedule_start_date . '<br>';
                echo 'schedule start time: ' . $schedule_start_time . '<br>';
                echo 'duration: ' . $duration . '<br>';
                echo 'course id: ' . $course_id . '<br>';
                echo 'published: ' . $published . '<br>';
        */

        $exam_id = DB::table('exams')->insertGetId(
            array(
                'exam_title' => $exam_title,
                'exam_description' => $exam_description,
                'scheduled' => $scheduled,
                'schedule_start' => $schedule_start,
                'duration' => $duration,
                'tutor_id' => $instructor,
                'course_id' => $course_id,
                'published' => $published,
            )
        );


        $questions = $request['question'];
        $question_type = $request['question_type'];
        $options = $request['option'];
        $answers = $request['answer'];

        for ($i=0, $j=0; $i<count($questions); $i++) {

            /*
            echo $questions[$i] . ":<br>";
            echo $options[$j] . " ----- " . $options[$j+1] . " ----- " . $options[$j+2] . " ----- " . $options[$j+3] . "<br>";
            echo "answer : " . $answers[$i];
            echo "<br><br><br>";
            */

            if (isset($questions[$i]) && !empty($questions[$i])) {

                $medianame = "media_" . ($i+1);
                $explanation_medianame = "explanation_media_" . ($i+1);
                $fileName = "";
                $explanation_filename = "";


                if(Input::hasFile($medianame)){
                    $file = Input::file($medianame);
                    $destinationPath = '/public/questionimage/';
                    $extension = $file->getClientOriginalExtension();
                    $fileName = time() .'.'.$extension;
                    $request->file($medianame)->move(base_path() . $destinationPath, $fileName);
                }

                if(Input::hasFile($explanation_medianame)){
                    $file = Input::file($explanation_medianame);
                    $destinationPath = '/public/explanation/';
                    $extension = $file->getClientOriginalExtension();
                    $explanation_filename = time() .'.'.$extension;
                    $request->file($explanation_medianame)->move(base_path() . $destinationPath, $explanation_filename);
                }


                if ($question_type[$i] == 'text') {

                    DB::table('exam_question')->insert([
                            'exam_id' => $exam_id,
                            'question_type' => $question_type[$i],
                            'question' => $questions[$i],
                            'answer' => $answers[$i],
                        ]
                    );

                }
                else {

                    DB::table('exam_question')->insert([
                            'exam_id' => $exam_id,
                            'question_type' => $question_type[$i],
                            'question' => $questions[$i],
                            'option_1' => $options[$j],
                            'option_2' => $options[$j+1],
                            'option_3' => $options[$j+2],
                            'option_4' => $options[$j+3],
                            'answer' => $answers[$i],
                        ]
                    );

                    $j += 4;

                }

            }

        }

        return redirect("/exam/course/" . $course_id);
    }


    public function viewCourseExam($id) {

        $exams = DB::table('exams')
            ->where('course_id', $id)
            ->get();

        $course_data = DB::table('courses')
            ->select('course_id', 'course_name')
            ->where('course_id', $id)
            ->first();

        return view ('scholars.exam.course-exams', [
            'exams' => $exams,
            'course_data' => $course_data,
        ]);
    }


    public function exam(Request $request) {

        $exam_id = $request['exam_id'];


        $exam_data = DB::table('exams')->where('exam_id', $exam_id)->first();

        $questions = DB::table('exam_question')
            ->where('exam_id', $exam_id)
            ->get();


        return view ('scholars.exam.exam', [
            'exam_id' => $exam_id,
            'questions' => $questions,
            'exam_data' => $exam_data,

        ]);
    }


    public function storeResult(Request $request) {

        $student_id = Auth::user()->id;
        $data = $request->input('params');

        $result_id = DB::table('exam_results')->insertGetId(
            array(
                'exam_id' => $data['exam_id'],
                'student_id' => $student_id,
                'answered_question' => $data['answered_question'],
                'total_question' => $data['total_question'],
                'score' => $data['score'],
            )
        );

        for ($i=0; $i<count($data["submitted_ans"]); $i++) {

            $submitted_answer = $data["submitted_ans"][$i][0];
            $question_id = $data["submitted_ans"][$i][1];

            DB::table('exam_scripts')->insert([
                'result_id' => $result_id,
                'question_id' => $question_id,
                'submitted_answer' => $submitted_answer,
            ]);

        }

        return json_encode($result_id);

    }


    public function viewExamResult($id) {

        $result_id = $id;

        $exam_id = DB::table('exam_results')
            ->where('result_id', $result_id)
            ->select('exam_id')
            ->first();
        $exam_id = $exam_id->exam_id;


        $exam_data = DB::table('exam_results')->where('result_id', $result_id)->first();

        $questions = DB::table('exam_question')
            ->select('exam_question.*',
                DB::raw("(SELECT submitted_answer FROM exam_scripts WHERE result_id=$result_id AND question_id=exam_question.question_id) as submitted_answer"))
            ->where('exam_question.exam_id', $exam_id)
            ->get();


        return view ('scholars.exam.exam-result', [
            'exam_id' => $exam_id,
            'questions' => $questions,
            'exam_data' => $exam_data,

        ]);
    }


    public function examHistory() {

        $user_id = Auth::user()->id;

        $exam_results = DB::table('exam_results')
            ->where('student_id', $user_id)
            ->select('exam_results.*', 'exams.exam_title', 'courses.course_id', 'courses.course_name')
            ->join('exams', 'exams.exam_id', '=', 'exam_results.exam_id')
            ->join('courses', 'exams.course_id', '=', 'courses.course_id')
            ->orderBy('exam_results.created_at', 'desc')
            ->get();

        return view('scholars.exam.exam-history', [
            'exam_results' => $exam_results
        ]);
    }


    public function deleteExam(Request $request) {
        $data = $request->input('params');
        $exam_id = $data['exam_id'];

        deleteExam($exam_id);

        return json_encode('success');

    }




}
