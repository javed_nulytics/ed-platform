<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsTutor{

    public function handle($request, Closure $next) {

        if (Auth::user() && (Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin' || Auth::user()->role == 'tutor')) {
            return $next($request);
        }

        return redirect('/');
    }
}
