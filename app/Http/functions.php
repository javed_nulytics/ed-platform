<?php


function setup() {

    $count = DB::table('users')->count();
    $user_id= 1;

    if ($count == 0) {

        $date = date('Y-m-d H:i:s');

        $user_id = DB::table('users')->insertGetId(
            array(
                'name' => 'Javed Nayeem',
                'email' => 'superuser@scholars.com',
                'password' => bcrypt('091288'),
                'role' => 'superadmin',
                'created_at' => $date,
                'updated_at' => $date
            )
        );
    }
    app_settings();
    //set_lookup($user_id);
}


function app_settings() {

    $count = DB::table('app_settings')->count();

    if ($count == 0) {
        DB::table('app_settings')->insert([
            'setting_key' => 'app_name',
            'setting_value' => '3 Scholars',
            'user_id' => 1,
        ]);

        DB::table('app_settings')->insert([
            'setting_key' => 'app_title',
            'setting_value' => 'LEARN WHAT YOU LOVE !',
            'user_id' => 1,
        ]);

        DB::table('app_settings')->insert([
            'setting_key' => 'app_subtitle',
            'setting_value' => 'Take High-Quality Online Courses from the Best Online Instructors of Bangladesh & Develop Your Skills',
            'user_id' => 1,
        ]);

        DB::table('app_settings')->insert([
            'setting_key' => 'app_version',
            'setting_value' => '1.0',
            'user_id' => 1,
        ]);

        DB::table('app_settings')->insert([
            'setting_key' => 'youtube_video_link',
            'setting_value' => 'https://www.youtube.com/embed/vlfDUtWngQU',
            'user_id' => 1,
        ]);
    }
}


function set_lookup($user_id=1) {

    $count = DB::table('lookup')->count();
    if ($count == 0) {
        $lookup = array('created', 'updated', 'deleted', 'course', 'section', 'lecture', 'category', 'level', 'user', 'role');
        for($i=0; $i<count($lookup); $i++) {
            DB::table('lookup')->insert([
                'lookup_name' => $lookup[$i],
                'created_by' => $user_id,
            ]);
        }
    }


    $count = DB::table('role_lookup')->count();
    if ($count == 0) {
        $lookup = array('superadmin', 'admin', 'tutor', 'student');
        for($i=0; $i<count($lookup); $i++) {
            DB::table('role_lookup')->insert([
                'role_name' => $lookup[$i],
                'created_by' => $user_id,
            ]);
        }
    }


    $count = DB::table('course_level')->count();
    if ($count == 0) {
        $lookup = array('beginner', 'intermediate', 'expert');
        for($i=0; $i<count($lookup); $i++) {
            DB::table('course_level')->insert([
                'course_level_name' => $lookup[$i],
                'created_by' => $user_id,
            ]);
        }
    }


    $count = DB::table('course_category')->count();
    if ($count == 0) {
        $lookup = array('General');
        for($i=0; $i<count($lookup); $i++) {
            DB::table('course_category')->insert([
                'category_name' => $lookup[$i],
                'created_by' => $user_id,
            ]);
        }
    }


    $count = DB::table('status_lookup')->count();
    if ($count == 0) {
        $lookup = array('active', 'inactive', 'requested', 'rejected', 'accepted', 'null');
        for($i=0; $i<count($lookup); $i++) {
            DB::table('status_lookup')->insert([
                'status_name' => $lookup[$i],
                'created_by' => $user_id,
            ]);
        }
    }

}


function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 10; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}


function getAppInfo() {

    $settings_data = DB::table('app_settings')
        ->select('setting_key', 'setting_value')
        ->get();

    $settings = array();
    foreach($settings_data as $row) $settings[$row->setting_key] = $row->setting_value;

    return $settings;
}


function get_words($sentence, $count = 15) {
    return implode(' ', array_slice(explode(' ', $sentence), 0, $count));
}


function check_course_enrollment($course_id) {

    $student_id = Auth::user()->id;

    $course_student_status = DB::table('course_student')
        ->select('status_lookup.status_name')
        ->join('status_lookup', 'status_lookup.status_id', '=', 'course_student.status')
        ->where('course_id', $course_id)
        ->where('student_id', $student_id)
        ->first();

    $course_student_status = (array)($course_student_status);

    if (count($course_student_status) == 0) return 'null';
    else return $course_student_status['status_name'];
}


function get_video_link($url, $mime_type) {

    $video_link = '';

    if ($mime_type == 'vimeo.com') {
        $video = explode("/",$url);
        $video_link = $video[count($video)-1];
    }
    else if ($mime_type == 'youtube.com') {
        $video = explode("=",$url);
        $video_2 = explode("/",$video[count($video)-1]);
        $video_link = $video_2[count($video_2)-1];
    }

    return $video_link;
}


function create_directory() {

    $path = public_path() . '/media';
    if (!File::exists($path)) File::makeDirectory($path);

}


function timeDifference($time_1, $time_2=0) {

    date_default_timezone_set('Asia/Dhaka');
    if ($time_2 == 0) $time_2 = date('Y-m-d H:i:s');

    $dateDiff = intval((strtotime($time_1)-strtotime($time_2))/60);

    $hours = abs(intval($dateDiff/60));
    $minutes = abs($dateDiff%60);

    $output = '';

    if ($hours > 23) {
        $hours = ceil($hours/24);
        $output = ($hours==1? '1 day ago' : $hours . ' days ago') ;
    }
    else if ($hours < 23 && $hours > 0) {
        $output = ($hours==1? '1 hour ago' : $hours . ' hours ago') ;
    }
    else {
        $output = ($minutes<=1? '1 minute ago' : $minutes . ' minutes ago') ;
    }

    return $output;
}


function calculateRevenue() {

    $course_count = 0;

    $paid_courses = DB::table('courses')
        ->select('course_id', 'price', 'discount')
        ->where('free_course', 0)
        ->get();


    foreach ($paid_courses as $course) {

        $course_price = $course->discount==0? $course->price:$course->discount;

        $count = DB::table('course_student')
            ->where('course_id', $course->course_id)
            ->distinct('course_id')
            ->count('course_student_id');

        $course_count += ($count * $course_price);
    }

    return $course_count;
}


function testFunction() {

    echo 'test function print <br>';

}


function deleteCourse($course_id = 0) {

    if ($course_id == 0) {

        $courses = DB::table('courses')->select('course_id')->get();

        foreach ($courses as $course) {
            deleteCourse($course->course_id);
        }
    }

    else {
        deleteSection(0, $course_id);
        deleteFAQ(0, $course_id);
        deleteQuiz(0, $course_id);
        deleteExam(0, $course_id);
        DB::table('courses')->where('course_id', $course_id)->delete();
    }


}


function deleteSection($section_id = 0, $course_id = 0) {

    if ($course_id != 0) {

        $sections = DB::table('sections')
            ->select('section_id')
            ->where('course_id', $course_id)
            ->get();

        foreach ($sections as $section) {
            deleteSection($section->section_id);
        }
    }

    else {
        deleteLecture(0, $section_id);
        DB::table('sections')->where('section_id', $section_id)->delete();
    }
}


function deleteLecture($lecture_id = 0, $section_id = 0) {

    if ($section_id != 0) {

        $lectures = DB::table('lectures')
            ->select('lecture_id')
            ->where('section_id', $section_id)
            ->get();

        foreach ($lectures as $lecture) {
            deleteLecture($lecture->lecture_id);
        }
    }

    else {
        deleteContent(0, $lecture_id);
        DB::table('lectures')->where('lecture_id', $lecture_id)->delete();
    }
}


function deleteContent($content_id = 0, $lecture_id = 0) {

    if ($lecture_id != 0) {

        $contents = DB::table('lecture_contents')
            ->select('content_id')
            ->where('lecture_id', $lecture_id)
            ->get();

        foreach ($contents as $content) {
            deleteContent($content->content_id);
        }
    }

    else {

        $contents = DB::table('lecture_contents')->select('content')->where('content_id', $content_id)->first();
        $destinationPath = 'media';
        deleteFile($destinationPath . '/' . $contents->content);

        DB::table('lecture_contents')->where('content_id', $content_id)->delete();
    }
}


function deleteFlashcard($flashcard_id = 0, $lecture_id = 0) {

    if ($lecture_id != 0) {

        $contents = DB::table('content_flashcard')
            ->select('flashcard_id')
            ->where('lecture_id', $lecture_id)
            ->get();

        foreach ($contents as $content) {
            deleteFlashcard($content->flashcard_id);
        }
    }

    else {
        $contents = DB::table('content_flashcard')->select('media')->where('flashcard_id', $flashcard_id)->first();
        $destinationPath = 'media';
        deleteFile($destinationPath . '/' . $contents->media);

        DB::table('content_flashcard')->where('flashcard_id', $flashcard_id)->delete();
    }
}


function deleteFAQ($faq_id = 0, $course_id = 0) {

    if ($course_id != 0) {

        $course_faq = DB::table('course_faq')
            ->select('faq_id')
            ->where('course_id', $course_id)
            ->get();

        foreach ($course_faq as $faq) {
            deleteFAQ($faq->faq_id);
        }
    }

    else {
        DB::table('course_faq')->where('faq_id', $faq_id)->delete();
    }
}


function deleteExam($exam_id = 0, $course_id = 0) {

    if ($course_id != 0) {

        $exams = DB::table('exams')
            ->select('exam_id')
            ->where('course_id', $course_id)
            ->get();

        foreach ($exams as $exam) {
            deleteExam($exam->exam_id);
        }
    }

    else {
        DB::table('exams')->where('exam_id', $exam_id)->delete();
        DB::table('exam_question')->where('exam_id', $exam_id)->delete();
        deleteExamResults($exam_id);
    }
}


function deleteExamResults($exam_id) {

    $exam_results = DB::table('exam_results')
        ->select('result_id')
        ->where('exam_id', $exam_id)
        ->get();

    foreach ($exam_results as $result) {
        DB::table('exam_results')->where('result_id', $result->result_id)->delete();
        DB::table('exam_scripts')->where('result_id', $result->result_id)->delete();
    }
}


function deleteQuiz($quiz_id = 0, $course_id = 0) {

    if ($course_id != 0) {

        $quizs = DB::table('quiz')
            ->select('quiz_id')
            ->where('course_id', $course_id)
            ->get();

        foreach ($quizs as $quiz) {
            deleteQuiz($quiz->quiz_id);
        }
    }

    else {

        deleteQuizQuestion(0, $quiz_id);
        deleteQuizResults($quiz_id);

        DB::table('quiz')->where('quiz_id', $quiz_id)->delete();


    }
}


function deleteQuizQuestion($question_id = 0, $quiz_id = 0) {

    if ($quiz_id != 0) {

        $questions = DB::table('quiz_question')
            ->select('question_id')
            ->where('quiz_id', $quiz_id)
            ->get();

        foreach ($questions as $question) {
            deleteQuizQuestion($question->question_id);
        }
    }

    else {
        $question = DB::table('quiz_question')->where('question_id', $question_id)->first();
        $destinationPath_1 = 'questionimage/';
        $destinationPath_2 = 'explanation/';
        deleteFile($destinationPath_1 . $question->question_media);
        deleteFile($destinationPath_2 . $question->explanation_media);

        DB::table('quiz_question')->where('question_id', $question_id)->delete();
    }
}


function deleteQuizResults($quiz_id) {

    $quiz_results = DB::table('quiz_results')
        ->select('result_id')
        ->where('quiz_id', $quiz_id)
        ->get();

    foreach ($quiz_results as $result) {
        DB::table('quiz_results')->where('result_id', $result->result_id)->delete();
        DB::table('quiz_scripts')->where('result_id', $result->result_id)->delete();
    }
}


function deleteFile($url) {
    File::delete($url);
}


function getLectureProgress($student_id, $lecture_id) {

    $quizzes = DB::table('quiz')
        ->where('lecture_id', $lecture_id)
        ->get();

    $quiz_count = count($quizzes);
    $lecture_completed = 0;

    foreach ($quizzes as $quiz) {

        $temp_quiz_count = DB::table('quiz_results')
            ->where('quiz_id', $quiz->quiz_id)
            ->where('student_id', $student_id)
            ->get();

        if (count($temp_quiz_count)) $lecture_completed++;
    }

    if ($quiz_count != 0) $lecture_completed = ($lecture_completed/$quiz_count) * 100;
    else $lecture_completed = 100;

    return $lecture_completed;
}


function getCourseProgress($student_id, $course_id) {

    $quizzes = DB::table('quiz')
        ->where('course_id', $course_id)
        ->get();

    $quiz_count = count($quizzes);
    $course_completed = 0;

    foreach ($quizzes as $quiz) {

        $temp_quiz_count = DB::table('quiz_results')
            ->where('quiz_id', $quiz->quiz_id)
            ->where('student_id', $student_id)
            ->get();

        if (count($temp_quiz_count)) $course_completed++;
    }

    if ($quiz_count != 0) $course_completed = ($course_completed/$quiz_count) * 100;
    else $course_completed = 100;

    return $course_completed;
}


function checkCourseCompletedStatus($student_id=0) {

    if ($student_id == 0) {

        $students = DB::table('users')
            ->select('id')
            ->where('role', 'student')
            ->get();

        foreach ($students as $student) {
            checkCourseCompleted($student->id);
        }
    }

    else checkCourseCompleted($student_id);

}


function checkCourseCompleted($student_id) {

    $course_student = DB::table('course_student')
        ->where('student_id', $student_id)
        ->get();

    foreach ($course_student as $item) {

        $course_completed = getCourseProgress($student_id, $item->course_id);

        DB::table('course_student')
            ->where('course_student_id', $item->course_student_id)
            ->update(['completed' => $course_completed]);

    }


}


function deleteUser($user_id) {

    $users = DB::table('users')->where('id', $user_id)->first();
    $destinationPath = 'images/users';

    if ($users->email != 'superuser@scholars.com') {

        if ($users->role != 'student') transferCourse($users->id);
        if ($users->user_img != 'default_user.png') deleteFile($destinationPath . '/' . $users->user_img);
        DB::table('users')->where('id', $user_id)->delete();

    }

}


function transferCourse($tutor_id, $transferred_id=0) {

    $courses = DB::table('courses')
        ->where('instructor', $tutor_id)
        ->get();

    $instructor = $transferred_id;

    if ($transferred_id == 0) $instructor = getSuperuserId();

    foreach ($courses as $course) {

        DB::table('courses')
            ->where('course_id', $course->course_id)
            ->update(['instructor' => $instructor]);

    }
}


function getSuperuserId() {

    $users = DB::table('users')->where('email', 'superuser@scholars.com')->first();
    return $users->id;

}


function is_connected() {
    $connected = @fsockopen("www.example.com", 80);
    //website, port  (try 80 or 443)
    if ($connected) {
        $is_conn = true; //action when connected
        fclose($connected);
    }else {
        $is_conn = false; //action in connection failure
    }
    return $is_conn;
}


function changePassword($user_id, $new_password) {

    DB::table('users')
        ->where('id', $user_id)
        ->update([
            'password' => Hash::make($new_password)
        ]);
}

